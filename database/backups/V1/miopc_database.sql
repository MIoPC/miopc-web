-- phpMyAdmin SQL Dump
-- version 4.6.6deb4+deb9u2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 04, 2021 at 01:52 AM
-- Server version: 10.1.48-MariaDB-0+deb9u1
-- PHP Version: 7.2.34-9+0~20210112.53+debian9~1.gbpfdd1e6

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miopc_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `value` text COLLATE utf8mb4_unicode_ci,
  `errors` bigint(20) DEFAULT NULL,
  `changes` bigint(20) DEFAULT NULL,
  `commands` bigint(20) DEFAULT NULL,
  `steps` bigint(20) DEFAULT NULL,
  `plays` bigint(20) DEFAULT NULL,
  `elapsed_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `journey_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `audio_files`
--

CREATE TABLE `audio_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `genre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `license` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `license_link` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `audio_files`
--

INSERT INTO `audio_files` (`id`, `path`, `author`, `title`, `source`, `genre`, `license`, `license_link`) VALUES
(1, 'assets/audio/acoustic/1.mp3', 'Sahara Sky', 'Sun Goes Down acoustic', 'https://freemusicarchive.org/music/Sahara_Sky/The_Long_Road/07_Sun_Goes_Down_acoustic', 'acoustic', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(2, 'assets/audio/acoustic/2.mp3', 'Wildlight', 'Rain (Acoustic)', 'https://freemusicarchive.org/music/Wildlight/The_Tide_Acoustic/Wildlight_-_The_Tide_Acoustic_-_01_Rain_Acoustic', 'acoustic', 'CC BY-NC-SA 3.0 US', 'https://creativecommons.org/licenses/by-nc-sa/3.0/us/'),
(3, 'assets/audio/acoustic/3.mp3', 'Doctor Turtle', 'Fingerlympics', 'https://freemusicarchive.org/music/Doctor_Turtle/5f91e09024ca8/fingerlympics', 'acoustic', 'CC BY 4.0', 'https://creativecommons.org/licenses/by/4.0/'),
(4, 'assets/audio/alternative/1.mp3', 'Forget the Whale', 'Without You', 'https://freemusicarchive.org/music/Forget_the_Whale/Take_to_the_Skies/Without_You', 'alternative', 'CC BY-NC-SA 4.0', 'https://creativecommons.org/licenses/by-nc-sa/4.0/'),
(5, 'assets/audio/alternative/2.mp3', 'Forget the Whale', 'Hex', 'https://freemusicarchive.org/music/Forget_the_Whale/What_I_Tell_Myself_Vol_2/Forget_the_Whale_-_Hex', 'alternative', 'CC BY-NC-SA 4.0', 'https://creativecommons.org/licenses/by-nc-sa/4.0/'),
(6, 'assets/audio/alternative/3.mp3', 'Forget the Whale', 'Another Trick Up My Sleeve', 'https://freemusicarchive.org/music/Forget_the_Whale/What_I_Tell_Myself_Vol_2/Forget_the_Whale_-_Another_Trick_Up_My_Sleeve', 'alternative', 'CC BY-NC-SA 4.0', 'https://creativecommons.org/licenses/by-nc-sa/4.0/'),
(7, 'assets/audio/blues/1.mp3', 'Kevin MacLeod', 'Slow Burn', 'https://freemusicarchive.org/music/Kevin_MacLeod/Blues_Sampler/Slow_Burn', 'blues', 'CC BY 3.0', 'https://creativecommons.org/licenses/by/3.0/'),
(8, 'assets/audio/blues/2.mp3', 'Lobo Loco', 'Comming Back - Instrumental (ID 1355)', 'https://freemusicarchive.org/music/Lobo_Loco/Not_my_Brain/comming-back-instrumental-id-1355mp3', 'blues', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(9, 'assets/audio/blues/3.mp3', 'Lobo Loco', 'Lucky Hans (ID 361)', 'https://freemusicarchive.org/music/Lobo_Loco/NIce_Nowhere/Lucky_Hans_ID_361_1261', 'blues', 'CC BY-NC-SA 3.0 DE', 'https://creativecommons.org/licenses/by-nc-sa/3.0/de/deed.en'),
(10, 'assets/audio/classical/1.mp3', 'Nico de Napoli', 'Bach: Prelude in E flat minor', 'https://freemusicarchive.org/music/Nico_de_Napoli/lento/bach-prelude-in-e-flat-minor', 'classical', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(11, 'assets/audio/classical/2.mp3', 'Dee Yan-Key', 'wanderlust', 'https://freemusicarchive.org/music/Dee_Yan-Key/wanderlust/wanderlust-1', 'classical', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(12, 'assets/audio/classical/3.mp3', 'Nico de Napoli', 'Chopin: Scherzo in B minor', 'https://freemusicarchive.org/music/Nico_de_Napoli/pianoforte-1/chopin-scherzo-in-b-minor', 'classical', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(13, 'assets/audio/country/1.mp3', 'Zephaniah And The 18 Wheelers', 'Take Your Love Out of Town', 'https://freemusicarchive.org/music/Zephaniah_And_The_18_Wheelers/Live_On_WFMUs_Honky_Tonk_Radio_Girl_Program_with_Becky_11316/Zephaniah_And_The_18_Wheelers_03_Take_Your_Love_Out_of_Town', 'country', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(14, 'assets/audio/country/2.mp3', 'Bryan Mathys', 'It\'s Not Hard to Get Lost', 'https://freemusicarchive.org/music/Bryan_Mathys/Wild_Heart/Bryan_Mathys_-_Wild_Heart_-_03_Its_Not_Hard_to_Get_Lost', 'country', 'CC BY 4.0', 'https://creativecommons.org/licenses/by/4.0/'),
(15, 'assets/audio/country/3.mp3', 'Dazie Mae', 'Can\'t Take No Chances', 'https://freemusicarchive.org/music/Dazie_Mae/Songs_Matured_in_Oak/04_-_Dazie_Mae_-_Can_t_Take_No_Chances', 'country', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(16, 'assets/audio/dance/1.mp3', 'Computer Music All-stars', 'Squigglejammer', 'https://freemusicarchive.org/music/Computer_Music_All-Stars/CPlaylistsRandom/Squigglejammer', 'dance', 'CC BY 4.0', 'https://creativecommons.org/licenses/by/4.0/'),
(17, 'assets/audio/dance/2.mp3', 'Florian Decros', 'Sun Tan Lines', 'https://freemusicarchive.org/music/Florian_Decros/Ab_absurdo/Florian_Decros_-_Ab_absurdo_-_05_Sun_Tan_Lines', 'dance', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(18, 'assets/audio/dance/3.mp3', 'Chalk Dinosaur', 'Air Around Us', 'https://freemusicarchive.org/music/Chalk_Dinosaur/FrostClick_FrostWire_Creative_Commons_Mixtape_Vol_5_/11_Chalk_Dinosaur_Air_Around_Us_Flow_State_FROSTCLICK_FROSTWIRE_CREATIVE_COMMONS_MIXTAPE_VOL_5', 'dance', 'CC BY 4.0', 'https://creativecommons.org/licenses/by/4.0/'),
(19, 'assets/audio/electronic/1.mp3', 'Borrtex', 'Impulsing', 'https://freemusicarchive.org/music/Borrtex/The_Impulse/18_Borrtex_-_18_Impulsing', 'electronic', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(20, 'assets/audio/electronic/2.mp3', 'Florian Decros', 'Nebula Rush', 'https://freemusicarchive.org/music/Florian_Decros/Ab_absurdo/Florian_Decros_-_Ab_absurdo_-_04_Nebula_Rush', 'electronic', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(21, 'assets/audio/electronic/3.mp3', 'Idiophone', 'Bio Unit', 'https://freemusicarchive.org/music/Bio_Unit/disquiet/idiophone', 'electronic', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(22, 'assets/audio/folk/1.mp3', 'Derek Clegg', 'Making It Right', 'https://freemusicarchive.org/music/Derek_Clegg/the-middler/making-it-right', 'folk', 'CC BY-NC-SA 4.0', 'https://creativecommons.org/licenses/by-nc-sa/4.0/'),
(23, 'assets/audio/folk/2.mp3', 'All the Time You Never Had', 'Scott Gratton', 'https://freemusicarchive.org/music/Scott_Gratton/progress-and-regress/all-the-time-you-never-had-1', 'folk', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(24, 'assets/audio/folk/3.mp3', 'Les Hayden', 'Golly Gosh', 'https://freemusicarchive.org/music/Les_Hayden/telepathy/golly-gosh2', 'folk', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(25, 'assets/audio/hip_hop/1.mp3', 'Audiobinger', 'I Used to Love Hip-Hop', 'https://freemusicarchive.org/music/Audiobinger/quarantine-beats-vol-1/i-used-to-love-hip-hop', 'hip_hop', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(26, 'assets/audio/hip_hop/2.mp3', 'Makaih Beats', 'Pray (makaih.com)', 'https://freemusicarchive.org/music/Makaih_Beats/pray/pray-makaihcom', 'hip_hop', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(27, 'assets/audio/hip_hop/3.mp3', 'Yung Kartz', 'Entitled', 'https://freemusicarchive.org/music/Yung_Kartz/february-2020/entitled', 'hip_hop', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(28, 'assets/audio/house/1.mp3', 'Marc Burt', 'Caledonia (edit)', 'https://freemusicarchive.org/music/Marc_Burt/landscapes/caledonia-edit', 'house', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(29, 'assets/audio/house/2.mp3', 'Metre', 'Breathe', 'https://freemusicarchive.org/music/Metre/surface-area/breathe', 'house', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(30, 'assets/audio/house/3.mp3', 'Cristi Cons', 'Couch Revenge', 'https://freemusicarchive.org/music/Cristi_Cons/BuzzRO_2010/02_Couch_Revenge', 'house', 'CC BY-NC-SA 3.0 RO', 'https://creativecommons.org/licenses/by-nc-sa/3.0/ro/deed.en'),
(31, 'assets/audio/indie/1.mp3', 'Ketsa', 'Straight-Up', 'https://freemusicarchive.org/music/Ketsa/the-lost-files/straight-up', 'indie', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(32, 'assets/audio/indie/2.mp3', 'Ryan Andersen', 'London Calling', 'https://freemusicarchive.org/music/Ryan_Andersen/Never_Sleep_-_Indie_Rock/London_Calling', 'indie', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(33, 'assets/audio/indie/3.mp3', 'Ketsa', 'Wild-Rivers', 'https://freemusicarchive.org/music/Ketsa/the-lost-files/wild-rivers', 'indie', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(34, 'assets/audio/jazz/1.mp3', 'Mela', 'The Strip', 'https://freemusicarchive.org/music/Mela/Mela_two/MELA_-_Mela_Two_-_09_The_Strip_1770', 'jazz', 'CC BY-SA 4.0', 'https://creativecommons.org/licenses/by-sa/4.0/'),
(35, 'assets/audio/jazz/2.mp3', 'Kevin MacLeod', 'Night on the Docks - Sax', 'https://freemusicarchive.org/music/Kevin_MacLeod/Jazz_Sampler/Night_on_the_Docks_-_Sax', 'jazz', 'CC BY 3.0', 'https://creativecommons.org/licenses/by/3.0/'),
(36, 'assets/audio/jazz/3.mp3', 'Dee Yan-Key', 'Tranquility', 'https://freemusicarchive.org/music/Dee_Yan-Key/facts_of_life/14--Dee_Yan-Key-Tranquility', 'jazz', 'CC BY-NC-SA 4.0', 'https://creativecommons.org/licenses/by-nc-sa/4.0/'),
(37, 'assets/audio/lo_fi/1.mp3', 'deeB', 'Blown Out', 'https://freemusicarchive.org/music/deeB/Thru_Nature_EP/deeB_-_07_-_Blown_Out', 'lo_fi', 'CC BY-NC-ND 3.0', 'https://creativecommons.org/licenses/by-nc-nd/3.0/'),
(38, 'assets/audio/lo_fi/2.mp3', 'Ketsa', 'Dream Stroll', 'https://freemusicarchive.org/music/Ketsa/abundance/dream-stroll', 'lo_fi', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(39, 'assets/audio/lo_fi/3.mp3', 'Scott Holmes Music', 'Urban Haze', 'https://freemusicarchive.org/music/Scott_Holmes/media-music-mix/urban-haze', 'lo_fi', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(40, 'assets/audio/metal/1.mp3', 'XTaKeRuX', 'Demonization', 'https://freemusicarchive.org/music/XTaKeRuX/Demonized/Demonization', 'metal', 'CC BY 4.0', 'https://creativecommons.org/licenses/by/4.0/'),
(41, 'assets/audio/metal/2.mp3', 'Loyalty Freak Music', 'GIGA METAL', 'https://freemusicarchive.org/music/Loyalty_Freak_Music/HYPER_METAL_/Loyalty_Freak_Music_-_HYPER_METAL__-_07_GIGA_METAL', 'metal', 'CC0 1.0', 'https://creativecommons.org/publicdomain/zero/1.0/'),
(42, 'assets/audio/metal/3.mp3', 'The Great Cold', 'NEPHELAI', 'https://freemusicarchive.org/music/The_Great_Cold/The_Great_Cold/The_Great_Cold_-_The_Great_Cold_-_09_NEPHELAI', 'metal', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(43, 'assets/audio/pop/1.mp3', 'Brock Tyler', 'Be My Baby', 'https://freemusicarchive.org/music/Brock_Tyler/Us_1318/Brock_Tyler_-_Us_-_02_Be_My_Baby', 'pop', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(44, 'assets/audio/pop/2.mp3', 'Scott Holmes Music', 'Clear Progress', 'https://freemusicarchive.org/music/Scott_Holmes/Corporate__Motivational_Music_2/clear-progress', 'pop', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(45, 'assets/audio/pop/3.mp3', 'Crowander', 'Funhouse', 'https://freemusicarchive.org/music/crowander/uplifting-funband/funhouse', 'pop', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(46, 'assets/audio/punk/1.mp3', 'Tendinite', 'Come Right Here', 'https://freemusicarchive.org/music/Tendinite/Tendinite/Tendinite_-_Tendinite_-_EP1_-_05_Come_Right_Here', 'punk', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(47, 'assets/audio/punk/2.mp3', 'Brandy', 'Rent Quest', 'https://freemusicarchive.org/music/Brandy/Live_at_WFMU_for_Spin_Age_Blasters_with_Creamo_Coyl_4222018/Brandy_7_Rent_Quest', 'punk', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(48, 'assets/audio/punk/3.mp3', 'Tendinite', 'Inhale/Exhale', 'https://freemusicarchive.org/music/Tendinite/Tendinite/Tendinite_-_Tendinite_-_EP1_-_01_Inhale-Exhale_CC-BY-NC-ND', 'punk', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(49, 'assets/audio/reggae/1.mp3', 'Shaolin Dub', 'Truimphant dub', 'https://freemusicarchive.org/music/Shaolin_Dub/Warrior_Way/Truimphant_dub_', 'reggae', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(50, 'assets/audio/reggae/2.mp3', 'Shaolin Dub', 'China dub', 'https://freemusicarchive.org/music/Shaolin_Dub/Warrior_Way/China_dub', 'reggae', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(51, 'assets/audio/reggae/3.mp3', 'Shaolin Dub', 'Yabba dub', 'https://freemusicarchive.org/music/Shaolin_Dub/Warrior_Way/Yabba_dub', 'reggae', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(52, 'assets/audio/rock/1.mp3', 'Yellow Cop', 'Last Xl', 'https://freemusicarchive.org/music/Yellow_Cop/Tragedy_memories/Yellow_Cop_-_Tragedy_Memories_-_01_Last_Xl', 'rock', 'CC BY-SA 4.0', 'https://creativecommons.org/licenses/by-sa/4.0/'),
(53, 'assets/audio/rock/2.mp3', 'Crowander', 'Slippery Rocks', 'https://freemusicarchive.org/music/crowander/from-the-garage/slippery-rocks', 'rock', 'CC BY-NC 4.0', 'https://creativecommons.org/licenses/by-nc/4.0/'),
(54, 'assets/audio/rock/3.mp3', 'Pheasant', 'Never Coming Back', 'https://freemusicarchive.org/music/Pheasant/Gravel_Beach/13_-_Pheasant_-_Never_Coming_Back', 'rock', 'CC BY-NC-SA 4.0', 'https://creativecommons.org/licenses/by-nc-sa/4.0/'),
(55, 'assets/audio/soul/1.mp3', 'BJ Block & Dawn Pemberton', 'Everybody\'s Party', 'https://freemusicarchive.org/music/BJ_Block__Dawn_Pemberton/The_Land_of_Make_Believe/05__BJ_Block_and_Dawn_Pemberton__Everybodys_Party__The_Land_of_Make_Believe__FrostWirecom', 'soul', 'CC BY-ND 3.0', 'https://creativecommons.org/licenses/by-nd/3.0/'),
(56, 'assets/audio/soul/2.mp3', 'Podington Bear', 'Blue Highway', 'https://freemusicarchive.org/music/Podington_Bear/Soul/Blue_Highway', 'soul', 'CC BY-NC 3.0', 'https://creativecommons.org/licenses/by-nc/3.0/'),
(57, 'assets/audio/soul/3.mp3', 'BJ Block & Dawn Pemberton', 'You Happy?', 'https://freemusicarchive.org/music/BJ_Block__Dawn_Pemberton/The_Land_of_Make_Believe/04__BJ_Block_and_Dawn_Pemberton__You_Happy__The_Land_of_Make_Believe__FrostWirecom', 'soul', 'CC BY-ND 3.0', 'https://creativecommons.org/licenses/by-nd/3.0/'),
(58, 'assets/audio/symphonic/1.mp3', 'Ketsa', 'Beautiful-Hope-Ambience', 'https://freemusicarchive.org/music/Ketsa/unified-division/beautiful-hope-ambience', 'symphonic', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(59, 'assets/audio/symphonic/2.mp3', 'BOPD', 'New England is Interesting', 'https://freemusicarchive.org/music/BOPD/Old_Paper_Houses/BOPD_-_NE_is_Interesting', 'symphonic', 'CC BY-NC 3.0', 'https://creativecommons.org/licenses/by-nc/3.0/'),
(60, 'assets/audio/symphonic/3.mp3', 'Kai Engel', 'Low Horizon', 'https://freemusicarchive.org/music/Kai_Engel/The_Scope/Kai_Engel_-_The_Scope_-_08_Low_Horizon', 'symphonic', 'CC BY 4.0', 'https://creativecommons.org/licenses/by/4.0/'),
(61, 'assets/audio/synth_pop/1.mp3', 'The Kyoto Connection', 'Take on me', 'https://freemusicarchive.org/music/The_Kyoto_Connection/No_Headphones_Required/02_-_Take_on_me', 'synth_pop', 'CC BY-NC-ND 2.5 AR', 'https://creativecommons.org/licenses/by-nc-nd/2.5/ar/deed.en'),
(62, 'assets/audio/synth_pop/2.mp3', 'Timecrawler 82', 'Infinity', 'https://freemusicarchive.org/music/Timecrawler_82/infinity/infinity', 'synth_pop', 'CC BY 4.0', 'https://creativecommons.org/licenses/by/4.0/'),
(63, 'assets/audio/synth_pop/3.mp3', 'Jahzzar', 'No Control', 'https://freemusicarchive.org/music/Jahzzar/Super_1222/03_No_Control', 'synth_pop', 'CC BY-SA 4.0', 'https://creativecommons.org/licenses/by-sa/4.0/'),
(64, 'assets/audio/techno/1.mp3', 'Lobo Loco', 'Rollercoaster (ID 1437)', 'https://freemusicarchive.org/music/Lobo_Loco/hamburg-lights/rollercoaster-id-1437', 'techno', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/'),
(65, 'assets/audio/techno/2.mp3', 'ENDRUDARK', 'Time Up', 'https://freemusicarchive.org/music/ENDRUDARK/Dreams/03_-_Time_Up', 'techno', 'CC BY 3.0', 'https://creativecommons.org/licenses/by/3.0/'),
(66, 'assets/audio/techno/3.mp3', 'Synapsis', 'The Path', 'https://freemusicarchive.org/music/Synapsis/Journey_Through_Oblivion/The_Path_1583', 'techno', 'CC BY-NC-ND 4.0', 'https://creativecommons.org/licenses/by-nc-nd/4.0/');

-- --------------------------------------------------------

--
-- Table structure for table `enums`
--

CREATE TABLE `enums` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `enums`
--

INSERT INTO `enums` (`id`, `type`) VALUES
(1, 'PROGRAMMING_SPECIALIZATION'),
(2, 'PROGRAMMING_LANGUAGE'),
(4, 'LIKERT_5'),
(5, 'MUSIC_GENRES');

-- --------------------------------------------------------

--
-- Table structure for table `journeys`
--

CREATE TABLE `journeys` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('desktop','mobile') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finished_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `survey_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_02_04_203117_create_surveys_table', 1),
(4, '2021_02_04_203136_create_sections_table', 1),
(5, '2021_02_04_203137_create_journeys_table', 1),
(6, '2021_02_04_203145_create_enums_table', 1),
(7, '2021_02_04_203146_create_questions_table', 1),
(8, '2021_02_04_203154_create_answers_table', 1),
(9, '2021_02_04_203155_create_values_table', 1),
(10, '2021_02_04_204316_create_survey_x_section_table', 1),
(11, '2021_02_04_204517_create_section_x_question_table', 1),
(12, '2021_02_07_014606_create_remove_abandoned_journeys_event', 1),
(16, '2021_02_04_204518_create_audio_files_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('STRING','NUMBER','TEXT','CHOICE','LIKERT','PUZZLE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('FAVORITE_GENRE','UNBELOVED_GENRE') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `puzzle_structure` text COLLATE utf8mb4_unicode_ci,
  `validation_rules` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `editable` tinyint(1) NOT NULL DEFAULT '1',
  `enum_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `title`, `type`, `role`, `puzzle_structure`, `validation_rules`, `editable`, `enum_id`) VALUES
(1, 'V1.programming_experience.title', 'NUMBER', NULL, NULL, 'REQUIRED;INTEGER;RANGE:0,50', 1, NULL),
(2, 'V1.it_seniority.title', 'NUMBER', NULL, NULL, 'REQUIRED;INTEGER;RANGE:0,50', 1, NULL),
(3, 'V1.programming_specialization.title', 'CHOICE', NULL, NULL, 'REQUIRED;CHOICE', 1, 1),
(4, 'V1.programming_language_proficiency.title', 'NUMBER', NULL, NULL, 'REQUIRED;INTEGER;RANGE:0,50', 1, NULL),
(5, 'V1.programming_language.title', 'CHOICE', NULL, NULL, 'REQUIRED;CHOICE', 1, 2),
(6, 'V1.procedural_paradigm_proficiency.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(7, 'V1.functional_paradigm_proficiency.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(8, 'V1.logical_paradigm_proficiency.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(9, 'V1.object_oriented_paradigm_proficiency.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(10, 'V1.favorite_music_genre.title', 'CHOICE', 'FAVORITE_GENRE', NULL, 'REQUIRED;CHOICE', 1, 5),
(12, 'V1.music_listening_when_working.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(13, 'V1.music_listening_when_programming.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(14, 'V1.audiophile.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(15, 'V1.music_stimulus_effect.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(16, 'V1.music_mood_effect.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(17, 'V1.music_education.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(18, 'V1.music_talent.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 1, 4),
(19, '', 'PUZZLE', NULL, '{\r\n	\"tiles\": [\r\n		{\r\n			\"x\": 0,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 1,\r\n			\"y\": 0,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 2,\r\n			\"y\": 0,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 3,\r\n			\"y\": 0,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 5,\r\n			\"y\": 0,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 4,\r\n			\"y\": 0,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 6,\r\n			\"y\": 0,\r\n			\"type\": \"start\"\r\n		}\r\n	],\r\n	\"pointer\": {\r\n		\"rotation\": 270\r\n	},\r\n	\"procedures\": [\r\n		2\r\n	]\r\n}', '', 0, NULL),
(20, '', 'PUZZLE', NULL, '{\r\n	\"tiles\": [\r\n		{\r\n			\"x\": 0,\r\n			\"y\": 1,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 1,\r\n			\"y\": 1,\r\n			\"type\": \"start\"\r\n		},\r\n		{\r\n			\"x\": 0,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		}\r\n	],\r\n	\"pointer\": {\r\n		\"rotation\": 0\r\n	},\r\n	\"procedures\": [\r\n		3\r\n	]\r\n}', '', 0, NULL),
(21, '', 'PUZZLE', NULL, '{\r\n	\"tiles\": [\r\n		{\r\n			\"x\": 0,\r\n			\"y\": 1,\r\n			\"type\": \"start\"\r\n		},\r\n		{\r\n			\"x\": 1,\r\n			\"y\": 1,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 2,\r\n			\"y\": 1,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 3,\r\n			\"y\": 1,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 0,\r\n			\"y\": 0,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 1,\r\n			\"y\": 0,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 2,\r\n			\"y\": 0,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 3,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		}\r\n	],\r\n	\"pointer\": {\r\n		\"rotation\": 90\r\n	},\r\n	\"procedures\": [\r\n		4,\r\n		2\r\n	]\r\n}', '', 0, NULL),
(22, '', 'PUZZLE', NULL, '{\r\n	\"tiles\": [\r\n		{\r\n			\"x\": 1,\r\n			\"y\": 2,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 2,\r\n			\"y\": 2,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 3,\r\n			\"y\": 2,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 0,\r\n			\"y\": 1,\r\n			\"type\": \"start\"\r\n		},\r\n		{\r\n			\"x\": 1,\r\n			\"y\": 1,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 2,\r\n			\"y\": 1,\r\n			\"type\": \"tile\"\r\n		},\r\n		{\r\n			\"x\": 3,\r\n			\"y\": 1,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 1,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 2,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 3,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		}\r\n	],\r\n	\"pointer\": {\r\n		\"rotation\": 90\r\n	},\r\n	\"procedures\": [\r\n		4,\r\n		3,\r\n		2\r\n	]\r\n}', '', 0, NULL),
(23, '', 'PUZZLE', NULL, '{\r\n	\"tiles\": [\r\n		{\r\n			\"x\": 0,\r\n			\"y\": 1,\r\n			\"type\": \"start\"\r\n		},\r\n		{\r\n			\"x\": 1,\r\n			\"y\": 1,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 2,\r\n			\"y\": 1,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 3,\r\n			\"y\": 1,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 4,\r\n			\"y\": 1,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 0,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 1,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 2,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 3,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		},\r\n		{\r\n			\"x\": 4,\r\n			\"y\": 0,\r\n			\"type\": \"checkpoint\"\r\n		}\r\n	],\r\n	\"pointer\": {\r\n		\"rotation\": 180\r\n	},\r\n	\"procedures\": [\r\n		5,\r\n		5,\r\n		5\r\n	]\r\n}', '', 0, NULL),
(24, '', 'PUZZLE', NULL, '{\n	\"tiles\": [\n		{\n			\"x\": 0,\n			\"y\": 2,\n			\"type\": \"checkpoint\"\n		},\n		{\n			\"x\": 1,\n			\"y\": 2,\n			\"type\": \"tile\"\n		},\n		{\n			\"x\": 2,\n			\"y\": 2,\n			\"type\": \"tile\"\n		},\n		{\n			\"x\": 3,\n			\"y\": 2,\n			\"type\": \"tile\"\n		},\n		{\n			\"x\": 4,\n			\"y\": 2,\n			\"type\": \"checkpoint\"\n		},\n		{\n			\"x\": 0,\n			\"y\": 1,\n			\"type\": \"start\"\n		},\n		{\n			\"x\": 1,\n			\"y\": 1,\n			\"type\": \"empty\"\n		},\n		{\n			\"x\": 2,\n			\"y\": 1,\n			\"type\": \"checkpoint\"\n		},\n		{\n			\"x\": 3,\n			\"y\": 1,\n			\"type\": \"empty\"\n		},\n		{\n			\"x\": 4,\n			\"y\": 1,\n			\"type\": \"tile\"\n		},\n		{\n			\"x\": 0,\n			\"y\": 0,\n			\"type\": \"checkpoint\"\n		},\n		{\n			\"x\": 1,\n			\"y\": 0,\n			\"type\": \"tile\"\n		},\n		{\n			\"x\": 2,\n			\"y\": 0,\n			\"type\": \"tile\"\n		},\n		{\n			\"x\": 3,\n			\"y\": 0,\n			\"type\": \"tile\"\n		},\n		{\n			\"x\": 4,\n			\"y\": 0,\n			\"type\": \"checkpoint\"\n		}\n	],\n	\"pointer\": {\n		\"rotation\": 0\n	},\n	\"procedures\": [\n		6,\n		6,\n		6\n	]\n}', '', 0, NULL),
(25, 'V1.self_esteem.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 0, 4),
(26, 'V1.focus_music.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 0, 4),
(27, 'V1.performance_music.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 0, 4),
(28, 'V1.focus_silence.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 0, 4),
(29, 'V1.performance_silence.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 0, 4),
(30, 'V1.difficulty_level.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 0, 4),
(31, 'V1.music_match.title', 'LIKERT', NULL, NULL, 'REQUIRED;LIKERT:5', 0, 4),
(32, 'V1.unbeloved_music_genre.title', 'CHOICE', 'UNBELOVED_GENRE', NULL, 'REQUIRED;CHOICE', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `background_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('SURVEY','PUZZLE_TUTORIAL','PUZZLE') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `title`, `subtitle`, `description`, `background_image`, `type`) VALUES
(1, 'V1.it_survey.title', 'V1.it_survey.subtitle', 'V1.it_survey.description', 'assets/svg/pages/journeys/questionnaire/it_section.svg', 'SURVEY'),
(2, 'V1.music_survey.title', 'V1.music_survey.subtitle', 'V1.music_survey.description', 'assets/svg/pages/journeys/questionnaire/music_section.svg', 'SURVEY'),
(3, 'V1.tutorial.title', 'V1.tutorial.subtitle', 'V1.tutorial.description', 'assets/svg/pages/journeys/questionnaire/tutorial_section.svg', 'PUZZLE_TUTORIAL'),
(4, 'V1.puzzle.title', 'V1.puzzle.subtitle', 'V1.puzzle.description', 'assets/svg/pages/journeys/questionnaire/puzzle_section.svg', 'PUZZLE'),
(5, 'V1.feedback.title', 'V1.feedback.subtitle', 'V1.feedback.description', 'assets/svg/pages/journeys/questionnaire/feedback_section.svg', 'SURVEY'),
(6, 'V1.feedback.title', 'V1.feedback.subtitle', 'V1.feedback.description', 'assets/svg/pages/journeys/questionnaire/feedback_section.svg', 'SURVEY');

-- --------------------------------------------------------

--
-- Table structure for table `section_x_question`
--

CREATE TABLE `section_x_question` (
  `section_id` bigint(20) UNSIGNED NOT NULL,
  `question_id` bigint(20) UNSIGNED NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `section_x_question`
--

INSERT INTO `section_x_question` (`section_id`, `question_id`, `position`) VALUES
(1, 1, 1),
(1, 2, 2),
(1, 3, 3),
(1, 4, 4),
(1, 5, 5),
(1, 6, 6),
(1, 7, 7),
(1, 8, 8),
(1, 9, 9),
(2, 10, 1),
(2, 12, 3),
(2, 13, 4),
(2, 14, 5),
(2, 15, 6),
(2, 16, 7),
(2, 17, 8),
(2, 18, 9),
(2, 32, 2),
(4, 19, 1),
(4, 20, 2),
(4, 21, 3),
(4, 22, 4),
(4, 23, 5),
(4, 24, 6),
(5, 25, 1),
(5, 28, 3),
(5, 29, 4),
(5, 30, 2),
(6, 25, 1),
(6, 26, 3),
(6, 27, 4),
(6, 30, 2),
(6, 31, 5);

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE `surveys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('SILENCE','FAVORITE_MUSIC','UNBELOVED_MUSIC') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `title`, `description`, `version`, `type`) VALUES
(1, 'V1.title', 'V1.description', 'V1', 'SILENCE'),
(2, 'V1.title', 'V1.description', 'V1', 'FAVORITE_MUSIC'),
(3, 'V1.title', 'V1.description', 'V1', 'UNBELOVED_MUSIC');

-- --------------------------------------------------------

--
-- Table structure for table `survey_x_section`
--

CREATE TABLE `survey_x_section` (
  `survey_id` bigint(20) UNSIGNED NOT NULL,
  `section_id` bigint(20) UNSIGNED NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `survey_x_section`
--

INSERT INTO `survey_x_section` (`survey_id`, `section_id`, `position`) VALUES
(1, 1, 1),
(1, 2, 2),
(1, 3, 3),
(1, 4, 4),
(1, 5, 5),
(2, 1, 1),
(2, 2, 2),
(2, 3, 3),
(2, 4, 4),
(2, 6, 5),
(3, 1, 1),
(3, 2, 2),
(3, 3, 3),
(3, 4, 4),
(3, 6, 5);

-- --------------------------------------------------------

--
-- Table structure for table `values`
--

CREATE TABLE `values` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enum_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `values`
--

INSERT INTO `values` (`id`, `value`, `label`, `enum_id`) VALUES
(1, 'backend', 'V1.programming_specialization.backend', 1),
(2, 'frontend', 'V1.programming_specialization.frontend', 1),
(3, 'fullstack', 'V1.programming_specialization.fullstack', 1),
(4, 'mobile_apps', 'V1.programming_specialization.mobile_apps', 1),
(5, 'desktop_apps', 'V1.programming_specialization.desktop_apps', 1),
(6, 'software_testing', 'V1.programming_specialization.software_testing', 1),
(7, 'devops', 'V1.programming_specialization.devops', 1),
(8, 'databases', 'V1.programming_specialization.databases', 1),
(9, 'artificial_intelligence', 'V1.programming_specialization.artificial_intelligence', 1),
(10, 'systems_administration', 'V1.programming_specialization.systems_administration', 1),
(11, 'cybersecurity', 'V1.programming_specialization.cybersecurity', 1),
(12, 'data_analysis', 'V1.programming_specialization.data_analysis', 1),
(13, 'c', 'V1.programming_language.c', 2),
(14, 'java', 'V1.programming_language.java', 2),
(15, 'python', 'V1.programming_language.python', 2),
(16, 'c++', 'V1.programming_language.c++', 2),
(17, 'c#', 'V1.programming_language.c#', 2),
(18, 'visual_basic', 'V1.programming_language.visual_basic', 2),
(19, 'javascript', 'V1.programming_language.javascript', 2),
(20, 'php', 'V1.programming_language.php', 2),
(21, 'r', 'V1.programming_language.r', 2),
(22, 'groovy', 'V1.programming_language.groovy', 2),
(23, 'assembly_language', 'V1.programming_language.assembly_language', 2),
(24, 'sql', 'V1.programming_language.sql', 2),
(25, 'swift', 'V1.programming_language.swift', 2),
(26, 'go', 'V1.programming_language.go', 2),
(27, 'ruby', 'V1.programming_language.ruby', 2),
(28, 'matlab', 'V1.programming_language.matlab', 2),
(29, 'perl', 'V1.programming_language.perl', 2),
(30, 'objective_c', 'V1.programming_language.objective_c', 2),
(50, '1', 'V1.likert_standard.disagree', 4),
(51, '2', '', 4),
(52, '3', '', 4),
(53, '4', '', 4),
(54, '5', 'V1.likert_standard.agree', 4),
(55, 'acoustic', 'V1.music_genres.acoustic', 5),
(56, 'alternative', 'V1.music_genres.alternative', 5),
(57, 'blues', 'V1.music_genres.blues', 5),
(58, 'classical', 'V1.music_genres.classical', 5),
(59, 'country', 'V1.music_genres.country', 5),
(60, 'dance', 'V1.music_genres.dance', 5),
(61, 'electronic', 'V1.music_genres.electronic', 5),
(62, 'folk', 'V1.music_genres.folk', 5),
(63, 'hip_hop', 'V1.music_genres.hip_hop', 5),
(64, 'house', 'V1.music_genres.house', 5),
(65, 'indie', 'V1.music_genres.indie', 5),
(66, 'jazz', 'V1.music_genres.jazz', 5),
(67, 'lo_fi', 'V1.music_genres.lo_fi', 5),
(68, 'metal', 'V1.music_genres.metal', 5),
(69, 'pop', 'V1.music_genres.pop', 5),
(70, 'punk', 'V1.music_genres.punk', 5),
(71, 'reggae', 'V1.music_genres.reggae', 5),
(72, 'rock', 'V1.music_genres.rock', 5),
(73, 'soul', 'V1.music_genres.soul', 5),
(74, 'symphonic', 'V1.music_genres.symphonic', 5),
(75, 'synth_pop', 'V1.music_genres.synth_pop', 5),
(76, 'techno', 'V1.music_genres.techno', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`journey_id`,`question_id`),
  ADD KEY `answers_question_id_foreign` (`question_id`);

--
-- Indexes for table `audio_files`
--
ALTER TABLE `audio_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `audio_files_genre_index` (`genre`);

--
-- Indexes for table `enums`
--
ALTER TABLE `enums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journeys`
--
ALTER TABLE `journeys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `journeys_survey_id_foreign` (`survey_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questions_role_index` (`role`),
  ADD KEY `questions_enum_id_foreign` (`enum_id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section_x_question`
--
ALTER TABLE `section_x_question`
  ADD PRIMARY KEY (`section_id`,`question_id`),
  ADD KEY `section_x_question_question_id_foreign` (`question_id`);

--
-- Indexes for table `surveys`
--
ALTER TABLE `surveys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey_x_section`
--
ALTER TABLE `survey_x_section`
  ADD PRIMARY KEY (`survey_id`,`section_id`),
  ADD KEY `survey_x_section_section_id_foreign` (`section_id`);

--
-- Indexes for table `values`
--
ALTER TABLE `values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `values_enum_id_foreign` (`enum_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audio_files`
--
ALTER TABLE `audio_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `enums`
--
ALTER TABLE `enums`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `surveys`
--
ALTER TABLE `surveys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `values`
--
ALTER TABLE `values`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `answers_journey_id_foreign` FOREIGN KEY (`journey_id`) REFERENCES `journeys` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `journeys`
--
ALTER TABLE `journeys`
  ADD CONSTRAINT `journeys_survey_id_foreign` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_enum_id_foreign` FOREIGN KEY (`enum_id`) REFERENCES `enums` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `section_x_question`
--
ALTER TABLE `section_x_question`
  ADD CONSTRAINT `section_x_question_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `section_x_question_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `survey_x_section`
--
ALTER TABLE `survey_x_section`
  ADD CONSTRAINT `survey_x_section_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `survey_x_section_survey_id_foreign` FOREIGN KEY (`survey_id`) REFERENCES `surveys` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `values`
--
ALTER TABLE `values`
  ADD CONSTRAINT `values_enum_id_foreign` FOREIGN KEY (`enum_id`) REFERENCES `enums` (`id`) ON DELETE CASCADE;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `remove_abandoned_journeys` ON SCHEDULE AT '2021-02-08 20:53:21' ON COMPLETION PRESERVE ENABLE DO DELETE LOW_PRIORITY
            FROM `journeys`
            WHERE `journeys`.`finished_at` IS NULL
            AND `journeys`.`updated_at` < DATE_SUB(NOW(), INTERVAL 1 DAY)$$

DELIMITER ;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
