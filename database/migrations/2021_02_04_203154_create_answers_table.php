<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->text('value')->nullable();
            $table->bigInteger('errors')->nullable();
            $table->bigInteger('changes')->nullable();
            $table->bigInteger('commands')->nullable();
            $table->bigInteger('steps')->nullable();
            $table->bigInteger('plays')->nullable();
            $table->bigInteger('elapsed_time')->nullable();
            $table->timestamps();

            // foreign keys
            $table->uuid('journey_id');
            $table->unsignedBigInteger('question_id');

            // relationships
            $table->foreign('journey_id')
                ->references('id')
                ->on('journeys')
                ->onDelete('cascade');

            $table->foreign('question_id')
                ->references('id')
                ->on('questions')
                ->onDelete('cascade');

            // primary keys
            $table->primary([ 'journey_id', 'question_id' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
