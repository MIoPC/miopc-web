<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->enum('type', [
                'STRING',
                'NUMBER',
                'TEXT',
                'CHOICE',
                'LIKERT',
                'PUZZLE',
            ]);
            $table->enum('role', [
                'FAVORITE_GENRE',
                'UNBELOVED_GENRE',
            ])->nullable();
            $table->text('puzzle_structure')->nullable();
            $table->string('validation_rules');
            $table->boolean('editable')->default(true);

            // add indexes
            $table->index('role');

            // foreign keys
            $table->unsignedBigInteger('enum_id')->nullable();

            // relationships
            $table->foreign('enum_id')
                ->references('id')
                ->on('enums')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
