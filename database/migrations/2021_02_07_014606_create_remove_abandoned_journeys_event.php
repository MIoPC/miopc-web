<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;


class CreateRemoveAbandonedJourneysEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection()->getpdo()->exec("
            CREATE EVENT `remove_abandoned_journeys` ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 1 DAY ON COMPLETION PRESERVE DO
            DELETE LOW_PRIORITY
            FROM `journeys`
            WHERE `journeys`.`finished_at` IS NULL
            AND `journeys`.`updated_at` < DATE_SUB(NOW(), INTERVAL 1 DAY)
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection()->getpdo()->exec("
            DROP EVENT IF EXISTS `remove_abandoned_journeys`
        ");
    }
}
