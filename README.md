# Readme #

![Preview](./readme/images/preview.png)

The core FE + BE code base for Music Influence on Programmers' Cognition Assessment Tool

# Installation #

To run backend installation, run composer:
```bash
composer install
```

Torun frontend installation, run npm:
```bash
yarn install
```
or yarn
```bash
npm install
```

# Pre-checks #

## Access ##
Before running the application, make sure that Laravel has a right file owners:

```bash
sudo chown -R $USER:www-data storage
sudo chown -R $USER:www-data bootstrap/cache
```

and file permissions:
```bash
chmod -R 775 storage
chmod -R 775 bootstrap/cache
```


## Environment ##
Make sure that your `.env` file exists and is configured correctly. Especially, make sure that you have provided a right credentials for connection with database and you have generated your `APP_KEY`:

```bash
php artisan key:generate
```

## Compiling assets ##
Because compiled assets are not being pushed into repository, you need to compile them yourself. To do this, run either a yarn:

```bash
yarn development
```

or npm development command:
```bash
npm run development
```
