<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


class GlobalFunctionsProvider extends ServiceProvider
{
    /**
     *
     */
    static function getPath() {
        return base_path() . '/app/Functions/Global';
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once static::getPath(). '/__route.php';
        require_once static::getPath(). '/__asset.php';
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
