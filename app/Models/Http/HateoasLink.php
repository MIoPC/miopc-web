<?php

namespace App\Models\Http;

class HateoasLink
{
    /**
     * Request types
     */
    const GET = 'GET';
    const POST = 'POST';
    const PATCH = 'PATCH';
    const PUT = 'PUT';
    const DELETE = 'DELETE';

    /**
     * Represents link URL
     *
     * @var string
     */
    public $href;

    /**
     * Represents link method
     *
     * @var string
     */
    public $type;

    /**
     * Represents link parameters
     *
     * @var string
     */
    public $parameters;

    /**
     * Create a new Link instance.
     *
     * @return void
     */
    public function __construct($href, $type = null, $parameters = null)
    {
        $this->href = $href;
        $this->type = $type ?? static::GET;
        $this->parameters = $parameters ?? new class {};
    }
}
