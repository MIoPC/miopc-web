<?php

namespace App\Models\Eloquent\Traits;

trait AttributesMutator
{
    /**
     *
     *
     * @return
     */
    protected function _collect($type) {
        if (!isset($this->{$type})) return [];
        return collect($this->{$type})->flip()->map(function ($value, $key) {
            return $this->{$key};
        })->toArray();
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        $mutators = [
            // Translations
            'translatable' => function($value) {
                return __($this->translationPath . $value);
            },
            // URLs generation
            'urls' => function($value) {
                return url($value) . "?v=" . config('resources.version.database');
            },
        ];

        foreach ($mutators as $type => $mutator) {
            if (isset($this->{$type}) && in_array($key, $this->{$type})) {
                if (isset($this[$key])) {
                    if ($this[$key]) return $mutator($this[$key]);
                }
            }
        }

        return parent::__get($key);
    }

    /**
     *
     *
     * @return
     */
    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            $this->_collect('translatable'),
            $this->_collect('urls')
        );
    }
}
