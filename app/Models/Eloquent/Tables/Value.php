<?php

namespace App\Models\Eloquent\Tables;

use Illuminate\Database\Eloquent\Model;
use App\Models\Eloquent\Tables\Enum;
use App\Models\Eloquent\Traits\AttributesMutator;

class Value extends Model
{
    /** */
    use AttributesMutator;

    /**
     *
     */
    protected $translatable = [ 'label' ];
    protected $translationPath = 'database/values/';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the enum for the given value.
     */
    public function enum()
    {
        return $this->belongsTo(Enum::class);
    }
}
