<?php

namespace App\Models\Eloquent\Tables;

use Illuminate\Database\Eloquent\Model;
use App\Models\Eloquent\Tables\Section;
use App\Models\Eloquent\Tables\Answer;
use App\Models\Eloquent\Tables\Enum;
use App\Models\Eloquent\Traits\AttributesMutator;

class Question extends Model
{
    /** */
    use AttributesMutator;

    /**
     *
     */
    protected $translatable = [ 'title' ];
    protected $translationPath = 'database/questions/';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'editable' => 'boolean',
        'puzzle_structure' => 'array',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the sections for the given question.
     */
    public function sections()
    {
        return $this
            ->belongsToMany(Section::class, 'section_x_question')
            ->withPivot('position');
    }

    /**
     * Get the enum for the given question.
     */
    public function enum()
    {
        return $this->belongsTo(Enum::class);
    }

    /**
     * Get the answers for the given question.
     */
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
