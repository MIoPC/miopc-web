<?php

namespace App\Models\Eloquent\Tables;

use Illuminate\Database\Eloquent\Model;
use App\Models\Eloquent\Traits\AttributesMutator;

class AudioFile extends Model
{
    /** */
    use AttributesMutator;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     *
     */
    protected $urls = [ 'path' ];
}
