<?php

namespace App\Models\Eloquent\Tables;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use App\Models\Eloquent\Tables\Answer;
use App\Models\Eloquent\Tables\Survey;
use App\Models\Eloquent\Tables\Section;

class Journey extends Model
{
    /**
     * Get the answers for the given journey.
     */
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    /**
     * Get the survey for the given journey.
     */
    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }
}
