<?php

namespace App\Models\Eloquent\Tables;

use Illuminate\Database\Eloquent\Model;
use App\Models\Eloquent\Tables\Answer;
use App\Models\Eloquent\Tables\Section;
use App\Models\Eloquent\Tables\Journey;
use App\Models\Eloquent\Tables\Question;
use App\Models\Eloquent\Traits\AttributesMutator;

class Survey extends Model
{
    /** */
    use AttributesMutator;

    /**
     *
     */
    protected $translatable = [ 'title', 'description' ];
    protected $translationPath = 'database/surveys/';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the answers for the given survey.
     */
    public function answers()
    {
        return $this->hasManyThrough(Answer::class, Journey::class);
    }

    /**
     * Get the answers for the given survey.
     */
    public function questions()
    {
        return $this->hasManyThrough(Question::class, Section::class);
    }

    /**
     * Get the sections for the given survey.
     */
    public function sections()
    {
        return $this
            ->belongsToMany(Section::class, 'survey_x_section')
            ->withPivot('position');
    }

    /**
     * Get the journeys for the given survey.
     */
    public function journeys()
    {
        return $this->hasMany(Journey::class);
    }
}
