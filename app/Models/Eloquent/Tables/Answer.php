<?php

namespace App\Models\Eloquent\Tables;

use Illuminate\Database\Eloquent\Model;
use App\Models\Eloquent\Tables\Journey;
use App\Models\Eloquent\Tables\Survey;
use App\Models\Eloquent\Tables\Question;

class Answer extends Model
{
    /**
     * Get the journey for the given answer.
     */
    public function journey()
    {
        return $this->belongsTo(Journey::class);
    }

    /**
     * Get the question for the given answer.
     */
    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
