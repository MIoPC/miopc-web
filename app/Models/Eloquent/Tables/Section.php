<?php

namespace App\Models\Eloquent\Tables;

use Illuminate\Database\Eloquent\Model;
use App\Models\Eloquent\Tables\Survey;
use App\Models\Eloquent\Tables\Question;
use App\Models\Eloquent\Tables\Journey;
use App\Models\Eloquent\Traits\AttributesMutator;

class Section extends Model
{
    /** */
    use AttributesMutator;

    /**
     *
     */
    protected $translatable = [ 'title', 'subtitle', 'description' ];
    protected $translationPath = 'database/sections/';

    /**
     *
     */
    protected $urls = [ 'background_image' ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the surveys for the given section.
     */
    public function surveys()
    {
        return $this
            ->belongsToMany(Survey::class, 'survey_x_section')
            ->withPivot('position');
    }

    /**
     * Get the questions for the given section.
     */
    public function questions()
    {
        return $this
            ->belongsToMany(Question::class, 'section_x_question')
            ->withPivot('position');
    }
}
