<?php

namespace App\Models\Eloquent\Tables;

use Illuminate\Database\Eloquent\Model;
use App\Models\Eloquent\Tables\Question;
use App\Models\Eloquent\Tables\Value;

class Enum extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the questions for the given enum.
     */
    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    /**
     * Get the values for the given enum.
     */
    public function values()
    {
        return $this->hasMany(Value::class);
    }
}
