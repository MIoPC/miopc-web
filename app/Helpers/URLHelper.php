<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Route;

class URLHelper
{
    /**
     *
     */
    const SOURCE_FILE = 'routes';

    /**
     * Resolve given slug for current language
     *
     * @param string $url
     * @return string
     */
    public static function resolve($url, $locale) {
        return __(static::SOURCE_FILE . ".$url", [], $locale);
    }
}
