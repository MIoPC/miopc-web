<?php

namespace App\Helpers;

class SharedValues
{
    /**
     * Stores path to the constant folder,
     * where all shared values across FE and BE are stored.
     *
     * @var string
     */
    public static $FOLDER_LOCATION = __DIR__ . '/../../shared';

    /**
     * Object holding all cached files.
     *
     * @var array
     */
    protected static $CACHED_FILES = [];

    /**
     * Returns JSON file representing shared constant
     * or throws error if no file was found.
     *
     * @param string $file
     * @return object
     */
    public static function get($file) {
        try {
            if (isset(static::$CACHED_FILES[$file])) {
                $data = static::$CACHED_FILES[$file];
            } else {
                $path = static::$FOLDER_LOCATION . "/$file.json";
                $data = json_decode(file_get_contents($path), true);
                static::$CACHED_FILES[$file] = $data;
            }
            return $data;
        } catch (\Exception $error) {
            throw $error;
            throw new Exception("Couldn't find any constant file named '$file'. Full error: $error->getMessage()");
        }
    }

    /**
     * Helper function for getting HATEOAS names
     *
     * @param string $link
     * @return object
     */
    public static function hateoas($link) {
        return static::get('hateoas')[$link];
    }
}
