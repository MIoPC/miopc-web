<?php

use App\Http\Middleware\LocaleManager;

/**
 * Generate a URL to a named route.
 *
 * @param  string  $name
 * @param  array   $parameters
 * @param  bool   $prefix
 * @param  string  $locale
 * @param  bool    $absolute
 * @return string
 */
function __asset($path, $locale = null)
{
    $locale = $locale ?? app()->getLocale();
    return asset("locale/$locale/$path");
}
