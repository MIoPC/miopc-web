<?php

use App\Http\Middleware\LocaleManager;

/**
 * Generate a URL to a named route.
 *
 * @param  string  $name
 * @param  array   $parameters
 * @param  bool   $prefix
 * @param  string  $locale
 * @param  bool    $absolute
 * @return string
 */
function __route($name, $parameters = [], $prefix = true, $locale = null, $absolute = true)
{
    $locale = $locale ?? app()->getLocale();
    if ($prefix) $name = "$locale.$name";
    $parameters = array_merge(
        [ LocaleManager::ROUTE_KEY => $locale ],
        $parameters
    );
    return route($name, $parameters, $absolute);
}
