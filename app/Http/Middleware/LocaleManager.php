<?php

namespace App\Http\Middleware;

use Closure;

class LocaleManager
{
    const SESSION_KEY = 'lm_lang';
    const ROUTE_KEY = 'lm_lang';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get locale from the URL
        $locale = $request->route()->parameter(static::ROUTE_KEY);

        // Get locale from session
        $appLocale = session(static::SESSION_KEY);

        // Save locale in the session only when it's declared within a URL and it differs
        if ($locale && $locale !== $appLocale) {
            session([ static::SESSION_KEY => $locale ]);
            $appLocale = $locale;
        }

        // Set application locale based on resolved locales
        app()->setLocale($appLocale);

        // Continue with request
        return $next($request);
    }
}
