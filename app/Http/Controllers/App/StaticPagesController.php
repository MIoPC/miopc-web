<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StaticPagesController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Pages Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles all static pages
    |
    */

    /**
     * Show the application's homepage.
     *
     * @return \Illuminate\Http\Response
     */
    public function homepage()
    {
        return view('pages.static.homepage.default', [
            'hasJourney' => !!session('journey_id'),
            'hasCompleted' => !!session('completed_journey'),
        ]);
    }

    /**
     * Show the thank-you page.
     *
     * @return \Illuminate\Http\Response
     */
    public function thankYou()
    {
        if (!session('completed_journey')) {
            return redirect(__route('homepage', [], false));
        }
        return view('pages.static.thank-you.default');
    }

    /**
     * Show the application's 404 page.
     *
     * @return \Illuminate\Http\Response
     */
    public function show404()
    {
        return redirect(__route('homepage', [], false));
    }
}
