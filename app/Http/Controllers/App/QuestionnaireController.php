<?php

namespace App\Http\Controllers\App;

use App\Helpers\SharedValues;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Questionnaire Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles all request to the questionnaire page
    |
    */

    /**
     * Show the questionnaire page.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // Get locale
        $locale = app()->getLocale();

        // Return view with respected data
        return view('pages.journeys.questionnaire.default', [
            'jsConfig' => [
                'id' => SharedValues::get('apps')['ASSESSMENT_TOOL'],
                'lang' => $locale,
                'endpoint' => route('dal.v1.questionnaire.latest'),
            ],
        ]);
    }
}
