<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Middleware\LocaleManager;

class LanguageController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Language Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles all language related calls
    |
    */

    /**
     * Resolves user's locales.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function resolve(Request $request)
    {
        $locales = config('app.supported_locales');
        $locale = session(LocaleManager::SESSION_KEY) ?? $request->getPreferredLanguage($locales);
        app()->setLocale($locale);
        return redirect()->route('homepage', [ LocaleManager::ROUTE_KEY => $locale ]);
    }
}
