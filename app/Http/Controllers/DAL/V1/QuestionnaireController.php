<?php

namespace App\Http\Controllers\DAL\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\SharedValues;
use App\Models\Http\HateoasLink;
use App\Models\Eloquent\Tables\Survey;
use App\Models\Eloquent\Tables\Section;
use App\Models\Eloquent\Tables\Answer;
use App\Models\Eloquent\Tables\Journey;
use App\Models\Eloquent\Tables\AudioFile;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class QuestionnaireController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Questionnaire DAL Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles all request to the questionnaire data access layer
    |
    */

    /**
     *
     * @todo RESTORE THIS INFO IN A NICER WAY
     * @return void
     */
    protected function _getRandomSurveyType() {
        $types = SharedValues::get('survey-types');
        return $types[array_rand($types)];
    }

    /**
     *
     *
     * @return void
     */
    protected function _getSessionData() {
        // Try to get info about the survey and session from session
        $surveyId = session('assigned_survey');
        $sectionId = session('last_visited_section');
        $journeyId = session('journey_id');
        $isVerified = !!session('is_verified');

        // If not survey was found, assign to one
        if (!$surveyId) {
            $surveyType = $this->_getRandomSurveyType();
            $survey = Survey::where('type', $surveyType)->first();
            $section = $survey
                ->sections()
                ->orderBy('survey_x_section.position', 'asc')
                ->first();
            $surveyId = $survey->id;
            $sectionId = $section->id;
            session([ 'assigned_survey' => $surveyId ]);
            session([ 'last_visited_section' => $sectionId ]);
        }

        // Return retrieved
        return [
            'journeyId' => $journeyId,
            'surveyId' => $surveyId,
            'sectionId' => $sectionId,
            'isVerified' => $isVerified,
        ];
    }

    /**
     * Get the latest journey
     *
     * @return \Illuminate\Http\Response
     */
    public function latest()
    {
        // Get session data
        $sessionData = $this->_getSessionData();

        // Resolving survey type
        $survey = Survey::find($sessionData['surveyId']);

        // Define a JSON response
        $response = array_merge([
            'isVerified' => $sessionData['isVerified'],
            'verificationKey' => config('recaptcha.api_site_key'),
            'surveyType' => $survey->type,
        ], $sessionData);

        // Add applicable HATEOAS links
        $links = new class {};
        $links->{SharedValues::hateoas('GET_SURVEY_SECTION')} = new HateoasLink(
            route('dal.v1.questionnaire.get-section', [
                $sessionData['surveyId'],
                $sessionData['sectionId'],
            ]),
            HateoasLink::POST
        );
        $links->{SharedValues::hateoas('VERIFY_JOURNEY')} = new HateoasLink(
            route('dal.v1.questionnaire.verify'),
            HateoasLink::POST
        );

        // Return the JSON response
        $response['_links'] = $links;
        return response()->json($response);
    }


    /**
     * Verify the user and create a journey object ofr them
     *
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request) {

        // Validate if ReCaptcha token is valid
        $validator = Validator::make(request()->all(), [
            'token' => recaptchaRuleName(),
        ]);

        // If validator fails, throw 403 error
        if ($validator->fails()) {
            return response()->json([
                'errorCode' => 'RECAPTCHA_TOKEN_MISMATCH',
                'errorMessage' => 'Server could not verify that given ReCaptcha token is valid.',
            ], 403);
        }

        // Otherwise get session data
        $sessionData = $this->_getSessionData();
        $surveyId = $sessionData['surveyId'];

        // And create a journey
        $journey = Journey::create([
            'survey_id' => $surveyId,
            'type' => $request->get('isMobile') ? 'mobile' : 'desktop',
        ]);
        session([
            'journey_id' => $journey->id,
            'is_verified' => true,
        ]);

        // Return OK response
        return response()->json([
            'status' => 'SUCCESS',
            'message' => 'Journey created successfully.'
        ]);
    }


    /**
     * Return questionnaire section
     *
     * @return \Illuminate\Http\Response
     */
    public function getSurveySection($surveyId, $sectionId)
    {
        // Define a JSON response
        $response = [];

        // Get section based on session data
        $section = $response['section'] = Section::with([
            'questions' => function($query) {
                $query->orderBy('position');
            },
            'questions.enum',
            'questions.enum.values',
        ])->find($sectionId);

        // Gather submitted answers based on a journey id
        $journeyId = $this->_getSessionData()['journeyId'];
        $answers = collect();
        if ($journeyId) {
            $ids = $section->questions->map(function($item) { return $item->id; });
            $answers = Answer::where('journey_id', $journeyId)
                ->whereIn('question_id', $ids)
                ->select('value', 'question_id')
                ->get();
        }
        foreach ($section->questions as $question) {
            $value = null;
            $answer = $answers->where('question_id', $question->id)->first();
            if ($answer) $value = $answer->value;
            $question->value = $value;

            // Add Hateoas links
            $links = new class {};
            $links->{SharedValues::hateoas('SUBMIT_ANSWER')} = new HateoasLink(
                route('dal.v1.questionnaire.submit-answer', [ $question->id ]),
                HateoasLink::POST
            );
            $question->_links = $links;
        }

        // Get assets
        $survey = Survey::find($surveyId);
        if ($section->type === 'PUZZLE') {
            $journey = Journey::find($journeyId);
            $type = $journey->survey->type;
            if ($type !== 'SILENCE') {
                $role = [
                    'FAVORITE_MUSIC' => 'FAVORITE_GENRE',
                    'UNBELOVED_MUSIC' => 'UNBELOVED_GENRE',
                ][$type];
                $genre = $journey->answers()
                    ->whereHas('question', function($query) use ($role) {
                        $query->where('role', $role);
                    })
                    ->first()
                    ->value;
                $files = AudioFile::where('genre', $genre)->get();
                $response['music'] = $files->isEmpty() ? null : $files;
            }
        }

        // Get section position
        $section->position = $position = $section
            ->surveys()
            ->where('id', $surveyId)
            ->first()
            ->pivot
            ->position;

        // Get next an previous sections id
        $siblings = $survey->sections()
            ->wherePivotIn('position', [
                $position - 1,
                $position + 1,
            ])
            ->get();

        // Add applicable HATEOAS links
        $links = new class {};

        // Submit link
        $links->{SharedValues::hateoas('SUBMIT_SURVEY_SECTION')} = new HateoasLink(
            route('dal.v1.questionnaire.submit-section', [
                $surveyId,
                $sectionId,
            ]),
            HateoasLink::PATCH
        );

        // Finish link
        $links->{SharedValues::hateoas('FINISH_JOURNEY')} = new HateoasLink(
            route('dal.v1.questionnaire.finish'),
            HateoasLink::POST
        );

        // Pagination
        foreach ([
            SharedValues::hateoas('PAGINATION_BACK') => $siblings->where('pivot.position', $position - 1)->first(),
            SharedValues::hateoas('PAGINATION_NEXT') => $siblings->where('pivot.position', $position + 1)->first(),
        ] as $name => $section) {
            if ($section) {
                if (!(
                    $section->type === 'PUZZLE' &&
                    $name === SharedValues::hateoas('PAGINATION_BACK')
                )) {
                    $links->{$name} = new HateoasLink(
                        route('dal.v1.questionnaire.get-section', [ $surveyId, $section->id ]),
                        HateoasLink::POST
                    );
                }
            }
        }

        // Save info about last seen section
        session([ 'last_visited_section' => intval($sectionId) ]);

        // Return the JSON response
        $response['_links'] = $links;
        return response()->json($response);
    }


    /**
     * Saves answers to given section in a database
     *
     * @return \Illuminate\Http\Response
     */
    public function submitSection(Request $request, $surveyId, $sectionId) {

        // Allow database changes only by verified users
        $journeyId = session('journey_id');

        // If user is not verified, return
        if (!$journeyId) {
            return response()->json([
                'errorCode' => 'JOURNEY_NOT_AUTHENTICATED',
                'errorMessage' => 'There is no related journey for a given request',
            ], 401);
        }

        // If user submitted answers before, delete them (since upsert is not supported by Laravel yet)
        $data = collect($request->get('answers'));
        if ($journeyId) {
            $ids = $data->map(function($row) { return $row['id']; });
            $answers = Answer::where('journey_id', $journeyId)
                ->whereIn('question_id', $ids)
                ->delete();
        }

        // Make a bulk insert into the DB
        $now = Carbon::now();
        $toInsert = $data->map(function($row) use ($journeyId, $now) {
            return [
                'value' => $row['value'],
                'question_id' => $row['id'],
                'journey_id' => $journeyId,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        });
        Answer::insert($toInsert->toArray());

        // Return OK response
        return response()->json([
            'status' => 'SUCCESS',
            'message' => 'Answers registered successfully.'
        ]);
    }

    /**
     * Saves answer in a database
     *
     * @return \Illuminate\Http\Response
     */
    public function submitAnswer(Request $request, $questionId) {

         // Allow database changes only by verified users
         $journeyId = session('journey_id');

         // If user is not verified, return
         if (!$journeyId) {
             return response()->json([
                 'errorCode' => 'JOURNEY_NOT_AUTHENTICATED',
                 'errorMessage' => 'There is no related journey for a given request',
             ], 401);
         }

        // Prepare data to insert
        $now = Carbon::now();
        $toInsert = array_merge(
            $request->except([
               '_method',
           ]), [
            'question_id' => $questionId,
            'journey_id' => $journeyId,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        Answer::insert($toInsert);

        // Return OK response
        return response()->json([
            'status' => 'SUCCESS',
            'message' => 'Answer registered successfully.'
        ]);
    }

    /**
     * Finishes the survey
     *
     * @return \Illuminate\Http\Response
     */
    public function finish(Request $request) {

        // Mark journey as ready
        $journeyId = session('journey_id');
        $journey = Journey::find($journeyId);
        $journey->finished_at = Carbon::now();
        $journey->save();

        // Remove all session related data
        session()->forget([
            'assigned_survey',
            'last_visited_section',
            'journey_id',
            'is_verified',
        ]);

        // Mark user as finished
        session([ 'completed_journey' => true ]);

        // Return OK response
        return response()->json([
            'redirect' => __route('thank-you'),
            'status' => 'SUCCESS',
            'message' => 'Answer registered successfully.'
        ]);
   }
}
