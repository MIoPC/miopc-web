<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Resources version
    |--------------------------------------------------------------------------
    |
    | To prevent unwanted caching of images, scripts and styles
    | this array contains current versions of every type of resources.
    |
    */
    'version' =>  [
        'favicon' => env('RESOURCES_VERSION_FAVICON', time()),
        'images' => env('RESOURCES_VERSION_IMAGES', time()),
        'css' => env('RESOURCES_VERSION_CSS', time()),
        'js' => env('RESOURCES_VERSION_JS', time()),
        'database' => env('RESOURCES_VERSION_DATABASE', time()),
    ]
];
