const mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const config = require('./webpack.config')

const paths = {
    input: 'resources',
    output: 'public/compiled'
}

mix.setPublicPath('public');
mix.webpackConfig(config)
mix.sass(`${paths.input}/sass/app.scss`, `${paths.output}/css`)
    .react(`${paths.input}/js/app.js`, `${paths.output}/js`)
