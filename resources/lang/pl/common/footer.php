<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Footer Language Lines
    |--------------------------------------------------------------------------
    */

    'logo' => [
        'alt' => 'Narzędzie Ewaluacyjne MIoPC',
    ],

    'language-switch' => [
        'title' => 'Zmień język'
    ],

    'note' => 'Aplikacja została stworzona na potrzeby pracy dyplomowej zatytuowanej "Zaprojektowanie i&nbsp;implementacja oprogramowania do testowania wpływu muzyki na zdolności kognitywne w&nbsp;zawodzie programisty" prowadzonej w&nbsp;ramach studiów II stopnia na <a href="https://www.wszib.edu.pl/" class="text-underline" target="_blank">Wyższej Szkole Zarządzania i&nbsp;Bankowości w&nbsp;Krakowie</a>.',

];
