<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Questions Language Lines (V1)
    |--------------------------------------------------------------------------
    */

    /**
     * Programming experience
     */
    'programming_experience' => [
        'title' => 'Od ilu lat zajmujesz się programowaniem?',
    ],

    /**
     * IT seniority
     */
    'it_seniority' => [
        'title' => 'Ile lat pracujesz w branży IT?',
    ],

    /**
     * Programming specialization
     */
    'programming_specialization' => [
        'title' => 'Wybierz specjalizaję, która najbardziej do Ciebie pasuje:',
    ],

    /**
     * Programming language proficiency
     */
    'programming_language_proficiency' => [
        'title' => 'W ilu językach programowania jesteś biegły?',
    ],

    /**
     * Programming language
     */
    'programming_language' => [
        'title' => 'Jakiego języka programowania używasz na co dzień?',
    ],

        /**
     * Procedural paradigm proficiency
     */
    'procedural_paradigm_proficiency' => [
        'title' => 'Znam i rozumiem paradygmat programowania proceduralnego.',
    ],

    /**
     * Functional paradigm proficiency
     */
    'functional_paradigm_proficiency' => [
        'title' => 'Znam i rozumiem paradygmat programowania funkcyjnego.',
    ],

    /**
     * Logical paradigm proficiency
     */
    'logical_paradigm_proficiency' => [
        'title' => 'Znam i rozumiem paradygmat programowania logicznego.',
    ],

    /**
     * Object-oriented paradigm proficiency
     */
    'object_oriented_paradigm_proficiency' => [
        'title' => 'Znam i rozumiem paradygmat programowania obiektowego.',
    ],

    /**
     * Favorite music genre
     */
    'favorite_music_genre' => [
        'title' => 'Jaki jest Twój ulubiony gatunek muzyczny?',
    ],

    /**
     * Unbeloved music genre
     */
    'unbeloved_music_genre' => [
        'title' => 'Który gatunek muzyczny jest przez Ciebie najmniej lubiany?',
    ],

    /**
     * Music listening when working
     */
    'music_listening_when_working' => [
        'title' => 'Często słucham muzyki podczas wykonywania swoich obowiązków.',
    ],

    /**
     * Music listening when programming
     */
    'music_listening_when_programming' => [
        'title' => 'Często słucham muzyki podczas programowania.',
    ],

    /**
     * Music stimulus effect
     */
    'music_stimulus_effect' => [
        'title' => 'Muzyka działa na mnie stymulująco podczas pracy.',
    ],

    /**
     * Audiophile
     */
    'audiophile' => [
        'title' => 'Jestem audiofilem.',
    ],

    /**
     * Music mood effect
     */
    'music_mood_effect' => [
        'title' => 'Muzyka pozytywnie wpływa na moje samopoczucie.',
    ],

    /**
     * Music education
     */
    'music_education' => [
        'title' => 'Odebrałem dobre wykształcenie muzyczne.',
    ],

    /**
     * Music talent
     */
    'music_talent' => [
        'title' => 'Posiadam talent muzyczny.',
    ],

    /**
     * Self-esteem
     */
    'self_esteem' => [
        'title' => 'Jestem zadowolony z tego, w jaki sposób rozwiązałem minigrę.',
    ],

    /**
     * Difficulty level
     */
    'difficulty_level' => [
        'title' => 'Musiałem bardzo się postarać, aby rozwiązać minigrę.',
    ],

    /**
     * Focus (music)
     */
    'focus_music' => [
        'title' => 'Muzyka w tle nie pozwalała mi się skupić.',
    ],

    /**
     * Performance (music)
     */
    'performance_music' => [
        'title' => 'Uzyskałbym lepszy wynik, gdybym nie słuchał muzyki.',
    ],

    /**
     * Focus (silence)
     */
    'focus_silence' => [
        'title' => 'Brak muzyki nie pozwalał mi się skupić.',
    ],

    /**
     * Performance (silence)
     */
    'performance_silence' => [
        'title' => 'Uzyskałbym lepszy wynik, gdybym słuchał muzyki.',
    ],

    /**
     * Music match (music)
     */
    'music_match' => [
        'title' => 'Podobała mi się muzyka, którą słyszałem w tle.',
    ],
];
