<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sections Language Lines (V1)
    |--------------------------------------------------------------------------
    */

    /**
     * IT survey
     */
    'it_survey' => [
        'title' => 'Programowanie',
        'subtitle' => 'Część 1:',
        'description' => 'Badanie składa się z&nbsp;5 części. Pierwsza z&nbsp;nich skupia się na Twoim dotychczasowym doświadczeniu w&nbsp;branży IT oraz programowaniu.',
    ],

    /**
     * Music Survey
     */
    'music_survey' => [
        'title' => 'Muzyka',
        'subtitle' => 'Część 2:',
        'description' => 'Ta część ankiety składa się z&nbsp;pytań dotyczących Twoich preferencji oraz stosunku do muzyki, a&nbsp;także Twojego wykształcenia w&nbsp;na tym polu.',
    ],

    /**
     * Tutorial
     */
    'tutorial' => [
        'title' => 'Samouczek',
        'subtitle' => 'Część 3:',
        'description' => '
            <p class="pb--2">Zaraz zostanie przedstawiony Ci koncpet minigry, która będzie użyta w&nbsp;następnych częściach ankiety. Postaraj się proszę o jak najlepsze jej zrozumienie, ponieważ stanowi ona <u>sedno tego badania</u>.</p>
            <p>Jest to również ostatni moment na to, aby skorygować swoje odpowiedzi z&nbsp;poprzednich sekcji - po zakończeniu tego samouczka, <u>nie będzie to bowiem możliwe</u>.</p>
        ',
    ],

    /**
     * Puzzle
     */
    'puzzle' => [
        'title' => 'Minigra',
        'subtitle' => 'Część 4:',
        'description' => '
            <p class="pb--2"></p>
            <p></p>
        ',
        'description' => '
            <p class="pb--2">Oto główna część badania. Zostaniesz zaraz poproszony o&nbsp;rozwiązanie 6&nbsp;poziomów minigry. Postaraj się zrobić to najlepiej, jak potrafisz, mając na uwadze liczbę użytych komend, czas wykonania zadania oraz wydajność kodu.</p>
            <p>Po każdorazowym rozwiązaniu poziomu, automatycznie zostanie wgrany kolejny, a&nbsp;Twój wynik zostanie zapisany. Nie będziesz mieć możliwości powtórzenia żadnego z&nbsp;poziomów.</p>
        ',
    ],

    /**
     * Feedback (generic)
     */
    'feedback' => [
        'title' => 'Twoja opinia',
        'subtitle' => 'Część 5:',
        'description' => 'To już ostatnia część tego badania. Na koniec chciałbym zadać Ci kilka pytań na temat Twoich wrażeń dotyczących rozwiązywania minigry.',
    ],

];
