<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Values Language Lines (V1)
    |--------------------------------------------------------------------------
    */

    /**
     * Programming specialization
     */
    'programming_specialization' => [
        'backend' => 'Backend',
        'frontend' => 'Frontend',
        'fullstack' => 'Fullstack',
        'mobile_apps' => 'Aplikacje mobilne',
        'desktop_apps' => 'Aplikacje desktopowe',
        'software_testing' => 'Testowanie oprogramowania',
        'devops' => 'DevOps',
        'databases' => 'Bazy danych',
        'artificial_intelligence' => 'Sztuczna inteligencja',
        'systems_administration' => 'Administracja systemami informatycznymi',
        'cybersecurity' => 'Cyberbezpieczeństwo',
        'data_analysis' => 'Eksploracja i analiza danych',
    ],

    /**
     * Programming Language
     */
    'programming_language' => [
        'assembly_language' => 'Assembler',
    ],

    /**
     * Likert standard scale
     */
    'likert_standard' => [
        'disagree' => 'Nie zgadzam się',
        'agree' => 'Zgadzam się',
    ],

    /**
     * Music genres
     */
    'music_genres' => [
        'acoustic' => 'Akustyczna',
        'alternative' => 'Alternatywny rock',
        'classical' => 'Klasyczna',
        'electronic' => 'Elektroniczna',
        'symphonic' => 'Symfoniczna',
    ],
];
