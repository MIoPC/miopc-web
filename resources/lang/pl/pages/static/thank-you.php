<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Thank-you Static Page Language Lines
    |--------------------------------------------------------------------------
    */

    'seo' => [
        'title' => 'Dziękuję!',
    ],

    'hero' => [
        'part-1' => "Twoje odpowiedzi zostały",
        'part-2' => "zapisane!",
    ],

    'description' => "Serdecznie dziękuję Ci za poświęcony czas i&nbsp;wzięcie udział w&nbsp;badaniu. Twoje odpowiedzi zostały zapisane i&nbsp;zostaną użyte w&nbsp;pracy dyplomowej badającej wpływ muzyki na zdolności kognitywne programisty.",

    'cta' => 'POWRÓT',

];
