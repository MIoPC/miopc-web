<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Homepage Static Page Language Lines
    |--------------------------------------------------------------------------
    */

    'seo' => [
        'title' => 'Narzędzie Ewaluacyjne MIoPC',
    ],

    'hero' => [
        'part-1' => 'W&nbsp;ramach swojej pracy magisterskiej badam wpływ muzyki na',
        'part-2' => "zdolności kognitywne",
        'part-3' => 'programistów.',
    ],

    'description' => "Jeśli jesteś deweloperem, testerem lub analitykiem danych, będę zobowiązany, jeśli zechcesz anonimowo odpowiedzieć na kwestionariusz i&nbsp;rozwiązać kilka zadań. Nie powinno to zająć więcej niż 15 do 25 minut.",

    'cta' => [
        'anonymous' => 'ROZPOCZNIJ BADANIE',
        'verified' => 'KONTYNUUJ BADANIE',
        'finished' => 'ZACZNIJ PONOWNIE',
    ],

];
