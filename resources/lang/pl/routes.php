<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Routes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to create dynamic, multilingual URLs.
    |
    */

    // Questionnaire journey
    'questionnaire' => '/badanie',

    // Finished journey
    'thank-you' => '/dziekuje',
];
