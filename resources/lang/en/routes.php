<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Routes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to create dynamic, multilingual URLs.
    |
    */

    // Questionnaire journey
    'questionnaire' => '/survey',

    // Finished journey
    'thank-you' => '/thank-you',
];
