<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Footer Language Lines
    |--------------------------------------------------------------------------
    */

    'logo' => [
        'alt' => 'MIoPC Assessment Tool',
    ],

    'language-switch' => [
        'title' => 'Change language'
    ],

    'note' => 'The application has been created as a part of a master\'s thesis entitled "Designing and implementing a software for testing music impact on cognitive skills in the programming profession" conducted at the <a href="https://www.wszib.edu.pl/" class="text-underline" target="_blank">University of Management and Banking in Kraków</a>.',

];
