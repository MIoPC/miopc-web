<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Thank-you Static Page Language Lines
    |--------------------------------------------------------------------------
    */

    'seo' => [
        'title' => 'Thank you!',
    ],

    'hero' => [
        'part-1' => "Your answers have been",
        'part-2' => "saved!",
    ],

    'description' => "Thank you for your time. Your answers have been anonymously recorded and will be used in a thesis examining the effects of music on cognitive abilities in the programming profession.",

    'cta' => 'GO BACK',

];
