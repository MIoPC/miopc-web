<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Homepage Static Page Language Lines
    |--------------------------------------------------------------------------
    */

    'seo' => [
        'title' => 'MIoPC Assessment Tool',
    ],

    'hero' => [
        'part-1' => "As a&nbsp;part of my master's degree thesis, I'm doing research on how music can impact",
        'part-2' => "programmers' cognition",
        'part-3' => 'skills.',
    ],

    'description' => "If you work as a developer, tester, or data scientist, I&nbsp;would be obliged if you would like to anonymously answer my questionnaire and resolve few puzzles. This shouldn't take more than 15 to 25 minutes.",

    'cta' => [
        'anonymous' => 'START SURVEY',
        'verified' => 'RESUME SURVEY',
        'finished' => 'START AGAIN',
    ],

];
