<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sections Language Lines (V1)
    |--------------------------------------------------------------------------
    */

    /**
     * IT survey
     */
    'it_survey' => [
        'title' => 'Programming',
        'subtitle' => 'Part 1:',
        'description' => 'This study consists of 5 parts. The first one focuses on your previous experience with the IT industry and programming activities.',
    ],

    /**
     * Music Survey
     */
    'music_survey' => [
        'title' => 'Music',
        'subtitle' => 'Part 2:',
        'description' => 'This part of the survey asks you about your preferences in music, as well as your education in this field.',
    ],

    /**
     * Tutorial
     */
    'tutorial' => [
        'title' => 'Tutorial',
        'subtitle' => 'Part 3:',
        'description' => '
            <p class="pb--2">You will be now introduced to a&nbsp;concept of the mini-game that is used in the next parts of the survey. Please, try to comprehend it as much as possible, as it is at the <u>core part of the study</u>.</p>
            <p>This is also the last moment to correct your answers from the previous sections - after completing this tutorial, you <u>will not be able</u> to do so.</p>
        ',
    ],

    /**
     * Puzzle
     */
    'puzzle' => [
        'title' => 'Assignment',
        'subtitle' => 'Part 4:',
        'description' => '
            <p class="pb--2">This is the core part of the research. You will be asked to resolve 6 levels of the mini-game. Please, try to do this the best you can, having in mind the number of used commands, elapsed time, and code efficiency.</p>
            <p>Once a puzzle is resolved, you will be automatically introduced to the next one. You will not be allowed to repeat any level.</p>
        ',
    ],

    /**
     * Feedback (generic)
     */
    'feedback' => [
        'title' => 'Your feedback',
        'subtitle' => 'Part 5:',
        'description' => 'This is the last part of the survey. Before you go, I would like to kindly ask you a couple of questions related to your performance.',
    ],

];
