<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Values Language Lines (V1)
    |--------------------------------------------------------------------------
    */

    /**
     * Programming specialization
     */
    'programming_specialization' => [
        'backend' => 'Backend',
        'frontend' => 'Frontend',
        'fullstack' => 'Fullstack',
        'mobile_apps' => 'Mobile applications',
        'desktop_apps' => 'Desktop applications',
        'software_testing' => 'Software testing',
        'devops' => 'DevOps',
        'databases' => 'Databases',
        'artificial_intelligence' => 'Artificial Intelligence',
        'systems_administration' => 'Systems administration',
        'cybersecurity' => 'Cybersecurity',
        'data_analysis' => 'Data mining and analysis',
    ],

    /**
     * Programming Language
     */
    'programming_language' => [
        'c' => 'C',
        'java' => 'Java',
        'python' => 'Python',
        'c++' => 'C++',
        'c#' => 'C#',
        'visual_basic' => 'Visual Basic',
        'javascript' => 'JavaScript',
        'php' => 'PHP',
        'r' => 'R',
        'groovy' => 'Groovy',
        'assembly_language' => 'Assembly language',
        'sql' => 'SQL',
        'swift' => 'Swift',
        'go' => 'Go',
        'ruby' => 'Ruby',
        'matlab' => 'MATLAB',
        'perl' => 'Perl',
        'objective_c' => 'Objective-C',
    ],

    /**
     * Likert standard scale
     */
    'likert_standard' => [
        'disagree' => 'Disagree',
        'agree' => 'Agree',
    ],

    /**
     * Music genres
     */
    'music_genres' => [
        'acoustic' => 'Acoustic',
        'alternative' => 'Alternative',
        'blues' => 'Blues',
        'classical' => 'Classical',
        'country' => 'Country',
        'dance' => 'Dance',
        'electronic' => 'Electronic',
        'folk' => 'Folk',
        'hip_hop' => 'Hip-hop',
        'house' => 'House',
        'indie' => 'Indie',
        'jazz' => 'Jazz',
        'lo_fi' => 'Lo-fi',
        'metal' => 'Metal',
        'pop' => 'Pop',
        'punk' => 'Punk',
        'reggae' => 'Reggae',
        'rock' => 'Rock',
        'soul' => 'Soul',
        'symphonic' => 'Symphonic',
        'synth_pop' => 'Synth pop',
        'techno' => 'Techno',
    ],
];
