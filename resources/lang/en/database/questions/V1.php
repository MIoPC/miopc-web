<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Questions Language Lines (V1)
    |--------------------------------------------------------------------------
    */

    /**
     * Programming experience
     */
    'programming_experience' => [
        'title' => 'For how many years have you been programming?',
    ],

    /**
     * IT seniority
     */
    'it_seniority' => [
        'title' => 'For how many years have you been working in the IT industry?',
    ],

    /**
     * Programming specialization
     */
    'programming_specialization' => [
        'title' => 'Choose the specialization that describes you the best:',
    ],

    /**
     * Programming language proficiency
     */
    'programming_language_proficiency' => [
        'title' => 'How many programming languages are you fluent in?',
    ],

    /**
     * Programming language
     */
    'programming_language' => [
        'title' => 'What programming language do you use on your regular basis?',
    ],

    /**
     * Procedural paradigm proficiency
     */
    'procedural_paradigm_proficiency' => [
        'title' => 'I know and understand the procedural programming paradigm.',
    ],

    /**
     * Functional paradigm proficiency
     */
    'functional_paradigm_proficiency' => [
        'title' => 'I know and understand the functional programming paradigm.',
    ],

    /**
     * Logical paradigm proficiency
     */
    'logical_paradigm_proficiency' => [
        'title' => 'I know and understand the logical programming paradigm.',
    ],

    /**
     * Object-oriented paradigm proficiency
     */
    'object_oriented_paradigm_proficiency' => [
        'title' => 'I know and understand the object-oriented programming paradigm.',
    ],

    /**
     * Favorite music genre
     */
    'favorite_music_genre' => [
        'title' => 'What is your favorite music genre?',
    ],

    /**
     * Unbeloved music genre
     */
    'unbeloved_music_genre' => [
        'title' => 'Which genre of music do you like the least?',
    ],

    /**
     * Music listening when working
     */
    'music_listening_when_working' => [
        'title' => 'I often listen to music while performing my duties in my workplace.',
    ],

    /**
     * Music listening when programming
     */
    'music_listening_when_programming' => [
        'title' => 'I listen to music a lot when I program.',
    ],

    /**
     * Music stimulus effect
     */
    'music_stimulus_effect' => [
        'title' => 'Music has a positive, stimulative effect on me when I work.',
    ],

    /**
     * Audiophile
     */
    'audiophile' => [
        'title' => 'I am an audiophile.',
    ],

    /**
     * Music mood effect
     */
    'music_mood_effect' => [
        'title' => 'Music has a positive effect on my well-being.',
    ],

    /**
     * Music education
     */
    'music_education' => [
        'title' => 'I received a good musical education.',
    ],

    /**
     * Music talent
     */
    'music_talent' => [
        'title' => 'I have musical talent.',
    ],

    /**
     * Self-esteem
     */
    'self_esteem' => [
        'title' => 'I am happy with my performance when solving the puzzle.',
    ],

    /**
     * Difficulty level
     */
    'difficulty_level' => [
        'title' => 'I had to put a lot of effort to resolve the puzzle.',
    ],

    /**
     * Focus (music)
     */
    'focus_music' => [
        'title' => 'The background music kept me from focusing.',
    ],

    /**
     * Performance (music)
     */
    'performance_music' => [
        'title' => 'I could have done better if I was not listening to music.',
    ],

    /**
     * Focus (silence)
     */
    'focus_silence' => [
        'title' => 'The lack of music kept me from focusing.',
    ],

    /**
     * Performance (silence)
     */
    'performance_silence' => [
        'title' => 'I could have done better if I was listening to music.',
    ],

    /**
     * Music match (music)
     */
    'music_match' => [
        'title' => 'I enjoyed the song that was playing in the background.',
    ],
];
