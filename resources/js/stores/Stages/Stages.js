import { observable, computed, action } from 'mobx'


/**
 *
 */
export class Stages {

    /** */
    @observable stages = []

    /** */
    @observable current = null

    /**
     *
     */
    constructor({
        stages = [],
        current = null,
    }) {
        this.stages = stages
        this.current = current
    }

    /**
     *
     * @param {Number} offset
     */
    onPosition = (offset) => {
        const requested = this.index + offset
        if (requested < 0) return null
        if (requested > this.stages.length - 1) return null
        return this.stages[requested]
    }

    /**
     *
     * @param {Number} offset
     */
    @action change = (offset) => {
        this.current = this.onPosition(offset)
    }

    /**
     *
     * @param {} stage
     */
    @action remove = (stage) => {
        this.stages = this.stages.filter(item => item !== stage)
    }

    /**
     *
     */
    next = () => {
        this.change(1)
    }

    /**
     *
     */
    previous = () => {
        this.change(-1)
    }

    /**
     *
     */
    @computed get upcoming() {
        return this.onPosition(1)
    }

    /**
     *
     */
    @computed get index() {
        if (!this.current) return -1
        return this.stages.indexOf(this.current)
    }

    /**
     *
     */
    @computed get canGoNext() {
        return this.index < this.stages.length - 1
    }

    /**
     *
     */
    @computed get canGoBack() {
        return this.index !== 0
    }
}

export default Stages
