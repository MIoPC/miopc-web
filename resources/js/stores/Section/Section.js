import { observable, computed } from 'mobx'

// helpers
import preloadImage from '@helpers/preloadImage'

// stores
import Question from '@stores/Question/Question'


/**
 *
 */
export class Section {

    /** */
    id

    /** */
    subtitle

    /** */
    title

    /** */
    description

    /** */
    image

    /** */
    type

    /** */
    position

    /** */
    @observable questions = []

    /**
     *
     * @param {Object} blueprint
     */
    constructor({
        id,
        title,
        subtitle,
        description,
        background_image,
        type,
        position,
        questions,
    }) {
        // Assign section related data
        this.id = id
        this.title = title
        this.subtitle = subtitle
        this.description = description
        this.image = background_image
        this.type = type
        this.position = position

        // Create questions objects
        this.questions = questions.map(data => new Question(data))
    }

    /**
     *
     */
    preloadAssets = async () => {
        await preloadImage(this.image)
    }

    /**
     *
     */
    isInactive = (question) => {
        const index = this.questions.indexOf(question)
        if (index === 0) return false
        for (let i = index - 1; i >= 0; i--) {
            if (!this.questions[i].isValid) return true
        }
        return false
    }

    /**
     *
     */
    @computed get isValid() {
        for (const question of this.questions) {
            if (!question.isValid) return false
        }
        return true
    }

    /**
     *
     */
    @computed get wasModified() {
        for (const question of this.questions) {
            if (question.wasModified) return true
        }
        return false
    }
}

export default Section
