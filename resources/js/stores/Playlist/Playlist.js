import { observable } from 'mobx'

// stores
import Audio from '@stores/Audio'


/**
 *
 */
export class Playlist {

    /** */
    @observable files = []

    /** */
    @observable current

    /**
     *
     * @param {} files
     */
    constructor(...files) {
        this.files = files.map(file => new Audio(file))
        this.current = this.files[0]
        this._attachEvents(this.current)
    }

    /**
     *
     */
    _attachEvents = (file) => {
        file.on('load', this._loadNext)
        file.on('end', this._playNext)
    }

    /**
     *
     */
    _getNext = (file) => {
        let index = this.files.indexOf(file) + 1
        if (index >= this.files.length) index = 0
        return this.files[index]
    }

    /**
     *
     */
    _loadNext = (file, event) => {
        this._getNext(file).preloadAssets()
    }

    /**
     *
     */
    _playNext = (file, event) => {
        file.destroy()
        const next = this._getNext(file)
        this._attachEvents(next)
        next.promisePlay()
        this.current = next
    }

    /**
     *
     */
    destroy = () => {
        for (const file of this.files) {
            file.destroy()
        }
    }

    /**
     *
     */
    preloadAssets = async () => {
        await this.current.preloadAssets()
    }

    /**
     *
     */
    promisePlay = async () => {
        await this.current.promisePlay()
    }
}

export default Playlist
