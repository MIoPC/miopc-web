import { computed, observable } from 'mobx'

// stores
import Stages from '@stores/Stages'
import Playlist from '@stores/Playlist'
import Game from '@organisms/minigames/BotMania/stores/models/Game'

// constants
import { STATE } from '@constants/page-state'
import {
    STAGE_TYPE,
} from './constants'

// config
import {
    STAGES,
    INITIAL_STAGE,
} from './config'


/**
 *
 */
export class Puzzle {

    /** */
    @observable stages

    /** */
    @observable questions = []

    /** */
    @observable currentQuestion

    /** */
    @observable game

    /** */
    numberOfLevels

    /** NASTY HACK, FIX THAT */
    questionnaire

    /** */
    playlist

    /**
     *
     * @param {} config
     */
    constructor({
        section,
        questionnaire,
        music,
    }) {
        // Set questions
        this.numberOfLevels = section.questions.length
        this.questions = section.questions.filter(question => !question.value)
        this.questionnaire = questionnaire

        // Set music
        if (music) {
            this.playlist = new Playlist(...music)
            this.playlist.preloadAssets()
        }

        // Set stages
        let stages = [...STAGES]
        if (!this.hasMusic) stages = stages.filter(stage => stage !== STAGE_TYPE.MUSIC_NOTICE)
        this.stages = new Stages({ stages, current: INITIAL_STAGE })
    }

    /**
     *
     */
    destroy = () => {
        if (this.hasMusic) this.playlist.destroy()
    }

    /**
     *
     */
    next = () => {
        if (this.stages.upcoming === STAGE_TYPE.LEVEL) this.nextLevel()
        this.stages.next()
    }

    /**
     *
     */
    nextLevel = async () => {
        this.questionnaire.state = STATE.LOADING
        if (this.hasMusic) await this.playlist.promisePlay()
        const index = this.currentQuestion ? this.currentIndex + 1 : 0
        const question = this.questions[index]
        const game = new Game()
        game.onWin = this.questionnaire.handleLevelWin
        game.mountLevel(question.level)
        this.game = game
        this.currentQuestion = question
        this.questionnaire.state = STATE.READY
    }

    /**
     *
     */
    skipLevel = async () => {
        this.questionnaire.handleLevelWin(false)
    }

    /**
     *
     */
    @computed get currentIndex() {
        return this.questions.indexOf(this.currentQuestion)
    }

    /**
     *
     */
    @computed get hasMoreLevels() {
        return this.currentIndex + 1 < this.questions.length
    }

    /**
     *
     */
    @computed get hasMusic() {
        return !!this.playlist
    }
}

export default Puzzle
