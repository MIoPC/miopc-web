// constants
import {
    STAGE_TYPE,
} from './constants'


/**
 *
 */
export const STAGES = Object.values(STAGE_TYPE)

/**
 *
 */
export const INITIAL_STAGE = Object.values(STAGES)[0]
