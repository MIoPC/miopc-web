import { computed, observable } from 'mobx'
import { Howl } from 'howler'


/**
 *
 */
export class Audio {

    /** */
    howl

    /** */
    id

    /** */
    author

    /** */
    title

    /** */
    license

    /** */
    licenseLink

    /** */
    source

    /** */
    path

    /**
     *
     * @param {} blueprint
     */
    constructor(blueprint) {
        // Map give data
        this.id = blueprint.id
        this.author = blueprint.author
        this.title = blueprint.title
        this.license = blueprint.license
        this.licenseLink = blueprint.license_link
        this.source = blueprint.source
        this.path = blueprint.path
    }

    /**
     *
     */
    _getHowl = () => {
        if (!this.howl) {
            this.howl = new Howl({
                src: this.path,
                preload: true,
                html5: true,
            })
        }
        return this.howl
    }

    /**
     *
     */
    preloadAssets = () => {
        const howl = this._getHowl()
        if (howl.state() === 'loaded') return
        return new Promise((resolve, reject) => {
            howl.on('load', resolve)
            howl.on('loaderror', reject)
            howl.on('playerror', reject)
        })
    }

    /**
     *
     */
    destroy = () => {
        this._getHowl().unload()
        this.howl = null
    }

    /**
     *
     */
    promisePlay = async () => {
        const howl = this._getHowl()
        if (howl.playing()) return
        await this.preloadAssets()
        howl.play()
    }

    /**
     *
     */
    on = (event, handler) => {
        this._getHowl().on(event, (data) => {
            handler(this, data)
        })
    }
}

export default Audio
