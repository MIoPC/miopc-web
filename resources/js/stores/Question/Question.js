import { observable, computed, action } from 'mobx'

// helpers
import service from '@helpers/service'

// stores
import Value from '@stores/Value'

// config
import {
    VALIDATION_HANDLERS,
} from './config'

// shared
import {
    SUBMIT_ANSWER,
} from '@shared/hateoas'



/**
 *
 */
export class Question {

    /** */
    id

    /** */
    title

    /** */
    type

    /** */
    placeholder

    /** */
    _rules

    /** */
    @observable enum

    /** */
    @observable validationMessages = []

    /** */
    @observable value

    /** */
    @observable position

    /** */
    level

    /** */
    _initialValue

    /** */
    _isEditable

    /** */
    _links

    /** */
    @observable isValid = false

    /**
     *
     * @param {Object} blueprint
     */
    constructor(blueprint) {
        // Assign question related data
        this.id = blueprint.id
        this.title = blueprint.title
        this.type = blueprint.type
        this._isEditable = blueprint.editable

        // Assign validation rules
        this._rules = blueprint.validation_rules.split(';')

        // Assign submitted value
        const value = blueprint.value || ''
        this.setValue(value)
        this._initialValue = value

        // Create enum values objects
        if (blueprint.enum) {
            this.enum = blueprint.enum.values.map(data => new Value(data))
        }

        // Save HATEOS links
        this._links = blueprint._links

        // Set up the level data
        this.level = blueprint.puzzle_structure
        this.position = blueprint.pivot.position
    }

    /**
     *
     */
    @computed get isDisabled() {
        // return true
        return !this._isEditable && !!this._initialValue
    }

    /**
     *
     */
    @computed get hasValidationMessages() {
        return this.validationMessages.length > 0
    }

    /**
     *
     */
    @computed get wasModified() {
        return this._initialValue !== this.value
    }

    /**
     *
     * @param {*} value
     * @param {boolean} breakCircuit
     */
    getValidationMessages = (value, breakCircuit = false) => {
        const messages = []
        for (const rule of this._rules) {
            let [name, parameters] = rule.split(':')
            const handler = VALIDATION_HANDLERS[name]
            if (!handler) continue
            if (parameters) parameters = parameters.split(',')
            const validation = handler(this, value, parameters)
            if (validation && validation.length > 0) {
                messages.push(...validation)
                if (breakCircuit) break
            }
        }
        return messages
    }

    /**
     *
     * @param {*} value
     */
    @action setValue = (value) => {
        this.isValid = !!value && !this.hasValidationMessages
        this.value = value
    }

    /**
     *
     */
    submitAnswer = async (data) => {
        const link = this._links[SUBMIT_ANSWER]
        await service.hateoas(link, data)
    }
}

export default Question
