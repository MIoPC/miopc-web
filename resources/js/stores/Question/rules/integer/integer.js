// constants
import {
    VALIDATION_MESSAGES,
} from '@constants/validation-rules'


/**
 *
 * @param {*} question
 * @param {*} value
 * @param {*} parameters
 */
export default (question, value) => {
    const parsed = parseInt(value, 10).toString()
    if (value !== parsed) return [[ VALIDATION_MESSAGES.NOT_AN_INTEGER ]]
}
