// constants
import {
    VALIDATION_MESSAGES,
} from '@constants/validation-rules'


/**
 *
 * @param {*} question
 * @param {*} value
 * @param {*} parameters
 */
export default (question, value, parameters) => {
    value = Number(value)
    const scaleLength = Number(parameters[0])
    if (value < 1 || value > scaleLength) return [[ VALIDATION_MESSAGES.GENERIC_INVALID_VALUE ]]
}
