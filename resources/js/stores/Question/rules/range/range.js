// constants
import {
    VALIDATION_MESSAGES,
} from '@constants/validation-rules'


/**
 *
 * @param {*} question
 * @param {*} value
 * @param {*} parameters
 */
export default (question, value, parameters) => {
    value = Number(value)
    const [min, max] = parameters
    if (value < Number(min) || value > Number(max)) return [[ VALIDATION_MESSAGES.NOT_IN_RANGE, min, max ]]
}
