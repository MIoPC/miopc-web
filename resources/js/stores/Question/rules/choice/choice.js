// constants
import {
    VALIDATION_MESSAGES,
} from '@constants/validation-rules'


/**
 *
 * @param {*} question
 * @param {*} value
 * @param {*} parameters
 */
export default (question, value) => {
    const values = question.enum.map(option => option.value)
    if (values.indexOf(value) < 0) return [[ VALIDATION_MESSAGES.GENERIC_INVALID_VALUE ]]
}
