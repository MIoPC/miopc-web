// constants
import {
    VALIDATION_MESSAGES,
} from '@constants/validation-rules'


/**
 *
 * @param {*} question
 * @param {*} value
 * @param {*} parameters
 */
export default (question, value) => {
    if (!value) return [[ VALIDATION_MESSAGES.REQUIRED_MISSING_VALUE ]]
}
