// constants
import {
    VALIDATION_TYPES,
} from '@constants/validation-rules'

// validators
import choice from './rules/choice'
import integer from './rules/integer'
import likert from './rules/likert'
import range from './rules/range'
import required from './rules/required'


/**
 *
 */
export const VALIDATION_HANDLERS = {
    /** */
    [VALIDATION_TYPES.CHOICE]: choice,
    /** */
    [VALIDATION_TYPES.INTEGER]: integer,
    /** */
    [VALIDATION_TYPES.LIKERT]: likert,
    /** */
    [VALIDATION_TYPES.RANGE]: range,
    /** */
    /** */
    [VALIDATION_TYPES.REQUIRED]: required,
}
