import { observable, computed } from 'mobx'

// stores
import Stages from '@stores/Stages'
import Game from '@organisms/minigames/BotMania/stores/models/Game'

// config
import {
    STAGES,
    INITIAL_STAGE,
    LEVEL_MAPPING,
} from './config'


/**
 *
 */
export class Tutorial {

    /** */
    @observable stages = new Stages({ stages: Object.values(STAGES) })

    /** */
    @observable game

    /**
     *
     * @param {*} stage
     */
    _setLevel = (level) => {
        this.game = new Game()
        this.game.mountLevel(
            level ||
            LEVEL_MAPPING[this.stages.current] ||
            LEVEL_MAPPING.DEFAULT,
        )
    }

    /**
     *
     */
    start = () => {
        this._setLevel()
        this.stages.current = INITIAL_STAGE
    }

    /**
     *
     */
    next = () => {
        this.stages.next()
        this._setLevel()
    }

    /**
     *
     */
    previous = () => {
        this.stages.previous()
        this._setLevel()
    }
}

export default Tutorial
