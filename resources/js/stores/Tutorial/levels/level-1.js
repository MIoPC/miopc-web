// constants
import { POINT_TYPES } from '@organisms/minigames/BotMania/constants/point-types'
import { DIRECTIONS } from '@organisms/minigames/BotMania/constants/directions'


/**
 *
 */
export default {
    tiles: [
        // row
        { x: 0, y: 1, type: POINT_TYPES.TILE },
        { x: 1, y: 1, type: POINT_TYPES.TILE },
        { x: 2, y: 1, type: POINT_TYPES.TILE },
        { x: 3, y: 1, type: POINT_TYPES.TILE },
        { x: 4, y: 1, type: POINT_TYPES.TILE },
        { x: 5, y: 1, type: POINT_TYPES.CHECKPOINT },
        { x: 6, y: 1, type: POINT_TYPES.CHECKPOINT },
        // row
        { x: 0, y: 0, type: POINT_TYPES.START },
        { x: 1, y: 0, type: POINT_TYPES.EMPTY },
        { x: 2, y: 0, type: POINT_TYPES.EMPTY },
        { x: 3, y: 0, type: POINT_TYPES.EMPTY },
        { x: 4, y: 0, type: POINT_TYPES.EMPTY },
        { x: 5, y: 0, type: POINT_TYPES.EMPTY },
        { x: 6, y: 0, type: POINT_TYPES.CHECKPOINT },
    ],
    pointer: { rotation: DIRECTIONS.NORTH },
    procedures: [3, 5],
}
