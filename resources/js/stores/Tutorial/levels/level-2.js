// constants
import { POINT_TYPES } from '@organisms/minigames/BotMania/constants/point-types'
import { DIRECTIONS } from '@organisms/minigames/BotMania/constants/directions'


/**
 *
 */
export default {
    tiles: [
        // row
        { x: 0, y: 0, type: POINT_TYPES.START },
        { x: 1, y: 0, type: POINT_TYPES.TILE },
        { x: 2, y: 0, type: POINT_TYPES.TILE },
        { x: 3, y: 0, type: POINT_TYPES.TILE },
        { x: 4, y: 0, type: POINT_TYPES.TILE },
        { x: 5, y: 0, type: POINT_TYPES.CHECKPOINT },
    ],
    pointer: { rotation: DIRECTIONS.EAST },
    procedures: [1, 2],
}
