// levels
import level1 from './levels/level-1'
import level2 from './levels/level-2'
import level3 from './levels/level-3'

// constants
import {
    STAGE_TYPE,
} from './constants'


/**
 *
 */
export const STAGES = Object.values(STAGE_TYPE)

/**
 *
 */
export const INITIAL_STAGE = Object.values(STAGES)[0]

/**
 *
 */
export const LEVEL_MAPPING = {
    DEFAULT: level1,
    [STAGE_TYPE.RECURSION]: level2,
    [STAGE_TYPE.SUMMARY]: level3,
}
