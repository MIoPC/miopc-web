import { observable, computed, action } from 'mobx'
import to from 'await-to-js'
import Polyglot from 'node-polyglot'
import { scroller } from 'react-scroll'

// stores
import Section from '@stores/Section'
import Verification from '@stores/Verification'

// helpers
import service from '@helpers/service'

// constants
import { STATE } from '@constants/page-state'
import { SECTION_TYPES } from '@constants/section-types'

// shared
import {
    GET_SURVEY_SECTION,
    SUBMIT_SURVEY_SECTION,
    VERIFY_JOURNEY,
    PAGINATION_BACK,
    PAGINATION_NEXT,
    FINISH_JOURNEY,
} from '@shared/hateoas'

/**
 *
 */
export class Questionnaire {

    /** */
    @observable state = STATE.LOADING

    /** */
    _ = new Polyglot()

    /** */
    @observable section

    /** */
    @observable tutorial

    /** */
    @observable puzzle

    /** */
    @observable verification = new Verification()

    /** */
    @observable _links = {}

    /** */
    @observable type = null

    /** */
    @observable isFinished = false

    /**
     *
     * @param {Object} config
     * @param {string} config.lang
     * @param {string} config.endpoint
     */
    @action initialize = async ({
        lang,
        endpoint,
    }) => {
        // Make parallel calls to the server
        const [bootException, [
            translation,
            latest,
        ]] = await to(
            Promise.all([
                import(`@locales/${lang}/questionnaire`),
                service.get(endpoint),
            ])
        )
        if (bootException) return this._handleException(bootException)

        // Load global locales
        this._.replace(translation.default)

        // Save latest data for further use
        const { data } = latest
        this.type = data.surveyType

        // If the user is not authenticated, show
        const [restoreException] = await to(
            Promise.all([
                this._mountSection(data._links[GET_SURVEY_SECTION]),
                this._restoreVerification({ ...data, lang }),
            ])
        )
        if (restoreException) return this._handleException(restoreException)

        // Mark page as loaded
        this.state = STATE.READY
    }

    /**
     *
     */
    _handleException = (exception) => {
        console.error(exception)
        this.state = STATE.ERROR
    }

    /**
     * Restores the verification mechanism
     * @param {Object} data
     */
    _restoreVerification = async ({
        _links,
        isVerified,
        verificationKey,
        lang
    }) => {
        await this.verification.initialize({
            _link: _links[VERIFY_JOURNEY],
            key: verificationKey,
            isVerified,
            lang,
        })
    }

    /**
     *
     * @param {Object} link
     */
    @action _mountSection = async (link) => {
        // Get a survey section from the response
        const { data } = await service.hateoas(link)

        // Create a section store
        this.section = new Section(data.section)

        // Save HATEOAS links for future use
        this._links = data._links

        // Preload all the assets
        await this.section.preloadAssets()

        // If it's a tutorial section preload the tutorial store
        if (this.section.type === SECTION_TYPES.PUZZLE_TUTORIAL) {
            return this.tutorial = new (await import('@stores/Tutorial')).default()
        }

        // Handle puzzle section
        if (this.section.type === SECTION_TYPES.PUZZLE) {
            this.puzzle = new (await import('@stores/Puzzle')).default({
                music: data.music,
                section: this.section,
                questionnaire: this,
            })
        }
    }

    /**
     *
     * @param {*} question
     * @param {*} value
     */
    @action updateAnswer = (question, value) => {
        // Validate the answer
        const errors = question.getValidationMessages(value, true)

        // Show corresponding validation messages
        question.validationMessages = errors.map(([type, ...parameters]) => this._.t(`validation.${type}`, parameters))

        // Update the question value
        question.setValue(value)
    }

    /**
     *
     */
    @action _submitAnswers = async () => {
        // Check if section was modified
        if (!this.section.wasModified) return

        // Check if section is valid
        if (!this.section.isValid) return

        // If the user is not verified, run verification process or exit
        if (!await this.verification.verifyUser(() => {
            this.state = STATE.LOADING
        })) return this.state = STATE.READY

        // Prepare an API payload
        const payload = {
            answers: this.section.questions.map(({ id, value }) => ({ id, value })),
        }

        // Make an API call to save answers in a database
        this.state = STATE.LOADING
        let [submitException] = await to(
            service.hateoas(
                this._links[SUBMIT_SURVEY_SECTION],
                payload,
            )
        )
        if (submitException) return this._handleException(submitException)
    }

    /**
     *
     */
    @action nextLevel = async () => {
        scroller.scrollTo('assignment-zone', { offset: -300 })
        if (this.puzzle.hasMoreLevels) {
            this.puzzle.nextLevel()
            return this.puzzle.game.onWin = this.handleLevelWin
        }
        this.puzzle.destroy()
        await this.next()
    }

    /**
     *
     */
    handleLevelWin = async (value = true) => {
        this.state = STATE.LOADING
        const question = this.puzzle.currentQuestion
        const { analytics } = this.puzzle.game.level
        await question.submitAnswer({
            value,
            errors: analytics.errors,
            changes: analytics.changes,
            commands: analytics.commands,
            steps: analytics.steps,
            plays: analytics.plays,
            elapsed_time: analytics.elapsedTime,
        })
        this.nextLevel()
        this.state = STATE.READY
    }

    /**
     *
     */
    @action next = async () => {
        await this._submitAnswers()
        this.state = STATE.LOADING
        await this._mountSection(this._links[PAGINATION_NEXT])
        this.state = STATE.READY
    }

    /**
     *
     */
    @action finish = async () => {
        await this._submitAnswers()
        this.state = STATE.LOADING
        const { data } =  await service.hateoas(this._links[FINISH_JOURNEY])
        window.location.assign(data.redirect)
    }

    /**
     *
     */
    @action previous = async () => {
        this.state = STATE.LOADING
        await this._mountSection(this._links[PAGINATION_BACK])
        this.state = STATE.READY
    }

    /**
     *
     */
    @computed get hasNextSection() {
        return !!this._links[PAGINATION_NEXT]
    }

    /**
     *
     */
    @computed get hasPreviousSection() {
        return !!this._links[PAGINATION_BACK]
    }

    /**
     *
     */
    bindTranslation = (prefix) => {
        prefix = prefix ? `${prefix}.` : ''
        return (path, ...parameters) => {
            return this._.t(`${prefix}${path}`, ...parameters)
        }
    }

    /**
     *
     */
    translate = (path) => {
        return this._.t(path)
    }
}

export default Questionnaire
