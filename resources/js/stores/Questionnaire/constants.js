/**
 *
 */
export const SECTION_TYPES = {
    IT_SURVEY: 'it_survey',
    MUSIC_SURVEY: 'music_survey',
    TUTORIAL: 'tutorial',
    PROGRAMMING_PUZZLE: 'programming_puzzle',
    FEEDBACK_SURVEY: 'feedback_survey',
}
