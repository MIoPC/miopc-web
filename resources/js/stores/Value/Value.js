/**
 *
 */
export class Value {

    /** */
    id

    /** */
    value

    /** */
    label

    /**
     *
     * @param {Object} blueprint
     */
    constructor({
        id,
        value,
        label,
    }) {
        // Assign value related data
        this.id = id
        this.value = value
        this.label = label
    }
}

export default Value
