/**
 *
 */
export const RE_CAPTCHA_URL = 'https://www.google.com/recaptcha/api.js'

/**
 *
 */
export const RE_CAPTCHA_CALLBACK_SUCCESS = '__reCaptchaCallbackSuccess'

/**
 *
 */
export const RE_CAPTCHA_CALLBACK_ERROR = '__reCaptchaCallbackError'

/**
 *
 */
export const RE_CAPTCHA_ELEMENT = (key) => {
    return (`
        <div class="g-recaptcha"
            data-sitekey="${key}"
            data-callback="${RE_CAPTCHA_CALLBACK_SUCCESS}"
            data-expired-callback="${RE_CAPTCHA_CALLBACK_ERROR}"
            data-error-callback="${RE_CAPTCHA_CALLBACK_ERROR}"
            data-size="invisible"
        ></div>
    `)
}
