import { observable, action } from 'mobx'
import to from 'await-to-js'

// helpers
import addScript from '@helpers/addScript'
import service from '@helpers/service'
import { isMobile } from '@helpers/userInfo'

// config
import {
    RE_CAPTCHA_URL,
    RE_CAPTCHA_CALLBACK_SUCCESS,
    RE_CAPTCHA_CALLBACK_ERROR,
    RE_CAPTCHA_ELEMENT,
} from './config'


/**
 *
 */
export class Verification {

    /** */
    @observable isVerified

    /** */
    _token

    /** */
    _link

    /** */
    key

    /**
     *
     * @param {Object} blueprint
     * @param {boolean} blueprint.isVerified
     */
    @action initialize = async ({
        isVerified,
        _link,
        key,
        lang,
    }) => {
        // Assign store related variables
        this.isVerified = isVerified
        this._link = _link
        this.key = key

        // If user was verified before, exit
        if (isVerified) return

        // Create DOM element for holding ReCaptcha modal
        this._attachReCaptchaElement()

        // Inject ReCaptcha javascript script
        await addScript(`${RE_CAPTCHA_URL}?hl=${lang}`)
    }

    /**
     *
     */
    _attachReCaptchaElement = () => {
        document.body.insertAdjacentHTML(
            'afterbegin',
            RE_CAPTCHA_ELEMENT(this.key),
        )
    }

    /**
     *
     */
    _triggerReCaptchaChallenge = () => {
        return new Promise((resolve, reject) => {
            // Grab API library object
            const { grecaptcha } = window

            // Define cleanup method
            const terminate = (callback) => {
                return (...data) => {
                    grecaptcha.reset()
                    delete window[RE_CAPTCHA_CALLBACK_SUCCESS]
                    delete window[RE_CAPTCHA_CALLBACK_ERROR]
                    callback(...data)
                }
            }

            // Register callbacks
            window[RE_CAPTCHA_CALLBACK_SUCCESS] = terminate(resolve)
            window[RE_CAPTCHA_CALLBACK_ERROR] = terminate(reject)

            // Trigger the challenge
            grecaptcha.execute()
        })
    }

    /**
     *
     * @param {function} beforeRequest
     */
    verifyUser = async (beforeRequest) => {
        // If user is already verified, do nothing
        if (this.isVerified) return true

        // Otherwise trigger ReCaptcha challenge
        const [failure, token] = await to(this._triggerReCaptchaChallenge())
        if (failure || !token) return false

        // If user resolved the ReCaptcha challenge, try to create a journey
        await beforeRequest()
        const [exception] = await to(service.hateoas(this._link, {
            isMobile: isMobile(),
            token,
        }))
        if (exception) return false

        // If token is OK, mark user as a verified
        this.isVerified = true
        return true
    }
}

export default Verification
