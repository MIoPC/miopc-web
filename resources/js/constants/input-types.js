/**
 *
 */
export const INPUT_TYPES = {
    STRING: 'STRING',
    NUMBER: 'NUMBER',
    TEXT: 'TEXT',
    CHOICE: 'CHOICE',
    LIKERT: 'LIKERT',
}
