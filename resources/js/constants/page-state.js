/**
 *
 */
export const STATE = {
    LOADING: 'loading',
    READY: 'ready',
    ERROR: 'error',
}
