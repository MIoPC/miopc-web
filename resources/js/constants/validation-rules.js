/**
 *
 */
export const VALIDATION_TYPES = {
    CHOICE: 'CHOICE',
    INTEGER: 'INTEGER',
    LIKERT: 'LIKERT',
    RANGE: 'RANGE',
    REQUIRED: 'REQUIRED',
}

/**
 *
 */
export const VALIDATION_MESSAGES = {
    // integer
    NOT_AN_INTEGER: 'NOT_AN_INTEGER',
    // range
    NOT_IN_RANGE: 'NOT_IN_RANGE',
    // required
    REQUIRED_MISSING_VALUE: 'REQUIRED_MISSING_VALUE',
    // generic
    GENERIC_INVALID_VALUE: 'GENERIC_INVALID_VALUE',
}
