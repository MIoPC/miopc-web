/**
 *
 */
export const SECTION_TYPES = {
    SURVEY: 'SURVEY',
    PUZZLE: 'PUZZLE',
    PUZZLE_TUTORIAL: 'PUZZLE_TUTORIAL',
}
