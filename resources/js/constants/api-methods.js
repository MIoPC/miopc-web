/**
 *
 */
export const API_METHODS = {
    GET: 'get',
    POST: 'post',
    DELETE: 'delete',
    PATCH: 'patch',
    PUT: 'put',
}
