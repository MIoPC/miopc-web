// helpers
import waitUntilPageLoads from '@helpers/waitUntilPageLoads'

// controllers
import common from '@boot/common'
import app from '@boot/app'


(async () => {
    // Await for page to be responsive
    await waitUntilPageLoads()

    // Initialize all boot function in async manner
    const config = window.__APP
    const installations = [
        common(config),
        app(config),
    ]

    // Await for installations to complete
    await Promise.all(installations)
})()
