import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

// atoms
import ActionElement from '@atoms/ActionElement'


export default class Button extends PureComponent {

    /** */
    static propTypes = {
        children: PropTypes.node,
        className: PropTypes.string,
        isDisabled: PropTypes.bool,
        appearance: PropTypes.oneOf([
            'primary-1',
            'primary-2',
        ]),
        isUppercase: PropTypes.bool,
        fullWidth: PropTypes.bool,
    }

    /**
     * Default props
     */
    static defaultProps = {
        appearance: 'primary-1',
        fullWidth: true,
        tag: 'button',
    }

    /**
     * Helper getter for retrieving CSS styling
     */
    get cssRules() {
        const classes = [this.props.className, 'button']
        if (this.props.appearance) classes.push(`button--${this.props.appearance}`)
        if (this.props.isDisabled) classes.push('button--disabled')
        if (this.props.fullWidth) classes.push('button--full')
        if (this.props.isUppercase) classes.push('text-uppercase')
        return classes.join(' ')
    }

    render() {
        return (
            <>
                <ActionElement
                    {...this.props}
                    className={this.cssRules}
                />
            </>
        )
    }
}
