// constants
import { INPUT_TYPES }from '@constants/input-types'


/**
 *
 */
export const TYPE_MAPPING = {
    [INPUT_TYPES.NUMBER]: 'number',
    [INPUT_TYPES.STRING]: 'text',
}
