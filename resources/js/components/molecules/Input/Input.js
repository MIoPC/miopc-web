import React, { Component as PureComponent } from 'react'
import PropTypes from 'prop-types'

// atoms
import InputWrapper from '@atoms/InputWrapper'

// constants
import { NOOP } from '@constants/no-op'

// config
import {
    TYPE_MAPPING,
} from './config'


export default class FormSection extends PureComponent {

    /** */
    static propTypes = {
        /** */
        value: PropTypes.string,
        /** */
        type: PropTypes.oneOf(Object.keys(TYPE_MAPPING)),
        /** */
        isInactive: PropTypes.bool,
        /** */
        isDisabled: PropTypes.bool,
        /** */
        onChange: PropTypes.func,
    }

    /** */
    static defaultProps = {
        /** */
        onChange: NOOP,
    }

    /**
     *
     */
    get type() {
        return TYPE_MAPPING[this.props.type]
    }

    /**
     *
     */
    get tabIndex() {
        return this.props.isInactive ? -1 : undefined
    }

    /**
     *
     * @param {*} event
     */
    handleChange = (event) => {
        this.props.onChange(event.target.value)
    }

    /**
     *
     */
    render () {
        return (
            <>
                <InputWrapper {...this.props}>
                    <input
                        className='input__element'
                        type={this.type}
                        value={this.props.value}
                        tabIndex={this.tabIndex}
                        disabled={this.props.isDisabled}
                        onChange={this.handleChange}
                    />
                </InputWrapper>
            </>
        )
    }
}
