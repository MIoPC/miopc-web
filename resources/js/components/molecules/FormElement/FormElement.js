import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// molecules
import Input from '@molecules/Input'
import Likert from '@molecules/Likert'
import Select from '@molecules/Select'

// constants
import { INPUT_TYPES }from '@constants/input-types'
import { NOOP } from '@constants/no-op'


@observer
export default class FormElement extends Component {

    /** */
    static propTypes = {
        /** */
        ordinal: PropTypes.string,
        /** */
        question: PropTypes.object,
        /** */
        isInactive: PropTypes.bool,
        /** */
        onChange: PropTypes.func,
    }

    /** */
    static defaultProps = {
        /** */
        onChange: NOOP,
    }

    /**
     *
     */
    get inputProps() {
        const { question } = this.props
        const {
            isDisabled,
            hasValidationMessages,
        } = question
        return Object.assign({
            ...this.props,
            isDisabled,
            hasValidationMessages,
            onChange: this.handleChange,
        }, question)
    }

    /**
     *
     * @param {*} value
     */
    handleChange = (value) => {
        this.props.onChange(
            this.props.question,
            value,
        )
    }

    /**
     * Helper getter for retrieving CSS styling
     */
    get cssRules() {
        const classes = ['form-element']
        if (this.props.isInactive) classes.push('form-element--inactive')
        return classes.join(' ')
    }

    /**
     *
     */
    render () {
        const { type } = this.props.question
        return (
            <>
                <div className={this.cssRules}>
                    <Choose>
                        <When condition={type === INPUT_TYPES.LIKERT}>
                            <Likert {...this.inputProps} />
                        </When>
                        <When condition={type === INPUT_TYPES.CHOICE}>
                            <Select {...this.inputProps} />
                        </When>
                        <Otherwise>
                            <Input {...this.inputProps} />
                        </Otherwise>
                    </Choose>
                </div>
            </>
        )
    }
}
