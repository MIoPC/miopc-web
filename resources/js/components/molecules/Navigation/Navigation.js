import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

// atoms
import ActionElement from '@atoms/ActionElement'

// molecules
import Button from '@molecules/Button'


export default class Navigation extends PureComponent {

    /** */
    static propTypes = {
        /** */
        hasNext: PropTypes.bool,
        /** */
        hasPrevious: PropTypes.bool,
        /** */
        isNextDisabled: PropTypes.bool,
        /** */
        _: PropTypes.func,
        /** */
        onNext: PropTypes.func,
        /** */
        onPrevious: PropTypes.func,
        /** */
        onFinish: PropTypes.func,
    }

    /**
     *
     */
    get nextTitle() {
        if (this.props.hasNext) return this.props._('next')
        return this.props._('finish')
    }

    /**
     *
     */
    get nextAction() {
        if (this.props.hasNext) return this.props.onNext
        return this.props.onFinish
    }

    /**
     *
     */
    render () {
        return (
            <>
                <div className='fs--sm-1'>
                    <Button
                        isDisabled={this.props.isNextDisabled}
                        onAction={this.nextAction}
                    >
                        {this.nextTitle}
                    </Button>
                    <If condition={this.props.hasPrevious}>
                        <div className='pt--2 text-center'>
                            <Button
                                onAction={this.props.onPrevious}
                                appearance='primary-2'
                            >
                                {this.props._('back')}
                            </Button>
                        </div>
                    </If>
                </div>
            </>
        )
    }
}
