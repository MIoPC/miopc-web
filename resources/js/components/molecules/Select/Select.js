import React, { Component as PureComponent } from 'react'
import PropTypes from 'prop-types'

// atoms
import InputWrapper from '@atoms/InputWrapper'

// constants
import { NOOP } from '@constants/no-op'


export default class Select extends PureComponent {

    /** */
    static propTypes = {
        /** */
        value: PropTypes.any,
        /** */
        enum: PropTypes.array,
        /** */
        isInactive: PropTypes.bool,
        /** */
        isDisabled: PropTypes.bool,
        /** */
        onChange: PropTypes.func,
    }

    /** */
    static defaultProps = {
        /** */
        onChange: NOOP,
    }

    /**
     *
     */
    get tabIndex() {
        return this.props.isInactive ? -1 : undefined
    }

    /**
     *
     * @param {*} event
     */
    handleChange = (event) => {
        this.props.onChange(event.target.value)
    }

    /**
     *
     */
    render () {
        return (
            <>
                <InputWrapper {...this.props}>
                    <select
                        className='input__select'
                        value={this.props.value}
                        tabIndex={this.tabIndex}
                        disabled={this.props.isDisabled}
                        onChange={this.handleChange}
                    >
                        <option></option>
                        <For each='option' of={this.props.enum}>
                            <option
                                value={option.value}
                                key={option.id}
                            >
                                {option.label}
                            </option>
                        </For>
                    </select>
                </InputWrapper>
            </>
        )
    }
}
