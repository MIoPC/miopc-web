import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

// atoms
import ActionElement from '@atoms/ActionElement'
import InputWrapper from '@atoms/InputWrapper'

// constants
import { NOOP } from '@constants/no-op'


export default class Likert extends PureComponent {

    /** */
    static propTypes = {
        /** */
        value: PropTypes.any,
        /** */
        title: PropTypes.string,
        /** */
        enum: PropTypes.array,
        /** */
        isInactive: PropTypes.bool,
        /** */
        isDisabled: PropTypes.bool,
        /** */
        onChange: PropTypes.func,
    }

    static defaultProps = {
        /** */
        onChange: NOOP,
    }

    /**
     *
     */
    handleChange = (event, value) => {
        this.props.onChange(value)
    }

    /**
     *
     */
    get scaleRange() {
        return this.props.enum.length
    }

    /**
     *
     */
    get scaleValues() {
        return [...Array(this.scaleRange).keys()].map(i => (i + 1).toString())
    }

    /**
     *
     */
    get scaleNeutralIndex() {
        return Math.round(this.scaleValues.length / 2)
    }

    /**
     *
     */
    get minLabel() {
        return this.props.enum[0].label
    }

    /**
     *
     */
    get maxLabel() {
        return this.props.enum[this.props.enum.length - 1].label
    }

    /**
     *
     * @param {*} value
     */
    getClasses = (value) => {
        const offset = Math.abs(value - this.scaleNeutralIndex)
        const classes = [
            'likert__value',
            `likert__value--${offset}`,
        ]
        if (value === this.props.value) classes.push('likert__value--selected')
        return classes.join(' ')
    }

    /**
     *
     */
    render () {
        return (
            <>
                <InputWrapper {...this.props}>
                    <span className='likert'>
                        <span className='likert__values'>
                            <For each='value' of={this.scaleValues}>
                                <ActionElement
                                    className={this.getClasses(value)}
                                    onAction={this.handleChange}
                                    isDisabled={this.props.isInactive}
                                    value={value}
                                    key={value}
                                >
                                    <span className='visually-hidden'>
                                        {value}
                                    </span>
                                </ActionElement>
                            </For>
                        </span>
                    </span>
                </InputWrapper>
                <div className='likert__labels fs--sm-2'>
                    <div className='row'>
                        <div className='col-6'>
                            {this.minLabel}
                        </div>
                        <div className='col-6 text-right'>
                            {this.maxLabel}
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
