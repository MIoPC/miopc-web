import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Parser from 'html-react-parser';

export default class Raw extends PureComponent {

    /** */
    static propTypes = {
        /** */
        children: PropTypes.node,
    }

    /**
     *
     */
    render () {
        return (
            <>{Parser(this.props.children)}</>
        )
    }
}
