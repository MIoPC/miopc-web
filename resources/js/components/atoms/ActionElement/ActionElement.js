// libraries
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

// helpers
import isActionEvent from '@helpers/isActionEvent'


export default class ActionElement extends PureComponent {

    /**
     * Prop types validation
     */
    static propTypes = {
        children: PropTypes.node,
        className: PropTypes.string,
        style: PropTypes.object,
        href: PropTypes.string,
        openInNewTab: PropTypes.bool,
        onAction: PropTypes.func,
        preventDefault: PropTypes.bool,
        stopPropagation: PropTypes.bool,
        attributes: PropTypes.object,
        isDisabled: PropTypes.bool,
        value: PropTypes.any,
        tag: PropTypes.oneOf([ 'a', 'button' ]),
    }

    /**
     * Default props
     */
    static defaultProps = {
        className: '',
        style: {},
        onAction: null,
        preventDefault: true,
        stopPropagation: true,
        attributes: {},
        tag: 'a',
    }

    /**
     * Generic handler for action events
     * @param {Event} event
     */
    handleEvent = (event) => {
        if (this.props.preventDefault) event.preventDefault()
        if (this.props.stopPropagation) event.stopPropagation()
        if (this.props.isDisabled || !isActionEvent(event) || !this.props.onAction) {
            return
        }
        this.props.onAction(event, this.props.value)
    }

    /**
     *
     */
    get tabIndex() {
        return this.props.isDisabled ? '-1' : '0'
    }

    /** */
    render() {
        return (
            <>
                <this.props.tag role='button'
                    tabIndex={this.tabIndex}
                    className={`action-element ${this.props.className}`}
                    style={this.props.style}
                    href={this.props.href}
                    target={this.props.openInNewTab ? '_blank' : null}
                    onClick={this.handleEvent}
                    onKeyPress={this.handleEvent}
                    {...this.props.attributes}>
                    {this.props.children}
                </this.props.tag>
            </>
        )
    }
}
