import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// atoms
import Raw from '@atoms/Raw'


@observer
export default class HeroSection extends PureComponent {

    /** */
    static propTypes = {
        /** */
        children: PropTypes.node,
        /** */
        title: PropTypes.string,
        /** */
        subtitle: PropTypes.string,
        /** */
        description: PropTypes.string,
        /** */
        image: PropTypes.string,
    }

    /**
     *
     */
    render () {
        return (
            <>
                <div className='hero-section'>

                    {/* Header */}
                    <div>
                        <div className='fs--4 fs-lg--6 font-weight-normal'>
                            <If condition={this.props.subtitle}>
                                <span>
                                    <Raw>{this.props.subtitle}</Raw>&nbsp;
                                </span>
                            </If>
                            <If condition={this.props.title}>
                                <u className='fc--primary-1 font-weight-medium'>
                                    <Raw>{this.props.title}</Raw>
                                </u>
                            </If>
                        </div>
                        <div className='pt--3'>
                            <hr className='bg--light-2' />
                        </div>
                        <If condition={this.props.description}>
                            <div className='text-justify pt--3'>
                                <Raw>{this.props.description}</Raw>
                            </div>
                        </If>
                    </div>

                    {/* Background image */}
                    <img
                        className='hero-section__background'
                        src={this.props.image}
                        alt=''
                    />

                    {/* Main content */}
                    <div className='hero-section__content'>
                        {this.props.children}
                    </div>
                </div>
            </>
        )
    }
}
