import React, { Component as PureComponent } from 'react'
import PropTypes from 'prop-types'

// atoms
import Raw from '@atoms/Raw'


export default class InputWrapper extends PureComponent {

    /** */
    static propTypes = {
        /** */
        children: PropTypes.node,
        /** */
        ordinal: PropTypes.string,
        /** */
        title: PropTypes.string,
        /** */
        validationMessages: PropTypes.array,
        /** */
        isDisabled: PropTypes.bool,
        /** */
        hasValidationMessages: PropTypes.bool,
        /** */
        isInactive: PropTypes.bool,
    }

    /**
     * Helper getter for retrieving CSS styling
     */
    get cssRules() {
        const classes = ['input']
        if (this.props.hasValidationMessages) classes.push('input--invalid')
        if (this.props.isDisabled) classes.push('input--disabled')
        return classes.join(' ')
    }

    /**
     *
     */
    render () {
        return (
            <>
                <label className={this.cssRules}>
                    <span className='input__header'>
                        <If condition={this.props.ordinal}>
                            <span className='input__ordinal'>
                                {this.props.ordinal}
                            </span>
                        </If>
                        <span className='input__label'>
                            {this.props.title}
                        </span>
                    </span>
                    {this.props.children}
                    <If condition={this.props.hasValidationMessages}>
                        <span className='input__messages'>
                            <For each='message' of={this.props.validationMessages} index='i'>
                                <span
                                    key={i}
                                    className='input__message'
                                >
                                    <Raw>
                                        {message}
                                    </Raw>
                                </span>
                            </For>
                        </span>
                    </If>
                </label>
            </>
        )
    }
}
