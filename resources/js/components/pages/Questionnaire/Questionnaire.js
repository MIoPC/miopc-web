import React, { Component } from 'react'
import { observer } from 'mobx-react'

// stores
import questionnaire from '@stores/Questionnaire'

// constants
import { STATE } from '@constants/page-state'
import { SECTION_TYPES } from '@constants/section-types'

// organisms
import FormSection from '@components/organisms/FormSection'
import TutorialSection from '@components/organisms/TutorialSection'
import PuzzleSection from '@components/organisms/PuzzleSection'

// sections
import LoadingScreen from '@sections/LoadingScreen'

@observer
export default class Questionnaire extends Component {

    /**
     *
     */
    componentDidMount() {
        questionnaire.initialize(this.props)
    }

    /**
     *
     */
    get commonProps() {
        return {
            section: questionnaire.section,
            hasNext: questionnaire.hasNextSection,
            hasPrevious: questionnaire.hasPreviousSection,
            _navigation: questionnaire.bindTranslation('global'),
            onSingleSubmission: () => {},
            onAnswer: questionnaire.updateAnswer,
            onPrevious: questionnaire.previous,
            onNext: questionnaire.next,
            onFinish: questionnaire.finish,
        }
    }

    /**
     *
     */
    render () {
        const {
            state,
            section,
        } = questionnaire
        return (
            <>
                <Choose>
                    <When condition={state === STATE.LOADING}>
                        <LoadingScreen />
                    </When>
                    <When condition={state === STATE.READY}>
                        <div className='pb--6'>
                            <Choose>
                                <When condition={section.type === SECTION_TYPES.PUZZLE_TUTORIAL}>
                                    <TutorialSection
                                        {...this.commonProps}
                                        tutorial={questionnaire.tutorial}
                                        _navigation={questionnaire.bindTranslation('tutorial.navigation')}
                                        _steps={questionnaire.bindTranslation('tutorial.steps')}
                                        _tutorial={questionnaire.bindTranslation('tutorial.stages')}
                                        _game={questionnaire.bindTranslation('game')}
                                    />
                                </When>
                                <When condition={section.type === SECTION_TYPES.PUZZLE}>
                                    <PuzzleSection
                                        {...this.commonProps}
                                        puzzle={questionnaire.puzzle}
                                        _={questionnaire.bindTranslation('puzzle')}
                                        _game={questionnaire.bindTranslation('game')}
                                    />
                                </When>
                                <Otherwise>
                                    <FormSection
                                        {...this.commonProps}
                                    />
                                </Otherwise>
                            </Choose>
                        </div>
                        <div className='pv--4 pv-xs--1 pv-sm--1'></div>
                    </When>
                    <Otherwise>
                        Error
                    </Otherwise>
                </Choose>
            </>
        )
    }
}
