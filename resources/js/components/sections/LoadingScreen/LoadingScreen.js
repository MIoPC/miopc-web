import React, { PureComponent } from 'react'


export default class LoadingScreen extends PureComponent {

    /**
     *
     */
    render () {
        return (
            <>
                <div className='container'>
                    <div className='row justify-content-center pv-lg--6 pv-xl--6'>
                        <div className='col-auto pv--6 fs--7'>
                            <div className='spinner spinner--2x'></div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
