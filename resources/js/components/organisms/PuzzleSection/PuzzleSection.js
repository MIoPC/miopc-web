import React, { Component } from 'react'
import { observer } from 'mobx-react'
import PropTypes from 'prop-types'
import { Element, scroller } from 'react-scroll'

// config
import {
    STAGE_MAPPING,
    SCROLL_ELEMENT_ID,
    SCROLL_CONFIG,
} from './config'


@observer
export default class PuzzleSection extends Component {

    /** */
    static propTypes = {
        /** */
        section: PropTypes.object,
        /** */
        puzzle: PropTypes.object,
        /** */
        _: PropTypes.func,
    }

    /**
     *
     */
    get SubsectionComponent() {
        return STAGE_MAPPING[this.props.puzzle.stages.current]
    }

    /**
     *
     */
    get _stage() {
        return (path, ...parameters) => {
            return this.props._(`stages.${this.props.puzzle.stages.current}.${path}`, ...parameters)
        }
    }

    /**
     *
     */
    _alignScroll = () => {
        scroller.scrollTo(SCROLL_ELEMENT_ID, SCROLL_CONFIG)
    }

    /**
     *
     */
    onNext = async () => {
        this._alignScroll()
        const { puzzle } = this.props
        puzzle.next()
    }

    /**
     *
     */
    onNextLevel = async () => {
        this._alignScroll()
        const { puzzle } = this.props
    }

    /**
     *
     */
    render () {
        return (
            <>
                <Element id={SCROLL_ELEMENT_ID}>
                    <div className='container'>
                        <div className='row justify-content-center'>
                            <div className='col-12 col-sm-9 col-md-7 col-lg-5'>
                                <this.SubsectionComponent
                                    {...this.props}
                                    _={this._stage}
                                    onNext={this.onNext}
                                    onNextLevel={this.onNextLevel}
                                />
                            </div>
                        </div>
                    </div>
                </Element>
            </>
        )
    }
}
