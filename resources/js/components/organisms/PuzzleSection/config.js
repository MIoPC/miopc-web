// subsections
import Introduction from './subsections/Introduction'
import MusicNotice from './subsections/MusicNotice'
import Level from './subsections/Level'


// constants
import {
    STAGE_TYPE,
} from '@stores/Puzzle/constants'

/**
 *
 */
export const STAGE_MAPPING = {
    [STAGE_TYPE.INTRODUCTION]: Introduction,
    [STAGE_TYPE.MUSIC_NOTICE]: MusicNotice,
    [STAGE_TYPE.LEVEL]: Level,
}

/**
 *
 */
export const SCROLL_ELEMENT_ID = 'assignment-zone'

/**
 *
 */
export const SCROLL_CONFIG = {
    offset: -300,
}
