import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// atoms
import HeroSection from '@atoms/HeroSection'

// molecules
import Button from '@molecules/Button'


@observer
export default class Introduction extends Component {

    /** */
    static propTypes = {
        /** */
        section: PropTypes.object,
        /** */
        _: PropTypes.func,
        /** */
        onNext: PropTypes.func,
    }

    /**
     *
     */
    render () {
        const {
            title,
            subtitle,
            description,
            image,
        } = this.props.section
        return (
            <>
                <HeroSection
                    subtitle={subtitle}
                    title={title}
                    description={description}
                    image={image}
                >
                    <div className='pt--6'>
                        <Button
                            onAction={this.props.onNext}
                        >
                            {this.props._('button')}
                        </Button>
                    </div>
                </HeroSection>
            </>
        )
    }
}
