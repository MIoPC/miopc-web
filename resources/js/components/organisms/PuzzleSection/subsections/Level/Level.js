import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// atoms
import HeroSection from '@atoms/HeroSection'
import Raw from '@atoms/Raw'

// molecules
import Button from '@molecules/Button'

// organisms
import BotMania from '@organisms/minigames/BotMania'

// assets
import {
    BACKGROUND_IMAGE,
} from './assets'


@observer
export default class Level extends Component {

    /** */
    static propTypes = {
        /** */
        question: PropTypes.object,
        /** */
        puzzle: PropTypes.object,
        /** */
        _: PropTypes.func,
        /** */
        _game: PropTypes.func,
    }

    /**
     *
     */
    get title() {
        const { puzzle } = this.props
        const { currentQuestion } = puzzle
        return this.props._('title', {
            current: currentQuestion.position,
            all: puzzle.numberOfLevels,
        })
    }

    /**
     *
     */
    render () {
        return (
            <>
                <HeroSection
                    subtitle={this.props._('subtitle')}
                    title={this.title}
                    image={BACKGROUND_IMAGE}
                >
                    <div className='pt--4 pb--6'>
                        <BotMania
                            game={this.props.puzzle.game}
                            _={this.props._game}
                        />
                    </div>
                    <div className='pb--2'>
                        <Raw>
                            {this.props._('skip.notice')}
                        </Raw>
                    </div>

                    <div className='fs--sm-1'>
                        <Button
                            onAction={this.props.puzzle.skipLevel}
                            appearance='primary-2'
                        >
                            {this.props._('skip.button')}
                        </Button>
                    </div>
                </HeroSection>
            </>
        )
    }
}
