import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// atoms
import HeroSection from '@atoms/HeroSection'
import Raw from '@atoms/Raw'

// molecules
import Button from '@molecules/Button'

// assets
import {
    BACKGROUND_IMAGE,
} from './assets'


@observer
export default class Introduction extends Component {

    /** */
    static propTypes = {
        /** */
        puzzle: PropTypes.object,
        /** */
        _: PropTypes.func,
        /** */
        onNext: PropTypes.func,
    }

    /**
     *
     */
    render () {
        const { playlist } = this.props.puzzle
        return (
            <>
                <HeroSection
                    subtitle={this.props._('subtitle')}
                    title={this.props._('title')}
                    image={BACKGROUND_IMAGE}
                >
                    <div className='pt--3 text-justify'>
                        <Raw>{this.props._('notice.intro')}</Raw>
                        <ul>
                            <For each='file' of={playlist.files} index='i'>
                                <li
                                    key={file.id}
                                    className={i === 0 ? '' : 'pt--1'}
                                >
                                    <Raw>
                                        {this.props._('notice.song', {
                                            title: file.title,
                                            author: file.author,
                                            url: file.licenseLink,
                                            license: file.license,
                                            source: file.source,
                                        })}
                                    </Raw>
                                </li>
                            </For>
                        </ul>
                        <Raw>{this.props._('notice.outro')}</Raw>
                    </div>
                    <div className='pt--6'>
                        <Button
                            onAction={this.props.onNext}
                        >
                            {this.props._('button')}
                        </Button>
                    </div>
                </HeroSection>
            </>
        )
    }
}
