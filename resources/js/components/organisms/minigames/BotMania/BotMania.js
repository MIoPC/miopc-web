import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// molecules
import Board from './components/molecules/Board'
import Program from './components/molecules/Program'
import Commands from './components/molecules/Commands'
import Debugger from './components/molecules/Debugger'
import Notifications from './components/molecules/Notifications'
import Button from '@molecules/Button'


@observer
export default class BotMania extends Component {

    /** */
    static propTypes = {
        /** */
        game: PropTypes.object,
        /** */
        _: PropTypes.func,
    }

    /**
     *
     */
    get mainButtonLabel() {
        const { _, game } = this.props
        if (game.level.isRunning) return _('navigation.stop')
        return _('navigation.execute')
    }

    /**
     *
     */
    get errorMessages() {
        const { _, game } = this.props
        return game.level.errors.map(error => _(`notifications.errors.${error}`))
    }

    /**
     *
     */
    render () {
        const { _, game } = this.props
        return (
            <>
                {/* Board section */}
                <div className='pb--2'>
                    <Board
                        key={game.level.id}
                        id={game.level.id}
                        pointer={game.level.pointer}
                        board={game.level.board}
                        speed={game.speed}
                    />
                </div>

                {/* User's program section */}
                <div className='pb--2'>
                    <Program
                        key={game.level.id}
                        id={game.level.id}
                        procedures={game.level.procedures}
                        onMoveChange={game.level.updateProcedureMoves}
                        title={_('sections.program.title')}
                    />
                </div>

                {/* Available commands */}
                <div className='pb--2'>
                    <Commands
                        key={game.level.id}
                        id={game.level.id}
                        availableMoves={game.level.availableMoves}
                        title={_('sections.commands.title')}
                    />
                </div>

                <div className='pb--2'>
                    <Debugger
                        isRunning={game.level.isRunning}
                        stack={game.level.stack}
                        title={_('sections.debugger.title')}
                    />
                </div>

                {/* Errors notification */}
                <If condition={game.level.hasErrors}>
                    <div className='pb--2'>
                        <Notifications
                            title={_('sections.notifications.title')}
                            errors={this.errorMessages}
                        />
                    </div>
                </If>

                {/* Navigation */}
                <div className='pt--1 fs--sm-1'>
                    <div className='pb--2'>
                        <Button
                            isDisabled={!game.level.canBePlayed}
                            onAction={game.level.isRunning ? game.level.reset : game.level.run}
                        >
                            {this.mainButtonLabel}
                        </Button>
                    </div>
                    <div>
                        <Button
                            isDisabled={!game.level.canBeCleanedUp}
                            onAction={game.level.restart}
                            appearance='primary-2'
                        >
                            {_('navigation.cleanup')}
                        </Button>
                    </div>
                </div>
            </>
        )
    }
}
