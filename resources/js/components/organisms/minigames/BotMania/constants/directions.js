/**
 *
 */
export const ROTATION_FACTOR = 90

/**
 *
 */
export const ROTATION_LIMIT = 360

/**
 *
 */
export const DIRECTIONS = {
    NORTH: 0,
    EAST: ROTATION_FACTOR,
    SOUTH: ROTATION_FACTOR * 2,
    WEST: ROTATION_FACTOR * 3,
}

