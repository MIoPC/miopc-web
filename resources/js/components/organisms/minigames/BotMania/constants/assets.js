// constants
import { MOVE_TYPES } from './moves-types'


/**
 *
 */
export const POINTER_IMAGE = '/assets/svg/game/pointer.svg'

/**
 *
 */
export const COMMAND_ICONS = {
    [MOVE_TYPES.FORWARD]: '/assets/svg/game/command_forward.svg',
    [MOVE_TYPES.RIGHT]: '/assets/svg/game/command_right.svg',
    [MOVE_TYPES.LEFT]: '/assets/svg/game/command_left.svg',
}
