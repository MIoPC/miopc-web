/**
 *
 */
export const MOVE_TYPES = {
    LEFT: 'left',
    FORWARD: 'forward',
    RIGHT: 'right',
}
