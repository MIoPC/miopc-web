/**
 *
 */
export const POINT_TYPES = {
    TILE: 'tile',
    START: 'start',
    CHECKPOINT: 'checkpoint',
    EMPTY: 'empty',
}
