import React, { Component } from 'react'
import { observer } from 'mobx-react'
import PropTypes from 'prop-types'


@observer
export default class Pointer extends Component {

    /** */
    static propTypes = {
        /** */
        pointer: PropTypes.object,
        /** */
        speed: PropTypes.number,
        /** */
        size: PropTypes.number,
    }

    /**
     *
     */
    get styles() {
        const { pointer: { x, y, rotation, isResetting } } = this.props
        const size = `${1 / this.props.size * 100}%`
        const styles = {
            width: size,
            transform: `
                translate(${x * 100}%, ${y * -100}%)
                rotate(${rotation }deg)
            `,
        }
        if (!isResetting) styles.transitionDuration = `${this.props.speed}ms`
        return styles
    }

    /**
     *
     */
    render () {
        return (
            <>
                <div className='pointer' style={this.styles}>
                    <div className='pointer__outer'>
                        <div className='pointer__inner'>
                            <img
                                className='pointer__image'
                                src={this.props.pointer.backgroundImage}
                                draggable='false'
                                alt=''
                            />
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
