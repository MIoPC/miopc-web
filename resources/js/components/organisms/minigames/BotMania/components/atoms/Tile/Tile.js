import React, { Component } from 'react'
import { observer } from 'mobx-react'
import PropTypes from 'prop-types'

// constants
import { POINT_TYPES } from '../../../constants/point-types'


@observer
export default class Tile extends Component {

    /** */
    static propTypes = {
        /** */
        children: PropTypes.node,
        /** */
        tile: PropTypes.object,
        /** */
        speed: PropTypes.number,
    }

    /**
     * Helper getter for retrieving CSS styling
     */
    get cssRules() {
        const classes = ['tile']
        const { tile } = this.props
        if (tile.type === POINT_TYPES.EMPTY) classes.push('tile--empty')
        if (tile.type === POINT_TYPES.CHECKPOINT) classes.push('tile--checkpoint')
        if (tile.type === POINT_TYPES.START) classes.push('tile--start')
        if (tile.wasVisited) classes.push('tile--visited')
        return classes.join(' ')
    }

    /**
     *
     */
    get styles() {
        const styles = {}
        const { tile: { isResetting } } = this.props
        if (!isResetting) {
            const { speed } = this.props
            const duration = speed * 0.7
            const delay = speed - duration
            styles.transitionDuration = `${duration}ms`
            styles.transitionDelay = `${delay}ms`
        }
        return styles
    }

    /**
     *
     */
    render () {
        return (
            <>
                <div className={this.cssRules}>
                    <div className='tile__wrapper'>
                        <div className='tile__inner' style={this.styles}>
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
