import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

// constant
import {
    COMMAND_ICONS,
} from '../../../constants/assets'


export default class Command extends PureComponent {

    /** */
    static propTypes = {
        /** */
        value: PropTypes.any,
        /** */
        isPlaceholder: PropTypes.bool,
    }

    /**
     *
     */
    get icon() {
        return COMMAND_ICONS[this.props.value]
    }

    /**
     * Helper getter for retrieving CSS styling
     */
    get cssRules() {
        const classes = ['command']
        if (this.props.isPlaceholder) classes.push('command--placeholder')
        return classes.join(' ')
    }

    /**
     *
     */
    render () {
        const { icon, props: { value } } = this
        return (
            <>
                <div className={this.cssRules}>
                    <Choose>
                        <When condition={icon}>
                            <img
                                className='command__icon'
                                src={icon}
                                alt={value}
                                draggable={false}
                            />
                        </When>
                        <Otherwise>
                            <span>
                                {value}
                            </span>
                        </Otherwise>
                    </Choose>
                </div>
            </>
        )
    }
}
