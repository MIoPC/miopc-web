import React, { PureComponent, createRef } from 'react'
import PropTypes from 'prop-types'
import Sortable from 'sortablejs'

// atoms
import Command from '../Command'

// config
import {
    CONFIG_FOR_DEPOT,
    CONFIG_FOR_PROGRAM,
} from './config'


export default class Procedure extends PureComponent {

    /** */
    static propTypes = {
        /** */
        items: PropTypes.array,
        /** */
        isDepot: PropTypes.bool,
        /** */
        isInteractive:PropTypes.bool,
        /** */
        canAddMoreItems: PropTypes.bool,
        /** */
        groupId: PropTypes.any,
        /** */
        maxItems: PropTypes.number,
        /** */
        onUpdate: PropTypes.func,
        /** */
        procedure: PropTypes.object,
    }

    /** */
    static defaultProps = {
        /** */
        items: [],
        /** */
        maxItems: 0,
    }

    /** */
    _sortable

    /** */
    _poolRef

    /**
     *
     * @param  {...any} parameters
     */
    constructor(...parameters) {
        super(...parameters)
        this._poolRef = createRef()
    }

    /**
     *
     */
    componentDidMount = () => {
        // Do not invoke any complex logic if component is for presentation only
        if (this.isPresentational) return
        // Resolve base configuration
        const config = { ...(this.props.isDepot ? CONFIG_FOR_DEPOT : CONFIG_FOR_PROGRAM) }
        // Assign groups
        config.group = { ...config.group, name: this.props.groupId }
        // For real procedure list
        if (!this.props.isDepot) {
            // Assign event listeners
            config.onAdd = this.handleAdd
            config.onUpdate = this.handleUpdate
            config.onSpill = config.onRemove = this.handleRemoval
            // Add maximum items constraint
            config.group.put = this.canAddMoreItems
        }
        // Invoke functionality
        this._sortable = Sortable.create(this._poolRef.current, config)
    }

    /**
     *
     */
    componentWillUnmount = () => {
        if (this._sortable) this._sortable.destroy()
    }

    /**
     *
     */
    get listElements() {
        return [...this._sortable.el.children]
    }

    /**
     *
     */
    get placeholders() {
        return [...Array(this.props.maxItems).keys()]
    }

    /**
     *
     */
    get isPresentational() {
        return !(this.props.isInteractive || this.props.isDepot)
    }

    /**
     *
     */
    get pollCSS() {
        const classes = ['procedure__pool']
        if (this.props.isInteractive) classes.push('procedure__pool--interactive')
        if (this.isPresentational) classes.push('procedure__pool--presentational')
        return classes.join(' ')
    }

    /**
     *
     */
    canAddMoreItems = () => {
        return this.props.canAddMoreItems
    }

    /**
     *
     * @param {*} elements
     */
    updateValues = (elements = this.listElements) => {
        const values = []
        for (const element of elements) {
            values.push(element.dataset.value)
        }
        this.props.onUpdate(this.props.procedure, values)
    }

    /**
     *
     * @param {*} item
     */
    handleManualRemoval = (item) => {
        item.remove()
        this.updateValues()
    }

    /**
     *
     * @param {*} event
     * @param {*} event.item
     */
    handleAdd = ({ item }) => {
        item.onclick = () => { this.handleManualRemoval(item) }
        this.updateValues()
    }

    /**
     *
     */
    handleUpdate = () => {
        this.updateValues()
    }

    /**
     *
     * @param {*} event
     */
    handleRemoval = ({ item }) => {
        const values = this.listElements.filter(element => element !== item)
        this.updateValues(values)
    }

    /**
     *
     */
    render () {
        return (
            <>
                <div className='procedure'>
                    <div className='procedure__container'>
                        <div className='procedure__wrapper'>
                            <div className='procedure__inner'>
                                <If condition={this.props.isInteractive}>
                                    <div className='procedure__pool procedure__pool--placeholders'>
                                        <For each='placeholder' of={this.placeholders} index='i'>
                                            <div
                                                key={i}
                                                className='procedure__item'
                                            >
                                                <Command
                                                    value={i + 1}
                                                    isPlaceholder={true}
                                                />
                                            </div>
                                        </For>
                                    </div>
                                </If>
                                <div
                                    ref={this._poolRef}
                                    className={this.pollCSS}
                                >
                                    <For each='item' of={this.props.items} index='i'>
                                        <div
                                            key={i}
                                            className='procedure__item'
                                            data-value={item}
                                        >
                                            <Command value={item} />
                                        </div>
                                    </For>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
