/**
 *
 */
export const COMMON_CONFIG = {
    animation: 200,
    group: {},
    emptyInsertThreshold: 25,
}

/**
 *
 */
export const CONFIG_FOR_DEPOT = {
    ...COMMON_CONFIG,
    sort: false,
    pull: 'clone',
    put: false,
    group: {
        pull: 'clone',
        put: false,
    },
}

/**
 *
 */
export const CONFIG_FOR_PROGRAM = {
    ...COMMON_CONFIG,
    multiDrag: true,
    selectedClass: 'bg--primary-1',
    removeOnSpill: true,
}
