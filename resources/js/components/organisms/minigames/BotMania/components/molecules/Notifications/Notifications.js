import React, { Component } from 'react'
import { observer } from 'mobx-react'
import PropTypes from 'prop-types'


@observer
export default class Notifications extends Component {

    /** */
    static propTypes = {
        /** */
        errors: PropTypes.array,
        /** */
        title: PropTypes.string,
    }

    /**
     *
     */
    render () {
        return (
            <>
                {/* Notifications */}
                <div>
                    <div className='pb--2'>
                        <strong>{this.props.title}</strong>
                    </div>
                    <div>
                        <For each='error' of={this.props.errors} index='i'>
                            <div className={i ? 'pt--1' : ''} key={`${error}-${i}`}>
                                <strong className='fc--form-error'>
                                    {error}
                                </strong>
                            </div>
                        </For>
                    </div>
                </div>
            </>
        )
    }
}
