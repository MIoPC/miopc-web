import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// atoms
import Command from '../../atoms/Command'
import Procedure from '../../atoms/Procedure'


@observer
export default class Debugger extends Component {

    /** */
    static propTypes = {
        /** */
        isRunning: PropTypes.bool,
        /** */
        stack: PropTypes.array,
        /** */
        title: PropTypes.string,
    }

    /**
     *
     */
    render () {
        return (
            <>
                {/* Debugger */}
                <div>
                    <div className='pb--2'>
                        <strong>{this.props.title}</strong>
                    </div>
                    <div>
                        <Procedure
                            items={[...this.props.stack]}
                        />
                    </div>
                </div>
            </>
        )
    }
}
