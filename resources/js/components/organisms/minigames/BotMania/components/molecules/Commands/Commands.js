import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

// atoms
import Procedure from '../../atoms/Procedure'


export default class Commands extends PureComponent {

    /** */
    static propTypes = {
        /** */
        id: PropTypes.any,
        /** */
        availableMoves: PropTypes.array,
        /** */
        title: PropTypes.string,
    }

    /**
     *
     */
    render () {
        return (
            <>
                {/* Commands pool */}
                <div>
                    <div className='pb--2'>
                        <strong>{this.props.title}</strong>
                    </div>
                    <div className='d-flex'>
                        <Procedure
                            items={this.props.availableMoves}
                            isDepot={true}
                            groupId={this.props.id}
                        />
                    </div>
                </div>
            </>
        )
    }
}
