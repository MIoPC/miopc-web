import React, { Component } from 'react'
import { observer } from 'mobx-react'
import PropTypes from 'prop-types'

// atoms
import Procedure from '../../atoms/Procedure'


@observer
export default class Procedures extends Component {

    /** */
    static propTypes = {
        /** */
        id: PropTypes.any,
        /** */
        procedures: PropTypes.array,
        /** */
        availableMoves: PropTypes.array,
        /** */
        onMoveChange: PropTypes.func,
    }

    /**
     *
     */
    render () {
        return (
            <>
                {/* User's program */}
                <div>
                    <div className='pb--1'>
                        <strong>Program</strong>
                    </div>
                    <For each='procedure' of={this.props.procedures} index='i'>
                        <div key={`${this.props.id}-${i}`}>
                            <div className='fs--sm-2 pv--1'>
                                {procedure.id}
                            </div>
                            <div className='d-flex'>
                                <Procedure
                                    groupId={this.props.id}
                                    maxItems={procedure.numberOfMoves}
                                    onUpdate={this.props.onMoveChange}
                                    canAddMoreItems={procedure.canAddMoreItems}
                                    procedure={procedure}
                                />
                            </div>
                        </div>
                    </For>
                </div>

                {/* Commands pool */}
                <div>
                    <div className='pb--2'>
                        <strong>Available commands</strong>
                    </div>
                    <div className='col-12 col-md-6'>
                        <Procedure
                            items={this.props.availableMoves}
                            isDepot={true}
                            groupId={this.props.id}
                        />
                    </div>
                </div>
            </>
        )
    }
}
