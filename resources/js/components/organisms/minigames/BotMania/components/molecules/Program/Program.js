import React, { Component } from 'react'
import { observer } from 'mobx-react'
import PropTypes from 'prop-types'

// atoms
import Procedure from '../../atoms/Procedure'


@observer
export default class Program extends Component {

    /** */
    static propTypes = {
        /** */
        id: PropTypes.any,
        /** */
        procedures: PropTypes.array,
        /** */
        onMoveChange: PropTypes.func,
        /** */
        title: PropTypes.string,
    }

    /**
     *
     */
    render () {
        return (
            <>
                {/* User's program */}
                <div>
                    <div className='pb--1'>
                        <strong>{this.props.title}</strong>
                    </div>
                    <For each='procedure' of={this.props.procedures} index='i'>
                        <div key={i}>
                            <div className='fs--sm-2 pv--1'>
                                {procedure.id}
                            </div>
                            <div className='d-flex'>
                                <Procedure
                                    groupId={this.props.id}
                                    maxItems={procedure.numberOfMoves}
                                    isInteractive={true}
                                    onUpdate={this.props.onMoveChange}
                                    canAddMoreItems={procedure.canAddMoreItems}
                                    procedure={procedure}
                                />
                            </div>
                        </div>
                    </For>
                </div>
            </>
        )
    }
}
