import React, { Component } from 'react'
import { observer } from 'mobx-react'
import PropTypes from 'prop-types'

// atoms
import Tile from '../../atoms/Tile'
import Pointer from '../../atoms/Pointer'


@observer
export default class Grid extends Component {

    /** */
    static propTypes = {
        /** */
        pointer: PropTypes.object,
        /** */
        board: PropTypes.object,
        /** */
        speed: PropTypes.number,
    }

    /**
     *
     */
    render () {
        return (
            <>
                <div className='board'>
                    <div className='board__inner'>
                        <For each='row' of={this.props.board.grid} index='i'>
                            <div
                                key={i}
                                className='board__row'
                            >
                                <For each='tile' of={row} index='j'>
                                    <div
                                        className='board__tile'
                                        key={`${i}-${j}`}
                                    >
                                        <Tile
                                            tile={tile}
                                            speed={this.props.speed}
                                        />
                                    </div>
                                </For>
                            </div>
                        </For>
                        <div className='board__pointer'>
                            <Pointer
                                speed={this.props.speed}
                                pointer={this.props.pointer}
                                size={this.props.board.width}
                            />
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
