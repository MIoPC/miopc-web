import { observable, computed } from 'mobx'
import wait from 'wait'

// stores
import Procedure from '../Procedure'
import Pointer from '../Pointer'
import Board from '../Board'
import Analytics from '../Analytics'

// helpers
import getTimestamp from '@helpers/getTimestamp'

// constants
import { MOVE_TYPES } from '../../../constants/moves-types'
import { POINT_TYPES } from '../../../constants/point-types'
import { ERROR_TYPES } from '../../../constants/error-types'


/**
 *
 */
export default class Level {

    /** */
    @observable id

    /** */
    @observable procedures = []

    /** */
    @observable board = [[]]

    /** */
    @observable pointer

    /** */
    @observable speed

    /** */
    @observable stack = []

    /** */
    @observable errors = []

    /** */
    @observable _runId = null

    /** */
    analytics = new Analytics()

    /** */
    _onWin

    /**
     *
     * @param {Object} blueprint
     * @param {Object} blueprint.procedures
     * @param {Object} blueprint.tiles
     * @param {Object} blueprint.pointer
     * @param {Object} blueprint.speed
     * @param {Object} blueprint.onWin
     */
    constructor({
        procedures,
        tiles,
        pointer,
        speed,
        onWin,
    }) {
        // generate level's id
        this.id = getTimestamp()

        // parsing board data
        this.board = new Board({ tiles })

        // parsing available procedures
        this.procedures = procedures.map((numberOfMoves, index) => {
            const isMain = index === 0
            return new Procedure({ numberOfMoves, isMain, id: index })
        })

        // create a pointer
        this.pointer = new Pointer({
            x: this.board.startPoint.x,
            y: this.board.startPoint.y,
            rotation: pointer.rotation,
        })

        // set speed
        this.speed = speed

        // assign listeners
        this._onWin = onWin

        // run analytics
        this.analytics.start()
    }

    /**
     *
     */
    @computed get mainProcedure() {
        return this.procedures[0]
    }

    /**
     *
     */
    @computed get isRunning() {
        return !!this._runId && this.stack.length > 0
    }

    /**
     *
     */
    @computed get isWon() {
        const { checkpoints } = this.board
        return !checkpoints.find(({ wasVisited }) => !wasVisited)
    }

    /**
     *
     *
     */
    @computed get availableMoves() {
        return [
            ...Object.values(MOVE_TYPES),
            ...this.procedures.map(procedure => procedure.id),
        ]
    }

    /**
     *
     */
    @computed get canBePlayed() {
        return this.mainProcedure.moves.length > 0
    }

    /**
     *
     */
    @computed get numberOfCommands() {
        return this.procedures.reduce((sum, procedure) => sum + procedure.moves.length, 0)
    }

    /**
     *
     */
    @computed get canBeCleanedUp() {
        for (const procedure of this.procedures) {
            if (procedure.moves.length > 0) return true
        }
        return false
    }

    /**
     *
     */
    @computed get hasErrors() {
        return this.errors.length > 0
    }

    /**
     *
     */
    _initRun = async () => {
        /** update analytics */
        this.analytics.steps = 0
        this.analytics.plays++

        /** remove errors */
        this.error = null

        /** reset the pointer */
        await this.reset()

        /** assign the stack to the main */
        this.stack = [...this.mainProcedure.moves]

        /** make game running */
        return this._runId = getTimestamp()
    }

    /**
     *
     */
    restart = async () => {
        await this.reset()
        for (const procedure of this.procedures) {
            procedure.restart()
        }
        this.id = getTimestamp()
    }

    /**
     *
     */
    reset = async () => {
        this.stop()
        return Promise.all([
            this.board.reset(),
            this.pointer.reset(),
        ])
    }

    /**
     *
     */
    stop = async (errors = []) => {
        if (errors.length) this.analytics.errors++
        this.errors = errors
        this._runId = null
        this.stack = []
    }

    /**
     *
     */
    run = async () => {
        const runId = await this._initRun()

        /** execute moves step by step */
        while (this._runId === runId && this.stack.length) {
            await this.executeNextMove(runId)
            this.analytics.steps++
            if (this.isWon) {
                this.stop()
                this.analytics.commands = this.numberOfCommands
                this.analytics.end()
                return this._onWin()
            }
        }
    }

    /**
     *
     */
    executeNextMove = async () => {
        const runId = this._runId
        let stack = [...this.stack]
        const move = stack.shift()
        const handler = ({
            [MOVE_TYPES.FORWARD]: this.handleForwardMove,
            [MOVE_TYPES.RIGHT]: this.handleRightMove,
            [MOVE_TYPES.LEFT]: this.handleLeftMove,
        })[move] || this.handleProceduresMove

        stack = await handler(move, stack) || stack
        const waitTime = handler === this.handleProceduresMove ? this.speed * .57 : this.speed
        await wait(waitTime)
        if (runId !== this._runId) return
        this.stack = stack
    }

    /**
     *
     */
    handleRightMove = () => {
        this.pointer.rotateRight()
    }

    /**
     *
     */
    handleLeftMove = () => {
        this.pointer.rotateLeft()
    }

    /**
     *
     */
    handleForwardMove = async () => {
        /** @todo check if pointer is out of bounds */
        const {
            nextForwardX,
            nextForwardY,
        } = this.pointer
        const nextTile = this.board.getAt(nextForwardX, nextForwardY)

        // Handle errors
        if (nextTile === null) return this.stop([ ERROR_TYPES.OUT_OF_BOUNDS ])
        if (nextTile.type === POINT_TYPES.EMPTY) return this.stop([ ERROR_TYPES.EMPTY_TILE ])

        this.pointer.goForward()
        const currentTile = this.board.getAt(
            this.pointer.x,
            this.pointer.y,
        )
        currentTile.wasVisited = true
    }

    /**
     *
     */
    handleProceduresMove = (move, stack) => {
        /** find procedure moves */
        const procedure = this.procedures.find(procedure => procedure.id === move)
        const { moves } = procedure
        return [...moves, ...stack]
    }

    /**
     *
     * @param {*} procedure
     * @param {*} moves
     */
    updateProcedureMoves = (procedure, moves) => {
        this.reset()
        this.analytics.changes++
        procedure.registerMoves(moves)
    }
}
