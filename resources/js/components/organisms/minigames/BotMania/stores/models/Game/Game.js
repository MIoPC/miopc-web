import { observable, action } from 'mobx'

// stores
import Level from '../Level'

// constants
import { NOOP } from '@constants/no-op'


/**
 *
 */
export default class Game {

    /** */
    @observable level

    /** */
    @observable speed = 700

    /** */
    onWin = NOOP

    /** */
    onError = NOOP

    /**
     *
     * @param {*} blueprint
     */
    @action mountLevel = (blueprint) => {
        this.level = new Level({
            ...blueprint,
            speed: this.speed,
            onWin: this._onWin,
            onError: this._onError,
        })
    }

    /**
     *
     * @param {*} value
     */
    @action setSpeed = (value) => {
        this.speed = value
        if(this.level) this.level.speed = value
    }

    /**
     *
     */
    _onWin = () => {
        this.onWin()
    }

    /**
     *
     */
    _onError = (exception) => {
        this.onError()
    }
}
