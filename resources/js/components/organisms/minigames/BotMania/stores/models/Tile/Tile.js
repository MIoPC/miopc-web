import { observable, action } from 'mobx'
import wait from 'wait'

// constants
import { POINT_TYPES } from '../../../constants/point-types'


/**
 *
 */
export default class Tile {

    /** */
    x

    /** */
    y

    /** */
    type

    /** */
    @observable wasVisited = false

    /** */
    @observable isResetting = false

    /**
     *
     * @param {Object} [blueprint]
     */
    constructor({
        x,
        y,
        type = POINT_TYPES.EMPTY,
    }) {
        this.x = x
        this.y = y
        this.type = type
    }

    /**
     *
     */
    @action reset = async () => {
        this.isResetting = true
        this.wasVisited = false
        await wait()
        this.isResetting = false
    }
}
