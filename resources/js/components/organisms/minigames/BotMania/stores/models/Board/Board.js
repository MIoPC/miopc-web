import { observable, computed } from 'mobx'

// stores
import Tile from '../Tile'

// constants
import { POINT_TYPES } from '../../../constants/point-types'


/**
 *
 */
export default class Board {

    /** */
    @observable _tiles = []

    /**
     *
     * @param {*} blueprint
     */
    constructor({
        tiles,
    }) {
        this._tiles = tiles.map(blueprint => new Tile(blueprint))
    }

    /**
     *
     */
    reset = () => {
        for (const row of this.grid) {
            for (const tile of row) {
                tile.reset()
            }
        }
    }

    /**
     *
     * @param {*} x
     * @param {*} y
     */
    getAt = (x, y) => {
        if (!this.isOnMap(x, y)) return null
        const row = this.size.height - y - 1
        return this.grid[row][x]
    }

    /**
     *
     * @param {*} x
     * @param {*} y
     */
    isOnMap = (x, y) => {
        if (x < 0 || x >= this.size.width) return false
        if (y < 0 || y >= this.size.height) return false
        return true
    }

    /**
     *
     */
    @computed get checkpoints() {
        return this._tiles.filter(({ type }) => type === POINT_TYPES.CHECKPOINT)
    }

    /**
     *
     */
    @computed get startPoint() {
        const { x, y } = this._tiles.find(({ type }) => type === POINT_TYPES.START)
        return { x, y }
    }

    /**
     *
     */
    @computed get size() {
        let width = 0
        let height = 0
        for (const { x, y } of this._tiles) {
            width = Math.max(width, x + 1)
            height = Math.max(height, y + 1)
        }
        return { width, height }
    }

    /**
     *
     */
    @computed get grid() {
        /** Populate the grid with empty values at first */
        const grid = []
        const { width, height } = this.size
        for (let y = 0; y < height; y++) {
            const row = []
            for (let x = 0; x < width; x++) {
                row.push(
                    new Tile({ x, y }),
                )
            }
            grid.push(row)
        }
        /** Populate the grid with declared points */
        for (let tile of this._tiles) {
            const { x, y } = tile
            grid[this.size.height - y - 1][x] = tile
        }
        return grid
    }

    /**
     *
     */
    @computed get width() {
        if (!this.height) return 0
        return this.grid[0].length
    }

    /**
     *
     */
    @computed get height() {
        return this.grid.length
    }
}
