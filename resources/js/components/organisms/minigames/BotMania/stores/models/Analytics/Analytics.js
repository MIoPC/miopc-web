import { observable, computed } from 'mobx'


/**
 *
 */
export default class Analytics {

    /** */
    errors = 0

    /** */
    changes = 0

    /** */
    commands = 0

    /** */
    steps = 0

    /** */
    plays = 0

    /** */
    _startDate

    /** */
    _endDate

    /**
     *
     */
    start = () => {
        this._startDate = new Date()
    }

    /**
     *
     */
    end = () => {
        this._endDate = new Date()
    }

    /**
     * Elapsed time in seconds
     */
    @computed get elapsedTime() {
        return this._endDate - this._startDate
    }
}
