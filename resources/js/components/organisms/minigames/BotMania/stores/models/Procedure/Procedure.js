import { observable, computed, action } from 'mobx'


/**
 *
 */
export default class Procedure {

    /** */
    @observable numberOfMoves

    /** */
    isMain

    /** */
    _id


    /** */
    @observable _registeredMoves = []

    /**
     *
     * @param {Object} blueprint
     */
    constructor({
        numberOfMoves,
        isMain,
        id,
    }) {
        this.numberOfMoves = numberOfMoves
        this.isMain = isMain
        this._id = id
    }

    /**
     *
     * @param {*} moves
     */
    @action registerMoves = (moves) => {
        this._registeredMoves = moves
    }

    /**
     *
     */
    @action restart = () => {
        this._registeredMoves = []
    }

    /**
     *
     */
    @computed get moves() {
        return this._registeredMoves
    }

    /**
     *
     */
    @computed get canAddMoreItems() {
        return this._registeredMoves.length < this.numberOfMoves
    }

    /**
     *
     */
    @computed get id() {
        return `F${this._id + 1}`
    }
}
