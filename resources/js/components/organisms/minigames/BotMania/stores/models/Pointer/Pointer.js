import { observable, computed, action } from 'mobx'
import wait from 'wait'

// constants
import {
    DIRECTIONS,
    ROTATION_FACTOR,
    ROTATION_LIMIT,
} from '../../../constants/directions'

import {
    POINTER_IMAGE,
} from '../../../constants/assets'


/**
 *
 */
export default class Level {

    /** */
    _initialX

    /** */
    _initialY

    /** */
    _initialRotation

    /** */
    @observable x

    /** */
    @observable y

    /** */
    @observable rotation = 0

    /** */
    @observable isResetting = false

    /** */
    backgroundImage = POINTER_IMAGE

    /**
     *
     * @param {Object} blueprint
     * @param {Object} blueprint.x
     * @param {Object} blueprint.y
     * @param {Object} blueprint.rotation
     */
    constructor({
        x,
        y,
        rotation,
    }) {
        this.x = this._initialX = x
        this.y = this._initialY = y
        this.rotation = this._initialRotation = rotation
    }

    /**
     *
     * @param {*} value
     */
    @action _rotate = (value) => {
        this.rotation += value
    }

    /**
     *
     */
    rotateRight = () => {
        this._rotate(ROTATION_FACTOR)
    }

    /**
     *
     */
    rotateLeft = () => {
        this._rotate(-ROTATION_FACTOR)
    }

    /**
     *
     */
    @action goForward = () => {
        this.x = this.nextForwardX
        this.y = this.nextForwardY
    }

    /**
     *
     */
    @action reset = async () => {
        this.isResetting = true
        this.x = this._initialX
        this.y = this._initialY
        this.rotation = this._initialRotation
        await wait()
        this.isResetting = false
    }

    /**
     *
     */
    @computed get nextForwardX() {
        if (this.direction === DIRECTIONS.EAST) return this.x + 1
        if (this.direction === DIRECTIONS.WEST) return this.x - 1
        return this.x
    }

    /**
     *
     */
    @computed get nextForwardY() {
        if (this.direction === DIRECTIONS.NORTH) return this.y + 1
        if (this.direction === DIRECTIONS.SOUTH) return this.y - 1
        return this.y
    }

    /**
     *
     */
    @computed get direction() {
        let direction = this.rotation % ROTATION_LIMIT
        if (direction < 0) direction += ROTATION_LIMIT
        return direction
    }
}
