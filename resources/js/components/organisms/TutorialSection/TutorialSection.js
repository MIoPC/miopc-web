import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'
import { Element, scroller } from 'react-scroll'

// atoms
import HeroSection from '@atoms/HeroSection'
import Navigation from '@components/molecules/Navigation'

// molecules
import Button from '@molecules/Button'

// config
import {
    TUTORIAL_STAGE_COMPONENT,
    SCROLL_ELEMENT_ID,
    SCROLL_CONFIG,
} from './config'

@observer
export default class TutorialSection extends Component {

    /** */
    static propTypes = {
        /** */
        section: PropTypes.object,
        /** */
        tutorial: PropTypes.object,
        /** */
        hasNext: PropTypes.bool,
        /** */
        hasPrevious: PropTypes.bool,
        /** */
        _navigation: PropTypes.func,
        /** */
        _steps: PropTypes.func,
        /** */
        _tutorial: PropTypes.func,
        /** */
        _game: PropTypes.func,
        /** */
        onAnswer: PropTypes.func,
        /** */
        onPrevious: PropTypes.func,
        /** */
        onNext: PropTypes.func,
    }

    /**
     *
     * @param  {...any} parameters
     */
    constructor(...parameters) {
        super(...parameters)
    }

    /**
     *
     */
    get tutorial() {
        return this.props.tutorial
    }

    /**
     *
     */
    get description() {
        if (this.tutorial.stages.current) return null
        return this.props.section.description
    }

    /**
     *
     */
    get _tutorial() {
        return (path) => {
            return this.props._tutorial(`${this.tutorial.stages.current}.${path}`)
        }
    }

    /**
     *
     */
    get SubsectionComponent() {
        return TUTORIAL_STAGE_COMPONENT[this.tutorial.stages.current]
    }

    /**
     *
     */
    _alignScroll = () => {
        scroller.scrollTo(SCROLL_ELEMENT_ID, SCROLL_CONFIG)
    }

    /**
     * @todo PUT THIS LOGIC IN STORE
     */
    handleNext = () => {
        this._alignScroll()
        if (this.tutorial.stages.canGoNext) return this.tutorial.next()
        this.props.onNext()
    }

    /**
     * @todo PUT THIS LOGIC IN STORE
     */
    handleBack = () => {
        this._alignScroll()
        if (this.tutorial.stages.canGoBack) return this.tutorial.previous()
        this.tutorial.stages.current = null
    }

    /**
     *
     */
    render () {
        const {
            title,
            subtitle,
            image,
        } = this.props.section
        const { game, stages: { current } } = this.tutorial
        return (
            <>
                <Element id={SCROLL_ELEMENT_ID}>
                    <div className='container'>
                        <div className='row justify-content-center'>
                            <div className='col-12 col-sm-9 col-md-7 col-lg-5'>
                                <HeroSection
                                    subtitle={subtitle}
                                    title={title}
                                    description={this.description}
                                    image={image}
                                >
                                    <div className={current ? 'pt--3' : 'pt--6'}>
                                        <Choose>
                                            <When condition={current}>
                                                <div className='text-justify'>
                                                    <this.SubsectionComponent
                                                        tutorial={this.tutorial}
                                                        onBack={this.handleBack}
                                                        onNext={this.handleNext}
                                                        _={this._tutorial}
                                                        _game={this.props._game}
                                                        _steps={this.props._steps}
                                                    />
                                                </div>
                                                <If condition={this.tutorial.stages.canGoNext}>
                                                    <div className='row pt--4 fs--sm-1' key={current}>
                                                        <div className='col-12 col-md-6 pt--2 order-3 order-md-1'>
                                                            <Button onAction={this.handleBack} appearance='primary-2'>
                                                                {this.props._steps('back')}
                                                            </Button>
                                                        </div>
                                                        <div className='col-12 col-md-6 pt--2 order-2'>
                                                            <Button onAction={this.handleNext}>
                                                                {this.props._steps('next')}
                                                            </Button>
                                                        </div>
                                                    </div>
                                                </If>
                                            </When>
                                            <Otherwise>
                                                <Navigation
                                                    hasNext={true}
                                                    hasPrevious={true}
                                                    _={this.props._navigation}
                                                    onNext={this.tutorial.start}
                                                    onPrevious={this.props.onPrevious}
                                                />
                                            </Otherwise>
                                        </Choose>
                                    </div>
                                </HeroSection>
                            </div>
                        </div>
                    </div>
                </Element>
            </>
        )
    }
}
