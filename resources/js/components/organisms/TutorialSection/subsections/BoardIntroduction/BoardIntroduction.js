import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// atoms
import Raw from '@atoms/Raw'

// organisms
import Board from '@organisms/minigames/BotMania/components/molecules/Board'


@observer
export default class BoardIntroduction extends Component {

    /** */
    static propTypes = {
        /** */
        tutorial: PropTypes.object,
        /** */
        _: PropTypes.func,
    }

    /**
     *
     */
    render () {
        const { level } = this.props.tutorial.game
        return (
            <>
                <div>
                    <Raw>
                        {this.props._('introduction')}
                    </Raw>
                </div>
                <div className='pv--3'>
                    <Board
                        id={level.id}
                        key={level.id}
                        pointer={level.pointer}
                        board={level.board}
                    />
                </div>
                <div>
                    <Raw>
                        {this.props._('description')}
                    </Raw>
                </div>
            </>
        )
    }
}
