import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// atoms
import Raw from '@atoms/Raw'
import Command from '@organisms/minigames/BotMania/components/atoms/Command'

// constants
import { MOVE_TYPES } from '@organisms/minigames/BotMania/constants/moves-types'


@observer
export default class PointerCommands extends Component {

    /** */
    static propTypes = {
        /** */
        tutorial: PropTypes.object,
        /** */
        _: PropTypes.func,
    }

    /**
     *
     */
    render () {
        return (
            <>
                <div>
                    <Raw>
                        {this.props._('introduction')}
                    </Raw>
                    <ul className='mb--0'>
                        <For each='move' of={Object.values(MOVE_TYPES)} index='i'>
                            <li key={i}>
                                <Raw>
                                    {this.props._(`moves.${move}`)}
                                </Raw>
                            </li>
                        </For>
                    </ul>
                </div>
                <div className='pv--2 row'>
                    <For each='move' of={Object.values(MOVE_TYPES)} index='i'>
                        <div key={i} className='col-auto pv--1'>
                            <Command value={move} />
                        </div>
                    </For>
                </div>
                <div>
                    <Raw>
                        {this.props._('description')}
                    </Raw>
                </div>
            </>
        )
    }
}
