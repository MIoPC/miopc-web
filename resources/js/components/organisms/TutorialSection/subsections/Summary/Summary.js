import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'
import wait from 'wait'

// atoms
import Raw from '@atoms/Raw'

// molecules
import Button from '@molecules/Button'

// organisms
import BotMania from '@organisms/minigames/BotMania'

// config
import {
    FINISH_DELAY_TIME,
} from './config'


@observer
export default class Summary extends Component {

    /** */
    static propTypes = {
        /** */
        tutorial: PropTypes.object,
        /** */
        onBack: PropTypes.func,
        /** */
        onNext: PropTypes.func,
        /** */
        _: PropTypes.func,
        /** */
        _game: PropTypes.func,
        /** */
        _steps: PropTypes.func,
    }

    /**
     *
     */
    componentDidMount = () => {
        this.game = this.props.tutorial.game
        this.game.onWin = this.handleWin
    }

    /**
     *
     */
    handleWin = async () => {
        await wait(FINISH_DELAY_TIME)
        this.props.onNext()
    }

    /**
     *
     */
    render () {
        return (
            <>
                <div>
                    <Raw>
                        {this.props._('introduction')}
                    </Raw>
                </div>

                <div className='pv--4'>
                    <BotMania
                        game={this.props.tutorial.game}
                        _={this.props._game}
                    />
                </div>

                <div className='pb--2'>
                    <Raw>
                        {this.props._('description')}
                    </Raw>
                </div>

                <div className='fs--sm-1'>
                    <Button onAction={this.props.onBack} appearance='primary-2'>
                        {this.props._steps('back')}
                    </Button>
                </div>
            </>
        )
    }
}
