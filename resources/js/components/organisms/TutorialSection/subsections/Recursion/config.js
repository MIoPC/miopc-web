// constants
import { MOVE_TYPES } from '@organisms/minigames/BotMania/constants/moves-types'


/**
 *
 */
export const INITIAL_CONFIG = [
    [ 'F2' ],
    [ MOVE_TYPES.FORWARD, 'F1' ],
]

/**
 *
 */
export const GAME_SPEED = 1100

/**
 *
 */
export const GAME_RESTART_TIME = 2000
