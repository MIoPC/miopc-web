import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'
import wait from 'wait'

// atoms
import Raw from '@atoms/Raw'
import Procedure from '@organisms/minigames/BotMania/components/atoms/Procedure'

// molecules
import Board from '@organisms/minigames/BotMania/components/molecules/Board'
import Debugger from '@organisms/minigames/BotMania/components/molecules/Debugger'

// constants
import { NOOP } from '@constants/no-op'

// config
import {
    INITIAL_CONFIG,
    GAME_SPEED,
    GAME_RESTART_TIME,
} from './config'


@observer
export default class Recursion extends Component {

    /** */
    static propTypes = {
        /** */
        tutorial: PropTypes.object,
        /** */
        _: PropTypes.func,
    }

    /** */
    timeout

    /**
     *
     */
    componentDidMount = () => {
        this.game = this.props.tutorial.game
        const { level } = this.game
        const { procedures } = level
        INITIAL_CONFIG.forEach((moves, i) => procedures[i].registerMoves(moves))
        this.game.setSpeed(GAME_SPEED)
        this.game.onWin = this.onWin
        level.run()
    }

    /**
     *
     */
    componentWillUnmount = () => {
        clearTimeout(this.timeout)
        this.timeout = null
    }

    /**
     *
     */
    onWin = async () => {
        const { level } = this.game
        this.timeout = setTimeout(level.run, GAME_RESTART_TIME)
    }

    /**
     *
     */
    render () {
        const { game } = this.props.tutorial
        return (
            <>
                <div className='pb--2'>
                    <Raw>
                        {this.props._('introduction')}
                    </Raw>
                </div>

                {/* Board section */}
                <div className='pb--2 d-flex'>
                    <Board
                        id={game.level.id}
                        key={game.level.id}
                        pointer={game.level.pointer}
                        board={game.level.board}
                        speed={game.speed}
                    />
                </div>

                {/* Debugger */}
                <div className='pb--2'>
                    <Debugger
                        key={game.level.id}
                        isRunning={game.level.isRunning}
                        stack={game.level.stack}
                    />
                </div>

                {/* User's program section */}
                <div className='pb--2'>
                    <div className='row'>
                        <For each='procedure' of={game.level.procedures} index='i'>
                            <div key={procedure.id} className='col-auto'>
                                <div className='fs--sm-2 pv--1'>
                                    {procedure.id}
                                </div>
                                <div className='d-flex no-action'>
                                    <Procedure
                                        items={INITIAL_CONFIG[i]}
                                        isInteractive={true}
                                        maxItems={procedure.numberOfMoves}
                                        onUpdate={NOOP}
                                    />
                                </div>
                            </div>
                        </For>
                    </div>
                </div>

                <div>
                    <Raw>
                        {this.props._('description')}
                    </Raw>
                </div>
            </>
        )
    }
}
