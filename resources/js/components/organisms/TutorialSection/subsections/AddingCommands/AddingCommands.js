import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// atoms
import Raw from '@atoms/Raw'
import Procedure from '@organisms/minigames/BotMania/components/atoms/Procedure'

// constants
import { MOVE_TYPES } from '@organisms/minigames/BotMania/constants/moves-types'
import { NOOP } from '@constants/no-op'


@observer
export default class AddingCommands extends Component {

    /** */
    static propTypes = {
        /** */
        tutorial: PropTypes.object,
        /** */
        _: PropTypes.func,
    }

    /**
     *
     * @param {*} procedure
     * @param {*} values
     */
    updateProcedure = (procedure, values) => {
        procedure.registerMoves(values)
    }

    /**
     *
     */
    render () {
        const { level } = this.props.tutorial.game
        return (
            <>
                <div>
                    <Raw>
                        {this.props._('introduction')}
                    </Raw>
                </div>
                <div className='pt--1 pb--2'>
                    <For each='procedure' of={level.procedures}>
                        <div key={procedure.id}>
                            <div className='fs--sm-2 pv--1'>
                                {procedure.id}
                            </div>
                            <div className='d-flex'>
                                <Procedure
                                    groupId={level.id}
                                    maxItems={procedure.numberOfMoves}
                                    isInteractive={true}
                                    onUpdate={this.updateProcedure}
                                    canAddMoreItems={procedure.canAddMoreItems}
                                    procedure={procedure}
                                />
                            </div>
                        </div>
                    </For>
                </div>
                <div>
                    <Raw>
                        {this.props._('description')}
                    </Raw>
                </div>
                <div className='pv--2'>
                    <div className='d-flex'>
                        <Procedure
                            items={Object.values(MOVE_TYPES)}
                            isDepot={true}
                            groupId={level.id}
                        />
                    </div>
                </div>
                <div>
                    <Raw>
                        {this.props._('outro')}
                    </Raw>
                </div>
            </>
        )
    }
}
