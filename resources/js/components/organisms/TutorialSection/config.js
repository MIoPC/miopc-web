// subsections
import BoardIntroduction from './subsections/BoardIntroduction'
import PointerCommands from './subsections/PointerCommands'
import AddingCommands from './subsections/AddingCommands'
import Recursion from './subsections/Recursion'
import Summary from './subsections/Summary'

// constants
import {
    STAGE_TYPE,
} from '@stores/Tutorial/constants'

/**
 *
 */
export const TUTORIAL_STAGE_COMPONENT = {
    [STAGE_TYPE.BOARD_INTRODUCTION]: BoardIntroduction,
    [STAGE_TYPE.POINTER_COMMANDS]: PointerCommands,
    [STAGE_TYPE.ADDING_COMMANDS]: AddingCommands,
    [STAGE_TYPE.RECURSION]: Recursion,
    [STAGE_TYPE.SUMMARY]: Summary,
}

/**
 *
 */
export const SCROLL_ELEMENT_ID = 'game-tutorial'

/**
 *
 */
export const SCROLL_CONFIG = {
    offset: -300,
}
