import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { observer } from 'mobx-react'

// atoms
import ActionElement from '@atoms/ActionElement'
import HeroSection from '@atoms/HeroSection'

// molecules
import FormElement from '@molecules/FormElement'
import Button from '@molecules/Button'
import Navigation from '@molecules/Navigation'

// constants
import { NOOP } from '@constants/no-op'


@observer
export default class FormSection extends Component {

    /** */
    static propTypes = {
        /** */
        section: PropTypes.object,
        /** */
        hasNext: PropTypes.bool,
        /** */
        hasPrevious: PropTypes.bool,
        /** */
        _navigation: PropTypes.func,
        /** */
        previousLabel: PropTypes.string,
        /** */
        onAnswer: PropTypes.func,
        /** */
        onPrevious: PropTypes.func,
        /** */
        onNext: PropTypes.func,
        /** */
        onFinish: PropTypes.func,
    }

    /** */
    static defaultProps = {
        /** */
        onNext: NOOP,
        /** */
        onAnswer: NOOP,
        /** */
        onFinish: NOOP,
    }

    /**
     *
     */
    handleSubmission = (event) => {
        event.preventDefault()
        this.props.onNext()
    }

    /**
     *
     */
    render () {
        const {
            id,
            title,
            subtitle,
            description,
            image,
            position,
            questions,
            isValid,
            isInactive,
        } = this.props.section
        return (
            <>
                <div className='container form-section'>
                    <div className='row justify-content-center'>
                        <form className='col-12 col-sm-9 col-md-7 col-lg-5' onSubmit={this.handleSubmission}>
                            <HeroSection
                                subtitle={subtitle}
                                title={title}
                                description={description}
                                image={image}
                            >
                                <div className='pt--6'>

                                    {/* Questions */}
                                    <For each='question' of={questions} index='i'>
                                        <div className='pb--6' key={`${id}-${question.id}`}>
                                            <FormElement
                                                ordinal={`(${position}.${i + 1})`}
                                                isInactive={isInactive(question)}
                                                question={question}
                                                onChange={this.props.onAnswer}
                                            />
                                        </div>
                                    </For>

                                    {/* Navigation */}
                                    <div className='pt--3'>
                                        <Navigation
                                            hasNext={this.props.hasNext}
                                            hasPrevious={this.props.hasPrevious}
                                            isNextDisabled={!isValid}
                                            _={this.props._navigation}
                                            onNext={this.handleSubmission}
                                            onPrevious={this.props.onPrevious}
                                            onFinish={this.props.onFinish}
                                        />
                                    </div>

                                </div>
                            </HeroSection>
                        </form>
                    </div>
                </div>
            </>
        )
    }
}
