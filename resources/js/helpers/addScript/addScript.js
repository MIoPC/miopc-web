/**
 * Dynamically loads JS script using <script> tag
 * @param {string} url URL to load
 */
export default (url) => {
    return new Promise((resolve, reject) => {
        const script = document.createElement('script')
        script.onload = resolve
        script.onerror = reject
        script.src = url
        document.head.appendChild(script)
    })
}
