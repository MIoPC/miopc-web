/**
 *
 * @param {string} url
 */
export default (url) => {
    return new Promise((resolve) => {
        const image = new Image()
        image.addEventListener('load', resolve)
        image.addEventListener('error', resolve)
        image.src = url
    })
}
