// config
import {
    MOBILE_REGEX,
} from './config'


/**
 * Primitive cache object
 */
const cache = {
    isMobile: null,
}


/**
 * Determines if user is in mobile segment
 */
export const isMobile = () => {
    if (cache.isMobile === null) cache.isMobile = (MOBILE_REGEX).test(navigator.userAgent)
    return cache.isMobile
}

/**
 * Determines if user is in desktop segment
 */
export const isDesktop = () => {
    return !isMobile()
}
