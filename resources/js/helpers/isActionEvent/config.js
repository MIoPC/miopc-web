/**
 * Supported event types
 */
export const EVENTS = {
    CLICK: 'click',
    KEY_PRESS: 'keypress',
}

/**
 * Supported action keys
 */
export const KEYS_NUMBERS = {
    ENTER: 13,
    SPACE: 32,
}
