// config
import {
    EVENTS,
    KEYS_NUMBERS,
} from './config'


/**
 * Determines if given event is an action event
 * @param {Event} event
 */
export default (event) => {

    const { type } = event

    // Click event
    if (type === EVENTS.CLICK) return true

    // Key pressed event
    if (type === EVENTS.KEY_PRESS) {
        if ([
            KEYS_NUMBERS.ENTER,
            KEYS_NUMBERS.SPACE,
        ].indexOf(event.charCode) >= 0) return true
    }

    // Not supported action event
    return false
}
