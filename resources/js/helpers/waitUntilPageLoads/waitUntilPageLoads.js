/**
 * Waits until page is responsive
 */
export default async () => {
    return new Promise((resolve) => {
        const { readyState } = document
        if (['interactive', 'complete'].indexOf(readyState) >= 0) {
            return resolve()
        }
        document.addEventListener('DOMContentLoaded', resolve)
    })
}
