import axios from 'axios'

// constants
import { API_METHODS } from '@constants/api-methods'


/**
 * Memoization object
 */
let service

/**
 * Instance resolver
 */
export default (() => {
    if (!service) {
        // Create an instance using the config defaults provided by the library
        service = axios.create()

        // Add hateoas method to handle HATEOAS links in a easy way
        service.hateoas = async (
            _link,
            data = {},
        ) => {
            const { href, type } = _link
            let parameters = { ..._link.parameters, ...data }
            const method = type.toLowerCase()
            let action = method
            if (method !== API_METHODS.GET) {
                action = API_METHODS.POST
                parameters._method = method
            }
            return service[action](href, parameters)
        }
    }
    return service
})()

