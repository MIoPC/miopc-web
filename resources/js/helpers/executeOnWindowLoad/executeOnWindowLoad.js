/**
 *
 * @param {Function} method
 */
export default (method) => {
    if (document.readyState === 'complete') return method()
    window.addEventListener('load', method)
}
