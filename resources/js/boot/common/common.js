// helpers
import executeOnWindowLoad from '@helpers/executeOnWindowLoad'

// config
import {
    JS_LOADING_CLASS,
} from './config'


/**
 *
 * @param {Object} config
 */
export default async (config) => {

    // Enable CSS transitions when the whole window loads
    executeOnWindowLoad(() => document.body.classList.remove(JS_LOADING_CLASS))
}
