import {
    ASSESSMENT_TOOL,
} from '@shared/apps'

/**
 *
 */
export const APP_LOADERS = {
    [ASSESSMENT_TOOL]: () => import('@components/pages/Questionnaire'),
}

/**
 *
 */
export const APP_SELECTOR = '[data-app]'
