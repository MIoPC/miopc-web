// config
import {
    APP_LOADERS,
    APP_SELECTOR,
} from './config'


export default async (config) => {

    // If no config was provided, exit
    if (!config) return

    // If no app with given id exists, exit as well
    const loader = APP_LOADERS[config.id]
    if (!loader) return

    // Dynamically load React, ReactDOM and request component
    const [ ReactDOM, React, { default: Component } ] = await Promise.all([
        import('react-dom'),
        import('react'),
        loader(),
    ])

    // Mount given component
    ReactDOM.render(
        React.createElement(Component, config, null),
        document.querySelector(APP_SELECTOR),
    )
}
