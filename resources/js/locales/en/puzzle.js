// constants
import {
    STAGE_TYPE,
} from '@stores/Puzzle/constants'


/**
 *
 */
export default {
    stages: {
        [STAGE_TYPE.INTRODUCTION]: {
            button: 'START',
        },
        [STAGE_TYPE.MUSIC_NOTICE]: {
            title: 'Notice',
            subtitle: '',
            notice: {
                intro: `<p class="pb--2">As a&nbsp;part of the research, you are being asked to listen to certain music while resolving the puzzles. The music will be played automatically in the background. Please, make sure that your speakers or headphones are working fine.</p>
                <p>The following songs will be presented in their original form:</p>
                `,
                song: '<a class="text-underline" target="_blank" href="%{source}">%{title}</a> created by <a class="text-underline" target="_blank" href="%{source}">%{author}</a> (based on <a href="%{url}" class="text-underline" target="_blank">%{license}</a> license).',
                outro: '<p>Please note that the authors of this survey were not involved in the creation of the presented songs and are not responsible for their contents. They have been randomly selected and downloaded from the <a class="text-underline" target="_blank" href="http://freemusicarchive.org/">Free Music Archive</a> website. If you think that they are somehow inappropriate, please feel free to contact <a href="mailto:admin@miopc.edu.pl" class="text-underline">admin@miopc.edu.pl</a>.</p>',
            },
            button: 'I UNDERSTAND, START',
        },
        [STAGE_TYPE.LEVEL]: {
            title: '%{current} out of %{all}',
            subtitle: 'Level',
            skip: {
                notice: 'If you feel that you are definitely unable to solve this task, please click the button below.',
                button: 'SKIP THIS LEVEL',
            },
        },
    },
}
