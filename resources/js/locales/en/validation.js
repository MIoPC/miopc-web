// constants
import {
    VALIDATION_MESSAGES,
} from '@constants/validation-rules'

/**
 *
 */
export default {
    // integer
    [VALIDATION_MESSAGES.NOT_AN_INTEGER]: 'The value must be an integer.',
    // range
    [VALIDATION_MESSAGES.NOT_IN_RANGE]: 'The value must be a&nbsp;number between %{0} and %{1}.',
    // required
    [VALIDATION_MESSAGES.REQUIRED_MISSING_VALUE]: 'The value is required.',
    // generic
    [VALIDATION_MESSAGES.GENERIC_INVALID_VALUE]: 'Provided value is not valid.',
}
