// constants
import { MOVE_TYPES } from '@components/organisms/minigames/BotMania/constants/moves-types'
import {
    STAGE_TYPE,
} from '@stores/Tutorial/constants'


/**
 *
 */
export default {
    navigation: {
        next: 'START TUTORIAL',
        back: 'CORRECT ANSWERS',
    },
    steps: {
        next: 'NEXT',
        back: 'BACK',
    },
    stages: {
        [STAGE_TYPE.BOARD_INTRODUCTION]: {
            introduction: 'The goal of the game is to optimally program a pointer in such a way so that it could visit all the yellow tiles on a given map.',
            description: 'The pointer always starts on the blue field and it can only move across the other tiles (which means that it cannot travel outside the map).',
        },
        [STAGE_TYPE.POINTER_COMMANDS]: {
            introduction: 'The pointer can make 3 moves:',
            description: 'The icons for each move are shown above.',
            moves: {
                [MOVE_TYPES.LEFT]: 'rotate left around its axis,',
                [MOVE_TYPES.FORWARD]: 'move one tile forward,',
                [MOVE_TYPES.RIGHT]: 'rotate right around its axis.',
            },
        },
        [STAGE_TYPE.ADDING_COMMANDS]: {
            introduction: 'Depending on the level, you will have one or more functions to utilize. The pointer will treat them as a program to execute. In the scenario below, you were given 2 functions: F1 (which is used as the starting point of the program) and F2.',
            description: 'In order to populate a function, drag any command of your choice from the pool below and release it above the F1 or F2 function areas above.',
            outro: 'You can delete the previously added command either by dragging it out of the function area or by simply clicking on its icon.',
        },
        [STAGE_TYPE.RECURSION]: {
            introduction: 'You can also nest function calls within each other in order to create a recursive behavior.',
            description: 'In the example above, the main function F1 calls the function F2 in order to move the pointer forward. After that, the F2 immediately calls the F1 back, creating an endless loop. As a result, the pointer can move forward an infinite number of times, and the program consists of only 3 commands.',
        },
        [STAGE_TYPE.SUMMARY]: {
            introduction: 'This is the final step of this tutorial. In order to move forward, please resolve the level below.',
            description: 'If you need to go back to double-check the game\'s rules or you need to correct the questions from the previous sections, click the button below.',
        },
    },
}
