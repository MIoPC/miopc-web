// constants
import {
    ERROR_TYPES,
} from '@organisms/minigames/BotMania/constants/error-types'


/**
 *
 */
export default {
    navigation: {
        execute: 'EXECUTE',
        stop: 'STOP',
        cleanup: 'CLEANUP',
    },
    sections: {
        program: {
            title: 'Your program',
        },
        commands: {
            title: 'Available commands',
        },
        debugger: {
            title: 'Debugger',
        },
        notifications: {
            title: 'Output',
        },
    },
    notifications: {
        errors: {
            [ERROR_TYPES.EMPTY_TILE]: 'The pointer would go out of bounds, please try again.',
            [ERROR_TYPES.OUT_OF_BOUNDS]: 'The pointer would go out of bounds, please try again.',
        },
    },
}
