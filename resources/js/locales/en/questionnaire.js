// locales
import validation from '@locales/en/validation'
import tutorial from '@locales/en/tutorial'
import game from '@locales/en/game'
import puzzle from '@locales/en/puzzle'


/**
 *
 */
export default {
    validation,
    tutorial,
    game,
    puzzle,
    global: {
        loading: 'LOADING...',
        finish: 'FINISH',
        next: 'NEXT',
        back: 'BACK',
    },
}
