// constants
import {
    VALIDATION_MESSAGES,
} from '@constants/validation-rules'

/**
 *
 */
export default {
    // integer
    [VALIDATION_MESSAGES.NOT_AN_INTEGER]: 'Wprowadzona wartość musi być liczbą całkowitą.',
    // integer
    [VALIDATION_MESSAGES.NOT_IN_RANGE]: 'Wprowadzona wartość musi być z&nbsp;przedziału od %{0} do %{1}.',
    // required
    [VALIDATION_MESSAGES.REQUIRED_MISSING_VALUE]: 'Wprowadzona wartość jest wymagana.',
    // generic
    [VALIDATION_MESSAGES.GENERIC_INVALID_VALUE]: 'Wartość jest nieprawidłowa.',
}
