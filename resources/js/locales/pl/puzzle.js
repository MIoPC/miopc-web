// constants
import {
    STAGE_TYPE,
} from '@stores/Puzzle/constants'


/**
 *
 */
export default {
    stages: {
        [STAGE_TYPE.INTRODUCTION]: {
            button: 'ZACZNIJ BADANIE',
        },
        [STAGE_TYPE.MUSIC_NOTICE]: {
            title: 'Uwaga',
            subtitle: '',
            notice: {
                intro: `<p class="pb--2">W&nbsp;ramach badania zostaniesz poproszony o&nbsp;słuchanie muzyki w&nbsp;trakcie rozwiązywania poziomów. Upewnij się proszę, że Twoje głośniki lub słuchawki są sprawne.</p>
                <p>Poniższe utwory zostaną zaprezentowane w&nbsp;swojej oryginalnej formie:</p>
                `,
                song: '<a class="text-underline" target="_blank" href="%{source}">%{title}</a> autorstwa <a class="text-underline" target="_blank" href="%{source}">%{author}</a> (na podstawie licencji <a href="%{url}" class="text-underline" target="_blank">%{license}</a>).',
                outro: '<p>Zastrzega się, że autorzy badania nie są twórcami oraz nie biorą odpowiedzialności za treści wymienionych utworów. Zostały one pobrane w&nbsp;sposób losowy z&nbsp;serwisu <a class="text-underline" target="_blank" href="http://freemusicarchive.org/">Free Music Archive</a>. Jeśli uważasz, że są one w&nbsp;jakiś sposób niestosowne, proszę skontaktuj się&nbsp;z nami na <a href="mailto:admin@miopc.edu.pl" class="text-underline">admin@miopc.edu.pl</a>.</p>',
            },
            button: 'ROZUMIEM, ZACZNIJ BADANIE',
        },
        [STAGE_TYPE.LEVEL]: {
            title: '%{current} z&nbsp;%{all}',
            subtitle: 'Poziom',
            skip: {
                notice: 'Jeśli uważasz, że definitywnie nie jesteś w&nbsp;stanie rozwiązać tego zadania, kliknij poniższy przycisk.',
                button: 'POMIŃ TEN POZIOM',
            },
        },
    },
}
