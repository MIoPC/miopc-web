// constants
import {
    ERROR_TYPES,
} from '@organisms/minigames/BotMania/constants/error-types'


/**
 *
 */
export default {
    navigation: {
        execute: 'URUCHOM',
        stop: 'ZATRZYMAJ',
        cleanup: 'ZRESETUJ',
    },
    sections: {
        program: {
            title: 'Twój program',
        },
        commands: {
            title: 'Dostępne komendy',
        },
        debugger: {
            title: 'Debugger',
        },
        notifications: {
            title: 'Rezultat',
        },
    },
    notifications: {
        errors: {
            [ERROR_TYPES.EMPTY_TILE]: 'Znacznik znalazłby się poza mapą, spróbuj ponownie.',
            [ERROR_TYPES.OUT_OF_BOUNDS]: 'Znacznik znalazłby się poza mapą, spróbuj ponownie.',
        },
    },
}
