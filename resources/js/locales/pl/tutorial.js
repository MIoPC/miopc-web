// constants
import { MOVE_TYPES } from '@components/organisms/minigames/BotMania/constants/moves-types'
import {
    STAGE_TYPE,
} from '@stores/Tutorial/constants'


/**
 *
 */
export default {
    navigation: {
        next: 'ROZPOCZNIJ SAMOUCZEK',
        back: 'POPRAW ODPOWIEDZI',
    },
    steps: {
        next: 'DALEJ',
        back: 'WSTECZ',
    },
    stages: {
        [STAGE_TYPE.BOARD_INTRODUCTION]: {
            introduction: 'Celem gry jest optymalne zaprogramowanie znacznika w taki sposób, aby odwiedził on wszystkie punkty kontrolne na danej mapie (oznaczone są one kolorem żółtym).',
            description: 'Znacznik początkowo zawsze znajduje się na niebieskim polu i&nbsp;przemieszczać się może wyłącznie po kafelkach (co oznacza, że nie może on wyjść poza zakres mapy).',
        },
        [STAGE_TYPE.POINTER_COMMANDS]: {
            introduction: 'Sam znacznik może wykonać 3 ruchy:',
            description: 'Ikony odpowiadające poszczególnym ruchom zamieszczono powyżej.',
            moves: {
                [MOVE_TYPES.LEFT]: 'obrót w&nbsp;lewo wokół własnej osi,',
                [MOVE_TYPES.FORWARD]: 'przemieszczenie się o jedno pole do przodu,',
                [MOVE_TYPES.RIGHT]: 'obrót w&nbsp;prawo wokół własnej osi.',
            },
        },
        [STAGE_TYPE.ADDING_COMMANDS]: {
            introduction: 'W&nbsp;zależności od mapy, będziesz mieć do dyspozycji jedną lub więcej funkcji, które jako całość tworzyć będą program wykonywany przez znacznik. W&nbsp;tym przypadku do Twojej dyspozycji są funkcje F1 (która służy jako punkt startowy programu) oraz F2.',
            description: 'Aby zaprogramować funkcję, przeciągnij dowolnie wybraną przez siebie komendę z&nbsp;puli poniżej bezpośrednio na obszar funkcji F1 lub F2 znajdujących się powyżej.',
            outro: 'Dodaną komendę możesz następnie usunąć poprzez przeciągnięcie jej poza obszar funkcji lub kliknięcie. Możesz również zmieniać kolejność komend lub przerzucać je pomiędzy funkcjami.',
        },
        [STAGE_TYPE.RECURSION]: {
            introduction: 'Możesz również zagnieżdżać w&nbsp;sobie wywołania funkcji w&nbsp;taki sposób, aby wywołać rekurencję.',
            description: 'Na powyższym przykładzie widać, jak główna funkcja F1 wywołuje funkcję F2, której zadaniem jest przesunięcie znacznika o&nbsp;jedno pole do przodu, oraz wywołanie zwrotnie funkcji F1. Dzięki temu nieskończonemu cyklowi, znacznik może dotrzeć do końca mapy używając zaledwie 3 komend.',
        },
        [STAGE_TYPE.SUMMARY]: {
            introduction: 'To już ostatni krok tego samouczka. Aby przejść do właściwiego badania, rozwiąż proszę poniższą planszę.',
            description: 'Jeśli potrzebujesz powtórzyć sobie zasady gry lub musisz poprawić pytania z&nbsp;poprzednich sekcji, naciśnij przycisk poniżej.',
        },
    },
}
