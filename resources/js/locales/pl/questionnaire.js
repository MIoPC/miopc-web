// locales
import validation from '@locales/pl/validation'
import tutorial from '@locales/pl/tutorial'
import game from '@locales/pl/game'
import puzzle from '@locales/pl/puzzle'


/**
 *
 */
export default {
    validation,
    tutorial,
    game,
    puzzle,
    global: {
        loading: 'ŁADOWANIE...',
        finish: 'ZAKOŃCZ',
        next: 'DALEJ',
        back: 'WSTECZ',
    },
}
