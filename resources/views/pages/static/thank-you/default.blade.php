@extends('layouts.app.default')

@section('page.title', __('pages/static/thank-you.seo.title'))

@section('page.content')
 <div>
        <div class="container pv--6 pt-xs--1 homepage-hero">
            <div class="row align-items-center justify-content-center text-xl-justify">

                {{-- Circuit image --}}
                <div class="col-12 col-md-3">
                    <div class="text-center opacity--faded pr-md--6 pr-lg--6 pr-xl--6">
                        <div class="pr-lg--6 pr-xl--6 homepage-hero__image">
                            <img
                                class="d-block"
                                src="{{ asset('assets/svg/pages/static/thank-you/survey-finished.svg') }}?v={{ config('resources.version.images') }}"
                                alt="" draggable="false"
                            />
                        </div>
                    </div>
                </div>

                {{-- Hero text content --}}
                <div class="col-12 col-md-5">

                    {{-- Hero's header --}}
                    <div class="fs--4 fs-lg--6 lh--4 font-weight-normal">
                        <p>
                            @lang('pages/static/thank-you.hero.part-1')
                            <u class="fc--primary-1 font-weight-medium">@lang('pages/static/thank-you.hero.part-2')</u>
                        </p>
                    </div>

                    {{-- Spacer --}}
                    <div class="pv--3">
                        <hr class="bg--light-2"/>
                    </div>

                    {{-- Description --}}
                    <div class="fc--dark-1 fs--1 fs-md--2 lh--4">
                        <p>
                            @lang('pages/static/thank-you.description')
                        </p>
                    </div>

                    {{-- CTA --}}
                    <div class="pt--6">
                        <div class="row">
                            <div class="col-12 col-sm-8">
                                <a
                                    class="button button--primary-1 button--full"
                                    href="{{ __route('homepage', [], false) }}"
                                >
                                    @lang('pages/static/thank-you.cta')
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pv--6 pv-xs--3 pv-sm--3"></div>
    </div>
@endsection
