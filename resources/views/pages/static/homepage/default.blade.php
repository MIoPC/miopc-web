@extends('layouts.app.default')

@section('head.title', __('pages/static/homepage.seo.title'))

@section('page.content')
    <div>
        <div class="container pv--6 pt-xs--1 homepage-hero">
            <div class="row align-items-center text-xl-justify">

                {{-- Circuit image --}}
                <div class="col-12 col-md-5 order-md-last">
                    <div class="text-center opacity--faded ph-md--6 ph-lg--6 ph-xl--6">
                        <div class="ph-lg--6 ph-xl--6 homepage-hero__image">
                            <img
                                class="d-block"
                                src="{{ asset('assets/svg/pages/static/homepage/comment-circuit.svg') }}?v={{ config('resources.version.images') }}"
                                alt="" draggable="false"
                            />
                        </div>
                    </div>
                </div>

                {{-- Hero text content --}}
                <div class="col-12 col-md-7">

                    {{-- Hero's header --}}
                    <div class="fs--4 fs-lg--6 lh--4 font-weight-normal">
                        <p>
                            @lang('pages/static/homepage.hero.part-1')
                            <u class="fc--primary-1 font-weight-medium">@lang('pages/static/homepage.hero.part-2')</u>
                            @lang('pages/static/homepage.hero.part-3')
                        </p>
                    </div>

                    {{-- Spacer --}}
                    <div class="pv--3">
                        <hr class="bg--light-2"/>
                    </div>

                    {{-- Description --}}
                    <div class="fc--dark-1 fs--1 fs-md--2 lh--4">
                        <p>
                            @lang('pages/static/homepage.description')
                        </p>
                    </div>

                    {{-- CTA --}}
                    <div class="pt--6">
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <a
                                    class="button button--primary-1 button--full"
                                    href="{{ __route('questionnaire') }}"
                                >
                                    @if ($hasJourney)
                                        @lang('pages/static/homepage.cta.verified')
                                    @else
                                        @if ($hasCompleted)
                                            @lang('pages/static/homepage.cta.finished')
                                        @else
                                            @lang('pages/static/homepage.cta.anonymous')
                                        @endif
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pv--6 pv-xs--3 pv-sm--3"></div>
    </div>
@endsection
