@extends('layouts.app.default')

@section('page.title', __('pages/journeys/questionnaire.seo.title'))

@section('page.content')
    <div data-app>
        <div class="container">
            <div class="row justify-content-center pv-lg--6 pv-xl--6">
                <div class="col-auto pv--6 fs--7">
                    <div class="spinner spinner--2x"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
