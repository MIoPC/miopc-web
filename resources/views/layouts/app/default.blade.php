@extends('layouts.scaffold.default')

{{-- Page title --}}
@section('head.title')
    @yield('page.title') | @lang('common/global.seo.app-name')
@endsection

{{-- Main content --}}
@section('layout.content')
    <div class="page">

        {{-- Menu --}}
        <div class="page__menu">
            @include('layouts.app.partials.menu')
        </div>

        {{-- Page content --}}
        <div class="page__content">
            @yield('page.content')
        </div>

        {{-- Footer --}}
        <div class="page__footer">
            @include('layouts.app.partials.footer')
        </div>
    </div>
@endsection
