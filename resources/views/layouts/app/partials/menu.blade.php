<div class="menu">

    {{-- Floating bar --}}
    <div class="menu__bar pt--1 bg--primary-1"></div>

    {{-- Floating bar spacing buffor --}}
    <div class="pt--1"></div>

    {{-- Menu inner body --}}
    <div class="container">
        <div class="pt--4 pb--6">
            <div class="menu__logo">
                <a href="{{ __route('homepage', [], false) }}">
                    <img
                        class="d-block no-select"
                        src="{{ __asset('assets/svg/logo/horizontal.svg') }}?v={{ config('resources.version.images') }}"
                        alt="{{ __('common/menu.logo.alt') }}" draggable="false"
                    />
                </a>
            </div>
        </div>
    </div>
</div>
