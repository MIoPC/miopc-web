<div class="footer bg--dark-1 fc--light-1 pv--4">
    <div class="container">
        <div class="row justify-content-between">
            <div class="footer__logo col-auto">
                <a href="{{ __route('homepage', [], false) }}">
                    <img
                        class="d-block no-select"
                        src="{{ __asset('assets/svg/logo/horizontal-inverse.svg') }}?v={{ config('resources.version.images') }}"
                        alt="{{ __('common/footer.logo.alt') }}" draggable="false"
                    />
                </a>
            </div>
            <div class="footer__content col">
                <div class="row justify-content-end">

                    {{-- Language switcher --}}
                    <div class="col-12 col-md-6 col-lg-5 col-xl-4">
                        <div class="footer-list text-right">
                            <div class="footer-list__header">
                                @lang('common/footer.language-switch.title')
                            </div>
                            <div class="footer-list__body d-flex justify-content-end">
                                @foreach (config('app.supported_locales') as $locale)
                                    <a
                                        href="{{ __route('homepage', [], false, $locale) }}"
                                        class="footer-list__link ml--2"
                                    >
                                        {{ __("common/locales.list.$locale") }}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-8 col-lg-6 col-xl-4 pt-xs--3 pt-sm--3">
                <p class="fs--sm-3 fc--light-2 text-justify">
                    @lang('common/footer.note')
                </p>
            </div>
        </div>
    </div>
</div>
