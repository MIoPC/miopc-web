{{-- Common HTML5 tags --}}
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('head.title')</title>

{{-- Styles and scripts --}}
<link href="{{ asset('compiled/css/app.css') }}?v={{ config('resources.version.css') }}" rel="stylesheet">
<script async src="{{ asset('compiled/js/app.js') }}?v={{ config('resources.version.js') }}"></script>

{{-- Favicon --}}
<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/favicons/apple-icon-57x57.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/favicons/apple-icon-60x60.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/favicons/apple-icon-72x72.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/favicons/apple-icon-76x76.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/favicons/apple-icon-114x114.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/favicons/apple-icon-120x120.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/favicons/apple-icon-144x144.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/favicons/apple-icon-152x152.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/favicons/apple-icon-180x180.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="icon" type="image/png" sizes="192x192" href="{{ asset('assets/favicons/android-icon-192x192.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/favicons/favicon-32x32.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/favicons/favicon-96x96.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/favicons/favicon-16x16.png') }}?v={{ config('resources.version.favicon') }}">
<link rel="manifest" href="{{ asset('assets/favicons/manifest.json') }}?v={{ config('resources.version.favicon') }}">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="{{ asset('assets/favicons/ms-icon-144x144.png') }}?v={{ config('resources.version.favicon') }}">
<meta name="theme-color" content="#333333">

{{-- CSRF token --}}
<meta name="robots" content="noindex, nofollow">
<meta name="csrf-token" content="{{ csrf_token() }}" data-csrf-token>

{{-- SEO content --}}
<meta property="og:title" content="@yield('head.title')" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ url()->current() }}" />
<meta property="og:image" content="{{ __asset('assets/images/seo/og-image.png') }}?v={{ config('resources.version.images') }}" />
