@if (isset($jsConfig))
    <script>
        window.__APP = @json($jsConfig);
    </script>
@endif
