<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @yield('head.before')
        @section('layout.head')
            @include('layouts.scaffold.partials.head')
        @show
        @yield('head.after')
    </head>
    <body class="js-loading">
        @yield('layout.content')
        @include('layouts.scaffold.partials.js-config')
    </body>
</html>
