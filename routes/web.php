<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\LocaleManager;
use App\Helpers\URLHelper as URL;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$supportedLocales = config('app.supported_locales');


/**
 * DAL routes
 */
Route::prefix('dal')->group(function () {
    Route::prefix('v1')->group(function () {
        /**
         * Questionnaire journey
         */
        Route::prefix('questionnaire')->group(function () {
            Route::get('/latest', 'DAL\V1\QuestionnaireController@latest')->name('dal.v1.questionnaire.latest');
            Route::post('/verify', 'DAL\V1\QuestionnaireController@verify')->name('dal.v1.questionnaire.verify');
            Route::post('/finish', 'DAL\V1\QuestionnaireController@finish')->name('dal.v1.questionnaire.finish');
            Route::post('/{questionId}/submit', 'DAL\V1\QuestionnaireController@submitAnswer')->name('dal.v1.questionnaire.submit-answer');
            Route::prefix('/survey/{surveyId}/section/{sectionId}')->group(function () {
                Route::post('/', 'DAL\V1\QuestionnaireController@getSurveySection')->name('dal.v1.questionnaire.get-section');
                Route::patch('/', 'DAL\V1\QuestionnaireController@submitSection')->name('dal.v1.questionnaire.submit-section');
            });
        });
    });
});

/**
 * Routes that can be translated across locales
 */
foreach ($supportedLocales as $locale) {
    Route::group([
        'prefix' => '/{' . LocaleManager::ROUTE_KEY . '}',
        'where' => [ LocaleManager::ROUTE_KEY => $locale ],
    ], function () use ($locale) {

        /**
         * Questionnaire journey
         */
        Route::get(URL::resolve('questionnaire', $locale), 'App\QuestionnaireController@show')->name($locale . '.questionnaire');

        /**
         * Finished journey
         */
        Route::get(URL::resolve('thank-you', $locale), 'App\StaticPagesController@thankYou')->name($locale . '.thank-you');
    });
}

/**
 * Common routes for every defined locale
 */
Route::group([
    'prefix' => '/{' . LocaleManager::ROUTE_KEY . '}',
    'where' => [ LocaleManager::ROUTE_KEY => join('|', $supportedLocales) ],
], function () {

    /**
     * Homepage
     */
    Route::get('/', 'App\StaticPagesController@homepage')->name('homepage');

    /**
     * 404
     */
    Route::any('/{any}', 'App\StaticPagesController@show404')->where('any', '.*');
});

/**
 * Resolve user's locale
 */
Route::any('/{any}', 'App\LanguageController@resolve')->where('any', '.*');
