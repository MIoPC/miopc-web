const path = require('path')


const dir = path.resolve(__dirname)
const root = path.resolve(dir, './resources/js')

module.exports = {
    resolve: {
        alias: {
            '@atoms': path.resolve(root, 'components/atoms'),
            '@boot': path.resolve(root, 'boot'),
            '@components': path.resolve(root, 'components'),
            '@constants': path.resolve(root, 'constants'),
            '@helpers': path.resolve(root, 'helpers'),
            '@locales': path.resolve(root, 'locales'),
            '@molecules': path.resolve(root, 'components/molecules'),
            '@organisms': path.resolve(root, 'components/organisms'),
            '@pages': path.resolve(root, 'components/pages'),
            '@sections': path.resolve(root, 'components/sections'),
            '@shared': path.resolve(dir, 'shared'),
            '@stores': path.resolve(root, 'stores'),
        },
    },
    output: {
        chunkFilename: 'compiled/js/chunks/[id].[contenthash].js',
    },
}
